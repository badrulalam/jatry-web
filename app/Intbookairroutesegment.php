<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbookairroutesegment extends Model
{
    protected $table = 'intbookairroutesegment';

    public function intbookairroute()
    {
        return $this->belongsTo('App\Intbookairroute');
    }

    public function intbookairseatmap()
    {
        return $this->hasMany('App\Intbookairseatmap');
    }
}
