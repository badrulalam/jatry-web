<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartitemair extends Model
{

    protected $table = 'cartitemair';

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }


    public function passenger()
    {
        return $this->hasMany('App\Cartitemairpassenger');
    }

//    public function cartairpassengeritem()
//    {
//        return $this->hasMany('App\Cartairpassengeritem');
//    }


    public function route()
    {
        return $this->hasMany('App\Cartairroute');
    }

}
