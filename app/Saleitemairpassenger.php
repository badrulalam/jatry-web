<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saleitemairpassenger extends Model
{


    protected $table = 'saleitemairpassenger';

    public function saleitemair()
    {
        return $this->belongsTo('App\Saleitemair');
    }

    public function saleairseatmap()
    {
        return $this->hasMany('App\Saleairseatmap');
    }

}
