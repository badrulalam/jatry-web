<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saleairroutesegment extends Model
{
    protected $table = 'saleairroutesegment';

    public function saleairroute()
    {
        return $this->belongsTo('App\Saleairroute');
    }

    public function saleairseatmap()
    {
        return $this->hasMany('App\Saleairseatmap');
    }


}
