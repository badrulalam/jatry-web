<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbookairroute extends Model
{

    protected $table = 'intbookairroute';

    public function intbookitemair()
    {
        return $this->belongsTo('App\Intbookitemair');
    }

    public function segment()
    {
        return $this->hasMany('App\Intbookairroutesegment');
    }
}
