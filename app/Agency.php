<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    protected $table = 'agency';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function agent()
    {
        return $this->hasMany('App\Agent');
    }

    public function customer()
    {
        return $this->hasMany('App\Customer');
    }

    public function bannerimage()
    {
        return $this->hasMany('App\Bannerimage');
    }


}
