<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbookairseatmap extends Model
{

    protected $table = 'intbookairseatmap';
    public function intbookairroutesegment()
    {
        return $this->belongsTo('App\Intbookairroutesegment');
    }

    public function intbookitemairpassenger()
    {
        return $this->belongsTo('App\Intbookitemairpassenger');
    }

}
