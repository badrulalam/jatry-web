<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routesearch extends Model
{
    protected $table = 'routesearch';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
