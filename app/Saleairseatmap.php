<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saleairseatmap extends Model
{
    protected $table = 'saleairseatmap';


    public function saleairroutesegment()
    {
        return $this->belongsTo('App\Saleairroutesegment');
    }

    public function saleitemairpassenger()
    {
        return $this->belongsTo('App\Saleitemairpassenger');
    }


}
