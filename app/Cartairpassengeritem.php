<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartairpassengeritem extends Model
{

    protected $table = 'cartairpassengeritem';


    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function cartitemair()
    {
        return $this->belongsTo('App\Cartitemair');
    }

    public function cartitemairpassenger()
    {
        return $this->belongsTo('App\Cartitemairpassenger');
    }



}
