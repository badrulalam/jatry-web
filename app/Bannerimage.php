<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bannerimage extends Model
{
    protected $table = 'bannerimage';

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }
}
