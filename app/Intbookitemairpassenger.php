<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbookitemairpassenger extends Model
{


    protected $table = 'intbookitemairpassenger';

    public function intbookitemair()
    {
        return $this->belongsTo('App\Intbookitemair');
    }

    public function intbookairseatmap()
    {
        return $this->hasMany('App\Intbookairseatmap');
    }

}
