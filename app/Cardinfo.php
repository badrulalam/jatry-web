<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cardinfo extends Model
{

    protected $table = 'cardinfo';

    public function sale()
    {
        return $this->hasMany('App\Sale');
    }

    public function intbook()
    {
        return $this->hasMany('App\Intbook');
    }
}
