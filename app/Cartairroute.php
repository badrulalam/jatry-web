<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartairroute extends Model
{
    protected $table = 'cartairroute';

    public function cartitemair()
    {
        return $this->belongsTo('App\Cartitemair');
    }

    public function segment()
    {
        return $this->hasMany('App\Cartairroutesegment');
    }



}
