<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domflight extends Model
{

    protected $table = 'domflight';

    public function domairline()
    {
        return $this->belongsTo('App\Domairline');
    }

    public function orgincity()
    {
        return $this->belongsTo('App\Domcity', 'orgincity_id');
    }

    public function destinationcity()
    {
        return $this->belongsTo('App\Domcity', 'destinationcity_id');
    }



}
