<?php

/**
 * Created by PhpStorm.
 * User: ze
 * Date: 8/7/16
 * Time: 6:16 PM
 */

namespace App\Helpers;

//use Illuminate\Support\Facades\Request;
//use App\Http\Requests;
use Request;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use DB;

class MyFuncs
{
    public static function full_name($first_name,$last_name) {
        return $first_name . ', '. $last_name;
    }

    public static function Hostname() {

        $hostn = explode('.', Request::server('HTTP_HOST'));
        $lowww = strtolower($hostn[0]);
        if($lowww=='www'){
            $hostnabc=array_except($hostn,array(0));
            return implode('.', array_filter($hostnabc));
        }
        else{
            return Request::server('HTTP_HOST');
        }


    }


    public static function getAgency() {
//        $agency = DB::table('agency')->where('url', self::Hostname())
//            ->where('isactive', 1)
//            ->first();

        $agency = Agency::where('url', self::Hostname())
            ->where('isactive', 1)
            ->first();
//            ->firstOrFail();



        if($agency){
//            return array_flatten($agency);
            return $agency;
//            return $agency->user->agent->id;
        }
        else{
            return false;
        }


    }

}