<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Tpackage extends Model
{
    use Sluggable;

    protected $table = 'tpackage';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }



    public function pcategory()
    {
        return $this->belongsToMany('App\Pcategory', 'pcategory_tpackage', 'tpackage_id', 'pcategory_id');
    }

    public function jcontinent()
    {
        return $this->belongsTo('App\Jcontinent');
    }

    public function jcountry()
    {
        return $this->belongsTo('App\Jcountry');
    }

    public function cartitempkg()
    {
        return $this->hasMany('App\Cartitempkg');
    }

}
