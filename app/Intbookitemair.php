<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbookitemair extends Model
{
    protected $table = 'intbookitemair';


    public function intbook()
    {
        return $this->belongsTo('App\Intbook');
    }

    public function route()
    {
        return $this->hasMany('App\Intbookairroute');
    }

    public function passenger()
    {
        return $this->hasMany('App\Intbookitemairpassenger');
    }


}
