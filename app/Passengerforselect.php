<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passengerforselect extends Model
{

    protected $table = 'passengerforselect';
    protected $fillable = [
        'user_id',
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'dob',
        'nationality',
        'passport_no',
        'passport_issue_country',
        'passport_expiry_date',
        'passenger_type'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }



}
