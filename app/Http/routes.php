<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
////    return view('welcome');
////    return Request::server('HTTP_HOST');
////    return MyFuncs::full_name("John","Doe");
//    return MyFuncs::Hostname();
//});



Route::get('/', [
    'as' => 'home',
    'uses' => 'FrontendController@homeAction'
]);
Route::get('flights', [
    'as' => 'flights',
    'uses' => 'FrontendController@FlightAction'
]);
Route::get('book-checkout-ui', [
    'as' => 'book-checkoutui',
    'uses' => 'FrontendController@BookCheckoutUiAction'
]);

Route::get('purchase-checkout-ui', [
    'as' => 'purchase-checkoutui',
    'uses' => 'FrontendController@PurchaseCheckoutUiAction'
]);

Route::get('book-confirm-ticket', [
    'as' => 'bookconfirmticket',
    'uses' => 'FrontendController@BookConfirmTicketAction'
]);

Route::get('purchase-confirm-ticket', [
    'as' => 'purchaseconfirmticket',
    'uses' => 'FrontendController@PurchaseConfirmTicketAction'
]);

Route::get('checkout-final', [
    'as' => 'checkoutfinal',
    'uses' => 'FrontendController@CheckoutFinalAction'
]);
Route::get('ticket-booking/{invoice}', [
    'as' => 'ticketbooking',
//    'uses' => 'FrontendController@TicketSaleAction'
    'uses' => 'IntbookingController@TicketBookingAction'
]);

Route::get('ticket-booking-cancle/{invoice}', [
    'as' => 'ticketbookingcancle',
//    'uses' => 'FrontendController@TicketSaleAction'
    'uses' => 'IntbookingController@TicketBookingCancleAction'
]);

Route::get('booking-ticket-purchase/{invoice}', [
    'as' => 'bookingticketpurchase',
    'uses' => 'SaleController@BookingTicketPurchaseAction'
]);


Route::get('ticket-sale/{invoice}', [
    'as' => 'ticketsale',
    'uses' => 'SaleController@TicketSaleAction'
]);

Route::get('ticket-sale-view/{invoice}', [
    'as' => 'ticketsaleview',
    'uses' => 'SaleController@TicketSaleVIewAction'
]);


Route::get('booking-pdf/{invoice}', [
    'as' => 'bookingpdf',
    'uses' => 'IntbookingController@TicketBookingPdfAction'
]);

Route::get('sale-pdf/{invoice}', [
    'as' => 'salepdf',
//    'uses' => 'FrontendController@TicketSaleAction'
    'uses' => 'SaleController@TicketSalePdfAction'
]);


Route::get('flight/booking-status', [
    'as' => 'booking-status',
    'uses' => 'FlightController@BookingStatusAction'
]);

Route::get('flight/flight-status', [
    'as' => 'flight-status',
    'uses' => 'FlightController@FlightStatusAction'
]);

Route::get('flight/my-tickets', [
    'as' => 'my-tickets',
    'uses' => 'FlightController@MyTicketsAction'
]);

//=========Domestic Start=============

Route::get('dom-flights', [
    'as' => 'domflights',
    'uses' => 'FrontendController@DomFlightAction'
]);


Route::get('dom-flights-test', [
    'as' => 'domflights-test',
    'uses' => 'DomflightController@flightTestAction'
]);



//=========Domestic End===============





//============Password start===========

Route::get('user-pass/change', [
    'as' => 'userpasschange',
    'uses' => 'UserController@changePasswordAction'
]);

Route::post('user-pass/change-save', [
    'as' => 'userpasschangesave',
    'uses' => 'UserController@changePasswordSaveAction'
]);


Route::get('user-pass/reset', [
    'as' => 'userpassreset',
    'uses' => 'UserController@resetPasswordAction'
]);

Route::post('user-pass/resetsend', [
    'as' => 'userpassresetsend',
    'uses' => 'UserController@resetPassworSendAction'
]);

Route::get('userpass/resetcode/{code}', [
    'as' => 'userpassresetcode',
    'uses' => 'UserController@resetPassworCodeAction'
]);


Route::post('userpass/resetcodedone', [
    'as' => 'userpassresetcodedone',
    'uses' => 'UserController@resetPassworCodeDoneAction'
]);


//============Password end===========



//----------Agency Start-----------------
Route::get('admin-agency/index', [
    'as' => 'allagencylist',
    'uses' => 'AgencyController@allAgencyListAction'
]);


Route::get('admin-agency/{id}/show', [
    'as' => 'adminagencyshow',
    'uses' => 'AgencyController@adminAgencyShowAction'
]);

Route::get('admin-agency/{id}/agent', [
    'as' => 'adminagencyagentlist',
    'uses' => 'AgencyController@adminAgencyAgentListAction'
]);

//----------Agency End-----------------


//-----------Agent Start----------------
Route::get('admin-agent/index', [
    'as' => 'allagencylist',
    'uses' => 'AgentController@allAgentListAction'
]);

//-----------Agent End----------------


//-------------Customer Start-----------------

Route::get('admin-customer/index', [
    'as' => 'allcustomerlist',
    'uses' => 'CustomerController@allCustomerListAction'
]);


//-------------Customer End-----------------



//----------Frontend Start-------------

Route::get('/agent-register', [
    'as' => 'agentregister',
    'uses' => 'AgentController@agentregisterAction'
]);

Route::post('/agent-register-save', [
    'as' => 'agentregistersave',
    'uses' => 'AgentController@agentregisterSaveAction'
]);

Route::get('/agent-login', [
    'as' => 'frontagentlogin',
    'uses' => 'AgentController@frontAgentLoginAction'
]);

Route::post('/agent-login-do', [
    'as' => 'agentlogindo',
    'uses' => 'Auth\AuthController@frontAgentLoginDOAction'
]);

Route::get('/agent-my-account', [
    'as' => 'agentmyaccount',
    'uses' => 'AgentController@agentMyAccountAction'
]);
Route::post('/agent-my-account-save', [
    'as' => 'agentmyaccountsave',
    'uses' => 'AgentController@agentMyAccountSaveAction'
]);


Route::get('/customer-register', [
    'as' => 'customerregister',
    'uses' => 'CustomerController@customerRegisterAction'
]);

Route::post('/customer-register-save', [
    'as' => 'customerregistersave',
//    'uses' => 'CustomerController@customerRegisterSaveAction'
    'uses' => 'Auth\AuthController@customerRegisterSaveAction'
]);
Route::post('/customer-register-with-booking', [
    'as' => 'customerregistersavewithbooking',
//    'uses' => 'CustomerController@customerRegisterSaveAction'
    'uses' => 'Auth\AuthController@customerRegisterSaveWithBookingAction'
]);


Route::get('/customer-register-success', [
    'as' => 'customerregistersuccess',
    'uses' => 'CustomerController@customerRegisterSuccessAction'
]);




Route::get('/customer-login', [
    'as' => 'customerlogin',
    'uses' => 'CustomerController@customerLoginAction'
]);

Route::post('/customer-login-do', [
    'as' => 'customerlogindo',
    'uses' => 'Auth\AuthController@customerLoginDOAction'
]);
Route::post('/customer-login-with-booking', [
    'as' => 'customerlogindomodal',
    'uses' => 'Auth\AuthController@customerLoginDoWithBookNowAction'
]);

Route::get('/customer-my-account', [
    'as' => 'customermyaccount',
    'uses' => 'CustomerController@customerMyAccountAction'
]);

//Route::post('/customer-my-account-save', [
//    'as' => 'customermyaccountsave',
//    'uses' => 'CustomerController@MyAccountSaveAction'
//]);

Route::post('/customer-my-account-login-info-save', [
    'as' => 'customermyaccountlogininfosave',
    'uses' => 'CustomerController@MyAccountLoginInfoSaveAction'
]);

Route::post('/customer-my-account-billing-info-save', [
    'as' => 'customermyaccountbillinginfosave',
    'uses' => 'CustomerController@MyAccountBillingInfoSaveAction'
]);

Route::post('/customer-my-account-shipping-info-save', [
    'as' => 'customermyaccountshippinginfosave',
    'uses' => 'CustomerController@MyAccountShippingInfoSaveAction'
]);


Route::get('/customer-add-traveler', [
    'as' => 'customeraddtraveler',
    'uses' => 'CustomerController@customerAddTravelerAction'
]);

Route::post('/customer-add-traveler-save', [
    'as' => 'customeraddtravelersave',
    'uses' => 'CustomerController@customerAddTravelerSaveAction'
]);

Route::get('/customer-add-traveler-edit/{id}', [
    'as' => 'customeraddtraveleredit',
    'uses' => 'CustomerController@customerAddTravelerEditAction'
]);

Route::patch('/customer-add-traveler-update/{id}', [
    'as' => 'customeraddtravelerupdate',
    'uses' => 'CustomerController@customerAddTravelerUpdateAction'
]);

//-----Card Info start-----
Route::get('/customer-add-card', [
    'as' => 'customeraddcard',
    'uses' => 'CustomerController@customerAddcardAction'
]);


Route::post('/customer-add-card-save', [
    'as' => 'customeraddcardsave',
    'uses' => 'CustomerController@customerAddcardSaveAction'
]);

Route::get('/customer-add-card/{trac}', [
    'as' => 'customeraddcardedit',
    'uses' => 'CustomerController@customerEditcardAction'
]);

Route::patch('/customer-add-card-update/{trac}', [
    'as' => 'customeraddcardupdate',
    'uses' => 'CustomerController@customerUpdatecardAction'
]);


//-----Card Info start-----











//----------Frontend End-------------












//Route::get('/admin', function () {
//    return view('admindash');
//});

Route::auth();
Route::get('/home', 'HomeController@index');


Route::get('/admin', [
    'as' => 'admindashboard',
    'uses' => 'admin\AdminController@adminDashboardAction'
]);

Route::get('/agency', [
    'as' => 'agencydashboard',
    'uses' => 'admin\AdminController@agencyDashboardAction'
]);

Route::get('/agent', [
    'as' => 'agentdashboard',
    'uses' => 'admin\AdminController@agentDashboardAction'
]);





//--------Register- Start-------
Route::get('user/register', [
    'as' => 'userregister',
    'uses' => 'Auth\AuthController@userregister'
]);


Route::post('user/registersave', [
    'as' => 'userregistersave',
    'uses' => 'Auth\AuthController@userregistersave'
]);


//--------Register- End-------







//----User-Start--



Route::get('user/new', [
    'as' => 'newuser',
    'uses' => 'UserController@newAction'
]);


Route::post('user/create', [
    'as' => 'createuser',
    'uses' => 'UserController@createAction'
]);


Route::get('user/index', [
    'as' => 'allusers',
    'uses' => 'UserController@index'
]);

Route::get('user/id/{id}', [
    'as' => 'useredit',
    'uses' => 'UserController@edit'
]);

Route::patch('user/update/{id}', [
    'as' => 'userupdate',
    'uses' => 'UserController@update'
]);


Route::delete('user/delete/{id}', [
    'as' => 'userdelete',
    'uses' => 'UserController@destroy'
]);

//----User-End--

Route::post('/dologin', [
    'as' => 'custlogin',
    'uses' => 'Auth\AuthController@doLogin'
]);






//----Role-Start--

Route::get('role/new', [
    'as' => 'newrole',
    'uses' => 'RoleController@newAction'
]);





Route::get('role/index', [
    'as' => 'allroles',
    'uses' => 'RoleController@indexAction'
]);

Route::post('role/create', [
    'as' => 'createrole',
    'uses' => 'RoleController@createAction'
]);

Route::get('role/id/{id}', [
    'as' => 'roleedit',
    'uses' => 'RoleController@editAction'
]);

Route::patch('role/update/{id}', [
    'as' => 'roleupdate',
    'uses' => 'RoleController@updateAction'
]);

Route::delete('role/delete/{id}', [
    'as' => 'roledelete',
    'uses' => 'RoleController@destroyAction'
]);

//----Role-End--



//----Permission---Start--

Route::get('permission/index', [
    'as' => 'allpermission',
    'uses' => 'PermissionController@indexAction'
]);

Route::get('permission/new', [
    'as' => 'newpermission',
    'uses' => 'PermissionController@newAction'
]);

Route::post('permission/create', [
    'as' => 'createpermission',
    'uses' => 'PermissionController@createAction'
]);



Route::get('permission/id/{id}', [
    'as' => 'permissionedit',
    'uses' => 'PermissionController@editAction'
]);

Route::patch('permission/update/{id}', [
    'as' => 'permissionupdate',
    'uses' => 'PermissionController@updateAction'
]);

Route::delete('permission/delete/{id}', [
    'as' => 'permissiondelete',
    'uses' => 'PermissionController@destroyAction'
]);

//----Permission---End--


//---------Banner Image Start-------
Route::get('bannerimage/index', [
    'as' => 'allbannerimage',
    'uses' => 'BannerimageController@indexAction'
]);


Route::get('bannerimage/new', [
    'as' => 'newbannerimage',
    'uses' => 'BannerimageController@newAction'
]);


Route::post('bannerimage/create', [
    'as' => 'createbannerimage',
    'uses' => 'BannerimageController@createAction'
]);


Route::get('bannerimage/id/{id}', [
    'as' => 'bannerimageedit',
    'uses' => 'BannerimageController@editAction'
]);

Route::patch('bannerimage/update/{id}', [
    'as' => 'bannerimageupdate',
    'uses' => 'BannerimageController@updateAction'
]);

Route::delete('bannerimage/delete/{id}', [
    'as' => 'bannerimagedelete',
    'uses' => 'BannerimageController@destroyAction'
]);



//---------Banner Image End-------





//-------DOmain Route Start----------

//Route::group(['domain' => '{aaa}.ami.com'], function()
//{
//    Route::get('/', function()
//    {
//        return 'My own domain';
//    });
//});



//Route::group(['domain' => '{subdomain}.{domain}.{tld}'], function(){
//
//    Route::get('/', function($sub, $domain, $tld){
//        return 'subdomain: ' . $sub . '.' . $domain . '.' . $tld;
//    });
//});



//-------DOmain Route End----------



//-------------Agent Start------------

Route::get('agent/agency-add-agent', [
    'as' => 'agencyaddagent',
    'uses' => 'AgencyController@addAgentAction'
]);

Route::get('agent/agency-agent-list', [
    'as' => 'agencyagentlist',
    'uses' => 'AgencyController@agencyAgentListAction'
]);





Route::post('agent/agency-add-agent-save', [
    'as' => 'agencyaddagentsave',
    'uses' => 'AgencyController@addAgentSaveAction'
]);

Route::get('agent/agency-agent-edit/{id}', [
    'as' => 'agencyagentedit',
    'uses' => 'AgencyController@agencyAgentEditAction'
]);

Route::patch('agent/agency-agent-update/{id}', [
    'as' => 'agencyagentupdate',
    'uses' => 'AgencyController@agencyAgentUpdateAction'
]);




//-------------Agent End--------------



//----Pcategory---Start--

Route::get('pcategory/index', [
    'as' => 'allpcategory',
    'uses' => 'PcategoryController@indexAction'
]);

Route::get('pcategory/new', [
    'as' => 'newpcategory',
    'uses' => 'PcategoryController@newAction'
]);




Route::post('pcategory/create', [
    'as' => 'createpcategory',
    'uses' => 'PcategoryController@createAction'
]);



Route::get('pcategory/id/{id}', [
    'as' => 'pcategoryedit',
    'uses' => 'PcategoryController@editAction'
]);

Route::patch('pcategory/update/{id}', [
    'as' => 'pcategoryupdate',
    'uses' => 'PcategoryController@updateAction'
]);

Route::delete('pcategory/delete/{id}', [
    'as' => 'pcategorydelete',
    'uses' => 'PcategoryController@destroyAction'
]);

//----Pcategory---End--


//----Tpackage---Start--

Route::get('tpackage/index', [
    'as' => 'alltpackage',
    'uses' => 'TpackageController@indexAction'
]);

Route::get('tpackage/new', [
    'as' => 'newtpackage',
    'uses' => 'TpackageController@newAction'
]);


Route::post('tpackage/create', [
    'as' => 'createtpackage',
    'uses' => 'TpackageController@createAction'
]);



Route::get('tpackage/id/{id}', [
    'as' => 'tpackageedit',
    'uses' => 'TpackageController@editAction'
]);

Route::patch('tpackage/update/{id}', [
    'as' => 'tpackageupdate',
    'uses' => 'TpackageController@updateAction'
]);


Route::delete('tpackage/delete/{id}', [
    'as' => 'tpackagedelete',
    'uses' => 'TpackageController@destroyAction'
]);

//----Tpackage---End--


//----Jcontinent---Start--

Route::get('jcontinent/index', [
    'as' => 'alljcontinent',
    'uses' => 'JcontinentController@indexAction'
]);

Route::get('jcontinent/new', [
    'as' => 'newjcontinent',
    'uses' => 'JcontinentController@newAction'
]);




Route::post('jcontinent/create', [
    'as' => 'createjcontinent',
    'uses' => 'JcontinentController@createAction'
]);


Route::get('jcontinent/id/{id}', [
    'as' => 'jcontinentedit',
    'uses' => 'JcontinentController@editAction'
]);

Route::patch('jcontinent/update/{id}', [
    'as' => 'jcontinentupdate',
    'uses' => 'JcontinentController@updateAction'
]);

Route::delete('jcontinent/delete/{id}', [
    'as' => 'jcontinentdelete',
    'uses' => 'JcontinentController@destroyAction'
]);

//----Jcontinent---End--


//----Jcountry---Start--

Route::get('jcountry/index', [
    'as' => 'alljcountry',
    'uses' => 'JcountryController@indexAction'
]);

Route::get('jcountry/new', [
    'as' => 'newjcountry',
    'uses' => 'JcountryController@newAction'
]);




Route::post('jcountry/create', [
    'as' => 'createjcountry',
    'uses' => 'JcountryController@createAction'
]);


Route::get('jcountry/id/{id}', [
    'as' => 'jcountryedit',
    'uses' => 'JcountryController@editAction'
]);

Route::patch('jcountry/update/{id}', [
    'as' => 'jcountryupdate',
    'uses' => 'JcountryController@updateAction'
]);

Route::delete('jcountry/delete/{id}', [
    'as' => 'jcountrydelete',
    'uses' => 'JcountryController@destroyAction'
]);

//----Jcountry---End--


//---------Frontend Tour Start--------


Route::get('/country-tour', [
    'as' => 'country-tour',
    'uses' => 'TourController@allTourAction'
]);


Route::get('/country-tour/{slug}', [
    'as' => 'single-country-tour',
    'uses' => 'TourController@singleCountryTourAction'
]);

Route::get('/all-country-tour', [
    'as' => 'all-country-tour',
    'uses' => 'TourController@allCountryTourAction'
]);


Route::get('/package-details/{slug}', [
    'as' => 'package-details',
    'uses' => 'TourController@packageDetailsAction'
]);

//---------Frontend Tour End--------



//----Package Add to Cart Strat---------------
Route::get('/customer-login-for-package/{slug}', [
    'as' => 'customerloginforpackage',
    'uses' => 'CartController@customerLoginForPackageAction'
]);


Route::post('/customer-login-for-package-do', [
    'as' => 'customerpackagelogindo',
    'uses' => 'Auth\AuthController@customerpackageLoginDoAction'
]);


Route::get('/cart-order', [
    'as' => 'cartorder',
    'uses' => 'CartController@cartOrderAction'
]);


//----Package Add to Cart End---------------


//============Facebook Login Strat=========================
//Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
//Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('auth/facebook', [
    'as' => 'tofacebook',
    'uses' => 'Auth\AuthController@redirectToProvider'
]);

//Route::get('/auth/facebook/callback', [
//    'as' => 'fromfacebook',
//    'uses' => 'Auth\AuthController@handleProviderCallback'
//]);

Route::get('/auth/facebook/callback', [
    'as' => 'fromfacebook',
    'uses' => 'CustomerController@handleProviderCallback'
]);



//============Facebook Login End===========================


//=================Send Mail================================

Route::post('/send', 'EmailController@send');

Route::get('/number', 'EmailController@numnerAction');


//=================Send Mail================================


