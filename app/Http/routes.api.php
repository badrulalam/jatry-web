<?php
/*
|--------------------------------------------------------------------------
| Web Router
|--------------------------------------------------------------------------
*/


/*login and registration api*/
Route::group(['prefix' => 'api', 'middleware' => ['cors']], function () {
    //authenticate api for get jwt token
    Route::post('authenticate', 'Auth\JwtAuthenticateController@authenticate');

    //register api for regitraion and get jwt token
    Route::post('register', 'Auth\JwtAuthenticateController@register');
//facebook login register
    Route::post('fb-register', 'CustomerController@handleProviderCallbackJWT');

    Route::post('trigger', 'TravelportController@triggerActionReal');
    Route::post('trigger/sample', 'TravelportController@triggerAction');
    Route::post('trigger/validate', 'TravelportController@triggerValidateAction');
    Route::post('uapi', 'TravelportController@uapiAction');
    
    
    Route::post('seatmap', 'TravelportController@getSeatMap');
    Route::post('price-req', 'TravelportController@getAirPriceReq');



    Route::get('cart', 'CartController@getCart');
    Route::get('cartinfo', 'CartController@getInfoCart');

    Route::put('cart', 'CartController@setCart');
    Route::put('cartinfo', 'CartController@setInfoCart');


    Route::post('book-checkout-complete', 'IntbookingController@CheckoutCompleteAction');

    Route::post('purchase-checkout-complete', 'SaleController@CheckoutCompleteAction');

    Route::post('sale-all-invoice', 'SaleController@SaleAllInvoiceAction');
    Route::post('click-invoice', 'SaleController@ClickInvoiceAction');


    Route::post('air-ticket', 'CartController@airTicketAddAction');

    Route::get('air-ticket', 'CartController@airTicketGetAction');

    Route::put('air-ticket', 'CartController@airTicketSingleUpdateAction');
    Route::delete('air-ticket/delete', 'CartController@airTicketSingleDeleteByParamsAction');



    Route::get('my-traveller', 'PassengerforselectController@getPassengerListByUser');
    Route::get('traveller-card', 'PassengerforselectController@getCardListByUser');



    Route::get('/country-tour/{slug}', [
        'uses' => 'TourController@singleCountryTourApiAction'
    ]);

    Route::get('/all-country-tour', [
        'uses' => 'TourController@allCountryTourApiAction'
    ]);


//    Route::get('/countriestour/{}', [
//        'uses' => 'TourController@allCountryTourApiAction'
//    ]);

    Route::get('/select-package-details/{slug}', [
        'uses' => 'TourController@selectPackageDetailsApiAction'
    ]);


//    --------Add to cart Package Start---

    Route::get('/package-add-to-cart/{slug}/{qty}', [
        'uses' => 'CartController@packageAddToCartAction'
    ]);

//    --------Add to cart Package End---

    Route::get('/cart-order-details', [
        'uses' => 'CartController@cartOrderDetailsAction'
    ]);

    Route::get('/package-edit-to-cart/{slug}/{qty}', [
        'uses' => 'CartController@packageEditToCartAction'
    ]);

    Route::get('image/{folder}/{file}', 'Auth\JwtAuthenticateController@getPictureAction');
    Route::get('/countries', function () {
        return Countries::getList('en', 'xml');
    });


//    ----------Domestic Start----------------------------
    Route::get('/city-list', [
        'uses' => 'DomcityController@cityListAction'
    ]);

    Route::post('/domestic-trigger', 'DomflightController@triggerValidateAction');

//    ----------Domestic End----------------------------

});


/*login and registration api*/
Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.auth']], function () {


    //token check
    Route::post('token-check', 'Auth\JwtAuthenticateController@checkRoles');

    //password change
    Route::post('change-password', 'UserController@changePasswordSaveJWTAction');


    /*User's traveller*/
    Route::get('traveller', 'PassengerforselectJWTController@getPassengerListByUser');
    Route::get('traveller/{id}', 'PassengerforselectJWTController@getSingleTravellerJWT');
    Route::post('traveller', 'PassengerforselectJWTController@createSingleTravellerJWT');
    Route::match(['put', 'patch'], 'traveller/{id}', 'PassengerforselectJWTController@updateSingleTravellerJWT');
    Route::delete('traveller/{id}', 'PassengerforselectJWTController@deleteSingleTravellerJWT');


    Route::get('credit-card', 'CustomerController@getCreditCardListByUserJWT');
    Route::get('credit-card/{trac}', 'CustomerController@getSingleCreditCardJWT');
    Route::post('credit-card', 'CustomerController@createSingleCreditCardJWT');
    Route::match(['put', 'patch'], 'credit-card/{trac}', 'CustomerController@updateSingleCreditCardJWT');
//    Route::delete('credit-card/{trac}', 'CustomerController@deleteSingleCreditCardJWT');



    Route::post('booking', 'CartController@airTicketAddAction')->middleware('jwt.auth');
    Route::get('booking', 'CartController@airTicketGetAction')->middleware('jwt.auth');
    Route::post('booking-update', 'CartController@airTicketSingleUpdateJWTAction')->middleware('jwt.auth');


    Route::get('profile', 'CustomerController@getProfileJWT')->middleware('jwt.auth'); //myprofile
    Route::put('profile', 'CustomerController@setProfileJWT')->middleware('jwt.auth'); //myprofile
    //
    
    Route::get('shipping-address', 'CustomerController@getShippingInfoActionJWT')->middleware('jwt.auth'); //myprofile
    Route::match(['put', 'patch'], 'shipping-address', 'CustomerController@setShippingInfoActionJWT')->middleware('jwt.auth'); //myprofile


});
Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.refresh']], function () {
    //token check
    Route::post('token-refresh', 'Auth\JwtAuthenticateController@tokenRefresh');

});


Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.auth']], function () {


//    flight/booking-status return current booking and canceled booking
    Route::get('flight/current-booking', 'FlightController@getCurrentBooingListActionJWT')->middleware('jwt.auth');
    Route::get('flight/current-booking/view/{invoice}', 'FlightController@viewCurrentBooingSingleActionJWT')->middleware('jwt.auth'); //Route::get('ticket-booking/{invoice}', [
    Route::post('flight/current-booking/cancel/{invoice}', 'FlightController@cancelCurrentBooingSingleActionJWT')->middleware('jwt.auth'); // Route::get('ticket-booking-cancle/{invoice}', [
    Route::post('flight/current-booking/purchase/{invoice}', 'SaleController@BookingTicketPurchaseActionJWT')->middleware('jwt.auth'); //Route::get('booking-ticket-purchase/{invoice}', [

    Route::get('flight/canceled-booking', 'FlightController@getCanceledBooingListActionJWT')->middleware('jwt.auth');
    Route::get('flight/canceled-booking/view/{invoice}', 'FlightController@viewCanceledBooingSingleActionJWT')->middleware('jwt.auth'); //Route::get('ticket-booking/{invoice}', [
});