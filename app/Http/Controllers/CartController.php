<?php

namespace App\Http\Controllers;

use App\Cartitemair;
use App\Cartitemairpassenger;
use App\Cartairroute;
use App\Cartairroutesegment;

use App\Passengerforselect;
use Illuminate\Http\Request;

use App\Http\Requests;
//use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Cartitempkg;
use DB;
use App\User;
use App\Customer;
use App\Role;
use App\Pcategory;
use App\Jcontinent;
use App\Jcountry;
use App\Tpackage;
use App\Routesearch;
use Auth;
use Session;
use Validator;

class CartController extends Controller
{
    //
    public function getCart()
    {
        if (Auth::guest()) {
            return 'not login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;
//                $cart->cart_content = "[]";
                $cart->save();
                return $cart->get();
            } else {
                return Auth::user()->cart->get();
            }

        }
//        return Auth::user()->cart;
    }

    public function getInfoCart()
    {
        if (Auth::guest()) {
            return 'not login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;

            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;
                $cart->save();
            }
            return Auth::user()->cart;
        }
//        return Auth::user()->cart;
    }

    public function setCart(Request $request)
    {
        $content = $request->input('content');
        if (Auth::guest()) {
            return 'not login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if ($cart) {
                $cart->cart_content = $content;
                $cart->save();
                return $cart->cart_content;
            } else {
                $cart_new = new Cart();
                $cart_new->user_id = Auth::user()->id;
                $cart_new->cart_content = $content;
                $cart_new->save();
                return $cart_new->cart_content;
            }
        }
    }

    public function setInfoCart(Request $request)
    {
        if (Auth::guest()) {
            return 'not login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if ($cart) {
                $cart->billing_firstname = $request->input('billing_firstname');
                $cart->billing_lastname = $request->input('billing_lastname');
                $cart->billing_email = $request->input('billing_email');
                $cart->billing_address = $request->input('billing_address');
                $cart->billing_city = $request->input('billing_city');
                $cart->billing_postalcode = $request->input('billing_postalcode');
                $cart->billing_country = $request->input('billing_country');
                $cart->billing_state = $request->input('billing_state');
                $cart->billing_phone = $request->input('billing_phone');
                $cart->shipping_firstname = $request->input('shipping_firstname');
                $cart->shipping_lastname = $request->input('shipping_lastname');
                $cart->shipping_email = $request->input('shipping_email');
                $cart->shipping_address = $request->input('shipping_address');
                $cart->shipping_city = $request->input('shipping_city');
                $cart->shipping_postalcode = $request->input('shipping_postalcode');
                $cart->shipping_country = $request->input('shipping_country');
                $cart->shipping_state = $request->input('shipping_state');
                $cart->shipping_phone = $request->input('shipping_phone');


                $cart->save();
                return $cart->cart_content;
            } else {
                $cart_new = new Cart();
                $cart_new->user_id = Auth::user()->id;


                $cart_new->billing_firstname = $request->input('billing_firstname');
                $cart_new->billing_lastname = $request->input('billing_lastname');
                $cart_new->billing_email = $request->input('billing_email');
                $cart_new->billing_address = $request->input('billing_address');
                $cart_new->billing_city = $request->input('billing_city');
                $cart_new->billing_postalcode = $request->input('billing_postalcode');
                $cart_new->billing_country = $request->input('billing_country');
                $cart_new->billing_state = $request->input('billing_state');
                $cart_new->billing_phone = $request->input('billing_phone');
                $cart_new->shipping_firstname = $request->input('shipping_firstname');
                $cart_new->shipping_lastname = $request->input('shipping_lastname');
                $cart_new->shipping_email = $request->input('shipping_email');
                $cart_new->shipping_address = $request->input('shipping_address');
                $cart_new->shipping_city = $request->input('shipping_city');
                $cart_new->shipping_postalcode = $request->input('shipping_postalcode');
                $cart_new->shipping_country = $request->input('shipping_country');
                $cart_new->shipping_state = $request->input('shipping_state');
                $cart_new->shipping_phone = $request->input('shipping_phone');

                $cart_new->save();
                return $cart_new->cart_content;
            }
        }
    }


    public function packageAddToCartAction(Request $request, $slug, $qty)
    {
//        $content = $request->input('content');
        if (Auth::guest()) {
            return 'not-login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug)->first();
            if ($cart) {
//                $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug)->first();
                $cartitem = Cartitempkg::where('cart_id', $cart->id)->where('tpackage_id', $tpackage->id)->first();
                if ($cartitem) {
                    $cartitem->quantity = ($cartitem->quantity + $qty);
                    $cartitem->save();

                    $totalItem = 0;
                    $totalAmount = 0;
                    foreach ($cart->cartitempkg as $cpkg) {
                        $totalItem = ($totalItem + $cpkg->quantity);
                        $totalAmount = ($totalAmount + (Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first()->price * $cpkg->quantity));
                    }

                    $totalFilt = array(
                        'totalItem' => $totalItem,
                        'totalAmount' => $totalAmount
                    );

                    $dataArr = array(
                        'aircart' => $cart,
                        'addedPackagecart' => $cartitem,
                        'tpackage' => $tpackage,
                        'totalFilt' => $totalFilt
                    );
                    return $dataArr;
                } else {
                    $newcartitem = new Cartitempkg();
                    $newcartitem->tpackage_id = $tpackage->id;
                    $newcartitem->cart_id = $cart->id;
                    $newcartitem->quantity = $qty;
                    $newcartitem->save();

                    $totalItem = 0;
                    $totalAmount = 0;
                    foreach ($cart->cartitempkg as $cpkg) {
                        $totalItem = ($totalItem + $cpkg->quantity);
                        $totalAmount = ($totalAmount + (Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first()->price * $cpkg->quantity));
                    }

                    $totalFilt = array(
                        'totalItem' => $totalItem,
                        'totalAmount' => $totalAmount
                    );

                    $dataArr = array(
                        'aircart' => $cart,
                        'addedPackagecart' => $newcartitem,
                        'tpackage' => $tpackage,
                        'totalFilt' => $totalFilt
                    );
                    return $dataArr;
                }


            } else {
                $cart_new = new Cart();
                $cart_new->user_id = Auth::user()->id;
                $cart_new->save();

                $newcartitem = new Cartitempkg();
                $newcartitem->tpackage_id = $tpackage->id;
                $newcartitem->cart_id = $cart_new->id;
                $newcartitem->quantity = $qty;
                $newcartitem->save();


                $totalItem = 0;
                $totalAmount = 0;

                foreach ($cart_new->cartitempkg as $cpkg) {
                    $totalItem = ($totalItem + $cpkg->quantity);
                    $totalAmount = ($totalAmount + (Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first()->price * $cpkg->quantity));
                }

                $totalFilt = array(
                    'totalItem' => $totalItem,
                    'totalAmount' => $totalAmount
                );

                $airCart = array();


                $dataArr = array(
                    'aircart' => $airCart,
                    'addedPackagecart' => $newcartitem,
                    'tpackage' => $tpackage,
                    'totalFilt' => $totalFilt
                );
                return $dataArr;
            }
        }


    }


    public function customerLoginForPackageAction(Request $request, $slug)
    {
        return view('customer.customerloginforpackage', compact('slug'));
    }


    public function cartOrderAction(Request $request)
    {
        return view('frontend.cart-order');
    }

    public function cartOrderDetailsAction(Request $request)
    {
        if (Auth::guest()) {
            return 'not-login';
        } elseif (Auth::user()) {

            $cart = Auth::user()->cart;

            if ($cart) {


                $totalItem = 0;
                $totalAmount = 0;
                if ($cart->cartitempkg->count() > 0) {

                    foreach ($cart->cartitempkg as $cpkg) {
                        $cpkg['package'] = Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first(['code', 'title', 'image1', 'price', 'slug']);
                        $totalItem = ($totalItem + $cpkg->quantity);
                        $totalAmount = ($totalAmount + (Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first()->price * $cpkg->quantity));

                    }

                    $totalFilt = array(
                        'totalItem' => $totalItem,
                        'totalAmount' => $totalAmount
                    );


                    $orderArr = array(
                        'cartpkgArr' => $cart->cartitempkg,
                        'totalFilt' => $totalFilt
                    );
//                    return 'package-hey';
                    return $orderArr;
                } else {
                    return 'package-empty-cart';
                }


            } else {
                return 'empty-cart';
            }


//            return Auth::user()->cart->cart_content;
        }
//        return Auth::user()->cart;
    }


    public function packageEditToCartAction(Request $request, $slug, $qty)
    {
//        $content = $request->input('content');
        if (Auth::guest()) {
            return 'not-login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug)->first();
            if ($cart) {
//                $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug)->first();
                $cartitem = Cartitempkg::where('cart_id', $cart->id)->where('tpackage_id', $tpackage->id)->first();
                if ($cartitem) {
                    $cartitem->quantity = $qty;
                    $cartitem->save();

                    $totalItem = 0;
                    $totalAmount = 0;
                    foreach ($cart->cartitempkg as $cpkg) {
                        $totalItem = ($totalItem + $cpkg->quantity);
                        $totalAmount = ($totalAmount + (Tpackage::where('isactive', 1)->where('id', $cpkg->tpackage_id)->first()->price * $cpkg->quantity));
                    }

                    $totalFilt = array(
                        'totalItem' => $totalItem,
                        'totalAmount' => $totalAmount
                    );

                    $dataArr = array(
//                        'aircart'=>$cart,
//                        'addedPackagecart' =>$cartitem,
//                        'tpackage' => $tpackage,
                        'totalFilt' => $totalFilt
                    );
                    return $dataArr;
                }


            }
        }


    }

    public function airTicketAddAction(Request $request)
    {

        $SearchParams = (array)$request->input('SearchParams');
        $AirPricePoints = (array)$request->input('AirPricePoint');
//return gettype($SearchParams["adults"]);


        if (Auth::guest()) {
            return response()->json('not-login', 401);
        }
        if (Auth::user()) {

            $validate = $this->validateCartParams($request);
            if ($validate->fails()) {
                $errorMessage = [];
                $errorMessage['invalidAttributes'] = $validate->errors();
                return response()->json($errorMessage, 400);
            }
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;
                $cart->save();
            }
            if ($cart) {
//           delete cart item air here
                $all_cart_item = Cartitemair::where('cart_id', $cart->id)->with('route')->with('passenger')->with('route.segment')->get();
                foreach ($all_cart_item as $citem) {
                    foreach ($citem->passenger as $cpass) {
                        $cpass->delete();
                    }
                    foreach ($citem->route as $croute) {
                        foreach ($croute->segment as $cseg) {
                            $cseg->delete();
                        }
                        $croute->delete();
                    }
                    $citem->delete();
                }
                $cart->billing_firstname = '';
                $cart->billing_lastname = '';
                $cart->billing_email = '';
                $cart->billing_address = '';
                $cart->billing_city = '';
                $cart->billing_postalcode = '';
                $cart->billing_country = '';
                $cart->billing_state = '';
                $cart->billing_phone = '';
                $cart->card_number = '';
                $cart->cardholder = '';
                $cart->exp_date = '';
                $cart->cvv = '';
                $cart->save();

                /*create cart item air*/
                $cart_item_air = new Cartitemair();
                $cart_item_air->cart_id = $cart->id;


//                $cart_item_air->Price = $AirPricePoints["Price"];

                $cart_item_air->Key = isset($AirPricePoints["Key"]) ? $AirPricePoints["Key"] : null;
                $cart_item_air->TotalPrice = isset($AirPricePoints["TotalPrice"]) ? $AirPricePoints["TotalPrice"] : null;
                $cart_item_air->BasePrice = isset($AirPricePoints["BasePrice"]) ? $AirPricePoints["BasePrice"] : null;
                $cart_item_air->ApproximateTotalPrice = isset($AirPricePoints["ApproximateTotalPrice"]) ? $AirPricePoints["ApproximateTotalPrice"] : null;
                $cart_item_air->ApproximateBasePrice = isset($AirPricePoints["ApproximateBasePrice"]) ? $AirPricePoints["ApproximateBasePrice"] : null;
                $cart_item_air->EquivalentBasePrice = isset($AirPricePoints["EquivalentBasePrice"]) ? $AirPricePoints["EquivalentBasePrice"] : null;
                $cart_item_air->Taxes = isset($AirPricePoints["Taxes"]) ? $AirPricePoints["Taxes"] : null;
                $cart_item_air->ApproximateTaxes = isset($AirPricePoints["ApproximateTaxes"]) ? $AirPricePoints["ApproximateTaxes"] : null;
                $cart_item_air->PolicyExclusion = isset($AirPricePoints["PolicyExclusion"]) ? ($AirPricePoints["PolicyExclusion"] == "true" ? true : false) : false;
                $cart_item_air->Refundable = isset($AirPricePoints["Refundable"]) ? ($AirPricePoints["Refundable"] == "true" ? true : false) : false;
                $cart_item_air->PlatingCarrier = isset($AirPricePoints["PlatingCarrier"]) ? $AirPricePoints["PlatingCarrier"] : null;
                $cart_item_air->TravelTime = isset($AirPricePoints["Journey"]["TravelTime"]) ? $AirPricePoints["Journey"]["TravelTime"] : null;

                $cart_item_air->adults = isset($SearchParams["adults"]) ? $SearchParams["adults"] : null;
                $cart_item_air->childs = isset($SearchParams["childs"]) ? $SearchParams["childs"] : null;
                $cart_item_air->infant = isset($SearchParams["infant"]) ? $SearchParams["infant"] : null;
                $cart_item_air->tripType = isset($SearchParams["tripType"]) ? $SearchParams["tripType"] : null;
                $cart_item_air->noOfSegments = isset($SearchParams["noOfSegments"]) ? $SearchParams["noOfSegments"] : null;
                $cart_item_air->class = isset($SearchParams["class"]) ? $SearchParams["class"] : null;


                $cart_item_air->json_content = json_encode(
                    array(
                        'AirPricePoint' => $AirPricePoints,
                        'SearchParams' => $SearchParams,
                    )
                );
                $cart_item_air->save();
                if (!$cart_item_air->id) {
                    return response()->json('error', 500);
                }
                $adults = $SearchParams["adults"];
                $childs = $SearchParams["childs"];
                $infant = $SearchParams["infant"];

                for ($i = 0; $i < $adults; $i++) {
                    $new_passenger = new Cartitemairpassenger();
                    $new_passenger->cartitemair_id = $cart_item_air->id;
                    $new_passenger->passenger_order = $i;
                    $new_passenger->passenger_type = "adults";
                    $new_passenger->save();

                }
                for ($i = 0; $i < $childs; $i++) {
                    $new_passenger = new Cartitemairpassenger();
                    $new_passenger->cartitemair_id = $cart_item_air->id;
                    $new_passenger->passenger_order = $i;
                    $new_passenger->passenger_type = "childs";
                    $new_passenger->save();

                }
                for ($i = 0; $i < $infant; $i++) {
                    $new_passenger = new Cartitemairpassenger();
                    $new_passenger->cartitemair_id = $cart_item_air->id;
                    $new_passenger->passenger_order = $i;
                    $new_passenger->passenger_type = "infant";
                    $new_passenger->save();

                }

                if (isset($AirPricePoints["Journey"]["Routes"])) {
                    for ($i = 0; $i < count($AirPricePoints["Journey"]["Routes"]); $i++) {
                        $route = $AirPricePoints["Journey"]["Routes"][$i];

                        $cart_route = new Cartairroute();
                        $cart_route->cartitemair_id = $cart_item_air->id;

                        $cart_route->ArrivalTime = isset($route["ArrivalTime"]) ? $route["ArrivalTime"] : null;
                        $cart_route->DepartureTime = isset($route["DepartureTime"]) ? $route["DepartureTime"] : null;
                        $cart_route->Destination = isset($route["Destination"]) ? $route["Destination"] : null;
//                        $cart_route->Duration = isset($route["Duration"]) ? $route["Duration"] : null;
                        $cart_route->Origin = isset($route["Origin"]) ? $route["Origin"] : null;
                        $cart_route->Stops = isset($route["Stops"]) ? $route["Stops"] : null;
                        $cart_route->ViaStops = isset($route["ViaStops"]) ? $route["ViaStops"] : null;
                        $cart_route->json_content = json_encode($route);
                        $cart_route->save();

//                        -------Route Search Start---
                        $findRouteSearch = Routesearch::where('Origin', $cart_route->Origin)->where('Destination', $cart_route->Destination)->first();
                        if(!$findRouteSearch){
                            $routesearch = new Routesearch();
                            $routesearch->Origin = $cart_route->Origin;
                            $routesearch->Destination = $cart_route->Destination;
                            $routesearch->user_id = Auth::user()->id;
                            $routesearch->save();
                        }


//                        -------Route Search End-----



                        if (isset($route["AirSegment"])) {
                            for ($j = 0; $j < count($route["AirSegment"]); $j++) {
                                $segment = $route["AirSegment"][$j];

                                $air_segment = new Cartairroutesegment();

                                $air_segment->cartairroute_id = $cart_route->id;

                                $air_segment->Key = isset($segment["Key"]) ? $segment["Key"] : null;
                                $air_segment->Group = isset($segment["Group"]) ? $segment["Group"] : null;
                                $air_segment->Carrier = isset($segment["Carrier"]) ? $segment["Carrier"] : null;
                                $air_segment->FlightNumber = isset($segment["FlightNumber"]) ? $segment["FlightNumber"] : null;
                                $air_segment->Origin = isset($segment["Origin"]) ? $segment["Origin"] : null;
                                $air_segment->Destination = isset($segment["Destination"]) ? $segment["Destination"] : null;
                                $air_segment->DepartureTime = isset($segment["DepartureTime"]) ? $segment["DepartureTime"] : null;
                                $air_segment->ArrivalTime = isset($segment["ArrivalTime"]) ? $segment["ArrivalTime"] : null;
                                $air_segment->FlightTime = isset($segment["FlightTime"]) ? $segment["FlightTime"] : null;
                                $air_segment->Distance = isset($segment["Distance"]) ? $segment["Distance"] : null;
                                $air_segment->DistanceUnits = isset($segment["DistanceUnits"]) ? $segment["DistanceUnits"] : null;
                                $air_segment->ETicketability = isset($segment["ETicketability"]) ? $segment["ETicketability"] : null;
                                $air_segment->Equipment = isset($segment["Equipment"]) ? $segment["Equipment"] : null;
                                $air_segment->ChangeOfPlane = isset($segment["ChangeOfPlane"]) ? ($segment["ChangeOfPlane"] == "true" ? true : false) : false;
                                $air_segment->ParticipantLevel = isset($segment["ParticipantLevel"]) ? $segment["ParticipantLevel"] : null;
                                $air_segment->LinkAvailability = isset($segment["LinkAvailability"]) ? ($segment["LinkAvailability"] == "true" ? true : false) : false;
                                $air_segment->PolledAvailabilityOption = isset($segment["PolledAvailabilityOption"]) ? $segment["PolledAvailabilityOption"] : null;
                                $air_segment->OptionalServicesIndicator = isset($segment["OptionalServicesIndicator"]) ? ($segment["OptionalServicesIndicator"] == "true" ? true : false) : false;
                                $air_segment->AvailabilitySource = isset($segment["AvailabilitySource"]) ? $segment["AvailabilitySource"] : null;
                                $air_segment->AvailabilityDisplayType = isset($segment["AvailabilityDisplayType"]) ? $segment["AvailabilityDisplayType"] : null;
                                $air_segment->NumberOfStops = isset($segment["NumberOfStops"]) ? $segment["NumberOfStops"] : null;
                                $air_segment->DestinationTerminal = isset($segment["FlightDetails"][0]["FlightDetailsRef"][0]["DestinationTerminal"]) ? $segment["FlightDetails"][0]["FlightDetailsRef"][0]["DestinationTerminal"] : null;
                                $air_segment->TravelTime = isset($segment["FlightDetails"][0]["FlightDetailsRef"][0]["TravelTime"]) ? $segment["FlightDetails"][0]["FlightDetailsRef"][0]["TravelTime"] : null;

                                $air_segment->BookingCode = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["BookingCode"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["BookingCode"] : null;
                                $air_segment->BookingCount = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["BookingCount"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["BookingCount"] : null;
                                $air_segment->CabinClass = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["CabinClass"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["CabinClass"] : null;
                                $air_segment->NotValidBefore = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["NotValidBefore"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["NotValidBefore"] : null;
                                $air_segment->NotValidAfter = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["NotValidAfter"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["NotValidAfter"] : null;
                                $air_segment->FareRuleKey = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["FareRuleKey"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["FareRuleKey"] : null;

                                $air_segment->MaxWeight = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["BaggageAllowance"]["MaxWeight"]["Value"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["BaggageAllowance"]["MaxWeight"]["Value"] : null;
                                $air_segment->WeightUnits = isset($this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["BaggageAllowance"]["MaxWeight"]["Unit"])
                                    ? $this->getFareInfoBySegmentRef($AirPricePoints, $air_segment->Key)["FareInfo"]["BaggageAllowance"]["MaxWeight"]["Unit"] : null;


                                $air_segment->json_content = json_encode($segment);

                                $air_segment->save();
                            }
                        }
                    }
                }
                return response()->json($cart_item_air->with('route')->with('passenger')->with('route.segment')->where('id', $cart_item_air->id)->first(), 200);
            }

        }
    }


    public function airTicketGetAction()
    {

        if (Auth::guest()) {
            return response()->json('not-login', 401);
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;
                $cart->save();
            }

            if ($cart) {
                return response()->json(
                    Cart::where('id', $cart->id)->with('ticket')->with('ticket.route')->with('ticket.passenger')->with('ticket.route.segment')->first()
                    , 200);
            } else {
                return response()->json('cart not fount', 500);
            }


        }
    }

    public function airTicketAllUpdateAction($ticketid)
    {

        if (Auth::guest()) {
            return 'not-login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;

                $cart->save();
            }

            if ($cart) {

                $item = Cartitemair::where('ticket_key', $ticketid)->first();
                if ($item) {
                    $ret = array(
                        "cartItem" => $item,
                        "cart_content" => json_decode(Cartitemair::where('ticket_key', $ticketid)->first()->cart_content),
                        "passenger" => Cartitemairpassenger::where('ticket_key', $ticketid)->get()
                    );

                    return $ret;
                } else {
                    return "not-found";
                }

            }


        }
    }

    public function airTicketSingleUpdateAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'passenger' => 'required',
            'customeradd' => 'required',
            'custpayment' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validator->errors();
//            return response()->json($errorMessage, 400);
        }
        $update_msg = [];

        if (Auth::guest()) {
            return response()->json('not-login', 401);
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;

                $cart->save();
            }

            if ($cart) {

                $update_msg['passenger'] = (bool)$request->input('passenger');
                if ((bool)$request->input('passenger')) {
                    $allPassenger = (array)$request->input('passenger');
                    foreach ($allPassenger as $item) {
                        Cartitemairpassenger::where('cartitemair_id', $cart->cartitemair->id)
                            ->where('id', $item["id"])
                            ->update($item);

                        $ps = Passengerforselect::firstOrNew(
                            array(
                                'user_id' => Auth::user()->id,
                                'first_name' => $item["first_name"],
                                'last_name' => $item["last_name"]
                            )
                        );

                        $ps->user_id = Auth::user()->id;
                        $ps->title = $item["title"];
                        $ps->first_name = $item["first_name"];
                        $ps->middle_name = $item["middle_name"];
                        $ps->last_name = $item["last_name"];
                        $ps->dob = $item["dob"];
                        $ps->nationality = $item["nationality"];
                        $ps->passport_no = $item["passport_no"];
                        $ps->passport_issue_country = $item["passport_issue_country"];
                        $ps->passport_expiry_date = $item["passport_expiry_date"];
                        $ps->passenger_type = $item["passenger_type"];
                        $ps->save();

//            print_r($item);
                    }
                }

                $update_msg['customeradd'] = (bool)$request->input('customeradd');
                if ((bool)$request->input('customeradd')) {
                    $customeradd = (array)$request->input('customeradd');

//                $user= Auth::user();
//                $custuser = \Auth::user();
//                $custuser->customer->first_name = $customeradd['first_name'];
//                $custuser->customer->last_name = $customeradd['last_name'];
//                $custuser->customer->email = $customeradd['email'];
//                $custuser->customer->address = $customeradd['address'];
//                $custuser->customer->city = $customeradd['city'];
//                $custuser->customer->state = $customeradd['state'];
//                $custuser->customer->postalcode = $customeradd['postalcode'];
//                $custuser->customer->country = $customeradd['country'];
//                $custuser->customer->phone = $customeradd['phone'];
//                $custuser->customer->save();

                    $cart->billing_firstname = $customeradd['first_name'];
                    $cart->billing_lastname = $customeradd['last_name'];
//                $cart->billing_email = $customeradd['email'];
                    $cart->billing_address = $customeradd['address'];
                    $cart->billing_city = $customeradd['city'];
                    $cart->billing_postalcode = $customeradd['postalcode'];
//                $cart->billing_country = $customeradd['country'];
//                $cart->billing_state = $customeradd['state'];
                    $cart->billing_phone = $customeradd['phone'];
                }

                $update_msg['custpayment'] = (bool)$request->input('custpayment');
                if ((bool)$request->input('custpayment')) {
                    $custpayment = (array)$request->input('custpayment');
                    $cart->card_number = $custpayment['card_number'];
                    $cart->cardholder = $custpayment['cardholder'];
                    $cart->exp_date = $custpayment['exp_date'];
                    $cart->cvv = $custpayment['cvv'];
                }

                $cart->save();

                return response()->json($update_msg, 200);

//                return response()->json(
//                    Cart::where('id', $cart->id)->with('ticket')->with('ticket.route')->with('ticket.passenger')->with('ticket.route.segment')->first()
//                    , 200);

            } else {
                return response()->json('cart not fount', 500);
            }

        }


    }

    public function airTicketSingleUpdateJWTAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'passenger' => 'required',
            'customeradd' => 'required',
            'custpayment' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validator->errors();
//            return response()->json($errorMessage, 400);
        }
        $update_msg = [];

        if (Auth::guest()) {
            return response()->json('not-login', 401);
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;

                $cart->save();
            }

            if ($cart) {

                $update_msg['passenger'] = (bool)$request->input('passenger');
                if ((bool)$request->input('passenger')) {
                    $allPassenger = (array)$request->input('passenger');

                    $allPassengerValidator = Validator::make($allPassenger, [
                        '*.id' => 'required',
                        '*.first_name' => 'required',
                        '*.last_name' => 'required',
                        '*.dob' => 'date',
                    ]);
                    if ($allPassengerValidator->fails()) {
                        $update_msg['passenger'] = [];
                        $update_msg['passenger']['invalidAttributes'] = $allPassengerValidator->errors();
                    }else{
                        foreach ($allPassenger as $item) {
                            Cartitemairpassenger::where('cartitemair_id', $cart->cartitemair->id)
                                ->where('id', $item["id"])
                                ->update($item);

                            $ps = Passengerforselect::firstOrNew(
                                array(
                                    'user_id' => Auth::user()->id,
                                    'first_name' => $item["first_name"],
                                    'last_name' => $item["last_name"]
                                )
                            );

                            $ps->user_id = Auth::user()->id;
                            $ps->title = $item["title"];
                            $ps->first_name = $item["first_name"];
                            $ps->middle_name = $item["middle_name"];
                            $ps->last_name = $item["last_name"];
                            $ps->dob = $item["dob"];
                            $ps->nationality = $item["nationality"];
                            $ps->passport_no = $item["passport_no"];
                            $ps->passport_issue_country = $item["passport_issue_country"];
                            $ps->passport_expiry_date = $item["passport_expiry_date"];
                            $ps->passenger_type = $item["passenger_type"];
                            $ps->save();

//            print_r($item);
                        }
                    }


                }

                $update_msg['customeradd'] = (bool)$request->input('customeradd');
                if ((bool)$request->input('customeradd')) {

                    $customeradd = (array)$request->input('customeradd');
                    $customeraddValidator = Validator::make($customeradd, [
                        'billing_firstname' => 'required|string',
                        'billing_lastname' => 'required|string',
                        'billing_address' => 'required|string',
                        'billing_city' => 'required|string',
                        'billing_postalcode' => 'required|string',
                        'billing_phone' => 'required|string'
                    ]);
                    if ($customeraddValidator->fails()) {
                        $update_msg['customeradd'] = [];
                        $update_msg['customeradd']['invalidAttributes'] = $customeraddValidator->errors();
                    } else {
                        $cart->billing_firstname = $customeradd['billing_firstname'];
                        $cart->billing_lastname = $customeradd['billing_lastname'];
//                $cart->billing_email = $customeradd['email'];
                        $cart->billing_address = $customeradd['billing_address'];
                        $cart->billing_city = $customeradd['billing_city'];
                        $cart->billing_postalcode = $customeradd['billing_postalcode'];
//                $cart->billing_country = $customeradd['country'];
//                $cart->billing_state = $customeradd['state'];
                        $cart->billing_phone = $customeradd['billing_phone'];
                    }


                }

                $update_msg['custpayment'] = (bool)$request->input('custpayment');
                if ((bool)$request->input('custpayment')) {
                    $custpayment = (array)$request->input('custpayment');
                    $custpaymentValidator = Validator::make($custpayment, [
                        'card_number' => 'required|string',
                        'cardholder' => 'required|string',
                        'exp_date' => 'required|string',
                        'cvv' => 'required|string'
                    ]);
                    if ($custpaymentValidator->fails()) {
                        $update_msg['custpayment'] = [];
                        $update_msg['custpayment']['invalidAttributes'] = $custpaymentValidator->errors();
                    } else {
                        $cart->card_number = $custpayment['card_number'];
                        $cart->cardholder = $custpayment['cardholder'];
                        $cart->exp_date = $custpayment['exp_date'];
                        $cart->cvv = $custpayment['cvv'];
                    }


                }

                $cart->save();

                return response()->json($update_msg, 200);

//                return response()->json(
//                    Cart::where('id', $cart->id)->with('ticket')->with('ticket.route')->with('ticket.passenger')->with('ticket.route.segment')->first()
//                    , 200);

            } else {
                return response()->json('cart not fount', 500);
            }

        }


    }

//    public function airTicketSingleDeleteAction($ticketid){
//        if(Auth::guest()){
//            return 'not-login';
//        }elseif(Auth::user()){
//            $cart = Auth::user()->cart;
//            if(!$cart){
//                $cart = new Cart() ;
//                $cart->user_id = Auth::user()->id;
//
//                $cart->save();
//            }
//
//            if($cart){
//                $dete = Cartitemair::where('ticket_key', $ticketid)->with('passenger')->delete();
//                return $dete;
//
//            }
//
//        }
//
//    }
    public function airTicketSingleDeleteByParamsAction(Request $request)
    {

        if (Auth::guest()) {
            return 'not-login';
        } elseif (Auth::user()) {
            $cart = Auth::user()->cart;
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = Auth::user()->id;

                $cart->save();
            }

            if ($cart) {
                $dete = Cartitemair::where('ticket_key', $request->input('ticketKey'))->with('passenger')->delete();
                return $dete;

            }

        }

    }

    private function getFareInfoBySegmentRef($AirPricePoints, $air_segment_key)
    {
        if (isset($AirPricePoints["Price"]["BookingInfo"]) && is_array($AirPricePoints["Price"]["BookingInfo"])) {
            foreach ($AirPricePoints["Price"]["BookingInfo"] as $key => $val) {
                if (isset($val["SegmentRef"])) {
                    if ($val["SegmentRef"] == $air_segment_key) {
                        return $val;
                    }
                }
            }
        }
    }

    private function validateCartParams($request)
    {
        $validator = Validator::make($request->all(), [
            'AirPricePoint' => 'required',
            'SearchParams' => 'required'


//            'SearchParams.tripType' => 'required|string|size:1|in:O,R,M',
//            'SearchParams.noOfSegments' => 'required|integer|min:1|max:5',
//            'SearchParams.adults' => 'required|integer|min:1|max:9',
//            'SearchParams.childs' => 'required|integer|min:0|max:9',
//            'SearchParams.infant' => 'required|integer|min:0|max:9',
//            'SearchParams.class' => 'required|string',
//            'SearchParams.origin' => 'required_if:SearchParams.tripType,O,R|string|size:3',
//            'SearchParams.destination' => 'required_if:SearchParams.tripType,O,R|string|size:3',
//            'SearchParams.departureDate' => 'required_if:tripType,O,R|date|after:yesterday',
//            'SearchParams.arrivalDate' => 'required_if:tripType,R|date|after:SearchParams.departureDate'
        ]);
        return $validator;

//        if ($validator->fails()) {
//            return $validator;
//        } else {
//            $SearchParamsValidator = Validator::make((array) $request->input('SearchParams'), [
//                'tripType' => 'required|string|size:1|in:O,R,M',
//                'noOfSegments' => 'required|integer|min:1|max:5',
//                'adults' => 'required|integer|min:1|max:9',
//                'childs' => 'required|integer|min:0|max:9',
//                'infant' => 'required|integer|min:0|max:9',
//                'class' => 'required|string',
//                'origin' => 'required_if:tripType,O,R|string|size:3',
//                'destination' => 'required_if:tripType,O,R|string|size:3',
//                'departureDate' => 'required_if:tripType,O,R|date|after:yesterday',
//                'arrivalDate' => 'required_if:tripType,R|date|after:departureDate'
//            ]);
//            return $SearchParamsValidator;
//        }

    }


}
