<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Http\Response;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use App\Cart;

use App\Cardinfo;
use App\Cartitemair;
use App\Cartitemairpassenger;
use Jenssegers\Agent\Agent as MAgent;

class FrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['homeAction','FlightAction', 'DomFlightAction']]);
    }


    public function homeAction()
    {
        $agent = new MAgent();
        if($agent->isMobile()){
            return Redirect::to('http://m.jatry.com');
        }

        return view('frontend.home');
    }
    public function FlightAction()
    {
        return view('frontend.flights');
    }

    public function DomFlightAction()
    {
        return view('frontend.dom-flights');
    }


    public function BookCheckoutUiAction()
    {
        return view('frontend.book-checkoutui');
    }

    public function PurchaseCheckoutUiAction()
    {
        return view('frontend.purchase-checkoutui');
    }

    public function BookConfirmTicketAction()
    {
        $cart =  Auth::user()->cart;
        $cartitemair = Cartitemair::where('cart_id',$cart->id)->first();
        return view('frontend.book-confirmticket', ['cart' => $cart]);
    }

    public function PurchaseConfirmTicketAction()
    {
        $cart =  Auth::user()->cart;
        $cartitemair = Cartitemair::where('cart_id',$cart->id)->first();
        return view('frontend.purchase-confirmticket', ['cart' => $cart]);
    }
    public function CheckoutFinalAction()
    {
        $cart =  Auth::user()->cart;
        return view('frontend.checkoutfinal', compact('cart'));
    }
    public function CheckoutCompleteAction(Request $request)
    {
        $flag = "true";
//        $flag = true;
        return $flag;

    }
public function TicketSaleAction(){
    return "success";
}









}
