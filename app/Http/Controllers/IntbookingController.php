<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;



use App\Intbookairroute;
use App\Intbookairroutesegment;


use Illuminate\Http\Response;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Redirect;
use URL;
use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Intbook;
use App\Cardinfo;
use App\Cartitemair;
use App\Intbookitemair;
use App\Intbookitemairpassenger;





class IntbookingController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth');
    }



    public function CheckoutCompleteAction(Request $request)
    {


        $cart =  Auth::user()->cart;
        $customer =  Auth::user()->customer;


//        $cardinfo = $this->cardByCheck($card_number, $cardholder, $exp_date, $cvv);
        $cardinfo = $this->cardByCheck($cart->card_number, $cart->cardholder, $cart->exp_date, $cart->cvv);
        $intbook = new Intbook();

//        $intbook->order_number = Auth::user()->id.'-'.Auth::user()->cart->id;
        $intbook->order_number = 'BOOK-'.date("Ymd").'-'.time().'-'.Auth::user()->id;
//        $intbook->billing_firstname = $cart->billing_firstname;
//        $intbook->billing_lastname = $cart->billing_lastname;
//        $intbook->billing_email = $cart->billing_email;
//        $intbook->billing_address = $cart->billing_address;
//        $intbook->billing_city = $cart->billing_city;
//        $intbook->billing_postalcode = $cart->billing_postalcode;
//        $intbook->billing_country = $cart->billing_country;
//        $intbook->billing_state = $cart->billing_state;
//        $intbook->billing_phone = $cart->billing_phone;


        $intbook->billing_firstname = $customer->first_name;
        $intbook->billing_lastname = $customer->last_name;
        $intbook->billing_email = $customer->email;
        $intbook->billing_address = $customer->address;
        $intbook->billing_city = $customer->city;
        $intbook->billing_postalcode = $customer->postalcode;
        $intbook->billing_country = $customer->country;
        $intbook->billing_state = $customer->state;
        $intbook->billing_phone = $customer->phone;

//        $intbook->shipping_firstname = $cart->shipping_firstname;
//        $intbook->shipping_lastname = $cart->shipping_lastname;
//        $intbook->shipping_email = $cart->shipping_email;
//        $intbook->shipping_address = $cart->shipping_address;
//        $intbook->shipping_city = $cart->shipping_city;
//        $intbook->shipping_postalcode = $cart->shipping_postalcode;
//        $intbook->shipping_country = $cart->shipping_country;
//        $intbook->shipping_state = $cart->shipping_state;
//        $intbook->shipping_phone = $cart->shipping_phone;

        $intbook->cardinfo_id = $cardinfo;

        $intbook->user_id = Auth::user()->id;
        $intbook->save();


//        =========================New Cart Save Start====================================
        $intbookItemAir = new Intbookitemair();

        $intbookItemAir->Price = $cart->ticket->Price;
        $intbookItemAir->Key = $cart->ticket->Key;
        $intbookItemAir->TotalPrice = $cart->ticket->TotalPrice;
        $intbookItemAir->BasePrice = $cart->ticket->BasePrice;
        $intbookItemAir->ApproximateTotalPrice = $cart->ticket->ApproximateTotalPrice;
        $intbookItemAir->ApproximateBasePrice = $cart->ticket->ApproximateBasePrice;
        $intbookItemAir->EquivalentBasePrice = $cart->ticket->EquivalentBasePrice;
        $intbookItemAir->Taxes = $cart->ticket->Taxes;
        $intbookItemAir->ApproximateTaxes = $cart->ticket->ApproximateTaxes;
        $intbookItemAir->PolicyExclusion = $cart->ticket->PolicyExclusion;
        $intbookItemAir->Refundable = $cart->ticket->Refundable;
        $intbookItemAir->PlatingCarrier = $cart->ticket->PlatingCarrier;
        $intbookItemAir->TravelTime = $cart->ticket->TravelTime;
        $intbookItemAir->adults = $cart->ticket->adults;
        $intbookItemAir->childs = $cart->ticket->childs;
        $intbookItemAir->infant = $cart->ticket->infant;
        $intbookItemAir->tripType = $cart->ticket->tripType;
        $intbookItemAir->noOfSegments = $cart->ticket->noOfSegments;
        $intbookItemAir->class = $cart->ticket->class;
        $intbookItemAir->json_content = $cart->ticket->json_content;
//        $intbookItemAir->saleprice = $cart->cartitemair->saleprice;
        $intbookItemAir->saleprice = 1002.00;
        $intbookItemAir->intbook_id = $intbook->id;
        $intbookItemAir->save();



        foreach($cart->ticket->passenger as $passenger){

            $intbookPassenger = new Intbookitemairpassenger();

            $intbookPassenger->ticket_key = '';
            $intbookPassenger->pnr_number = time();
            $intbookPassenger->title = $passenger->title;
            $intbookPassenger->first_name = $passenger->first_name;
            $intbookPassenger->middle_name = $passenger->middle_name;
            $intbookPassenger->last_name = $passenger->last_name;
            $intbookPassenger->dob = $passenger->dob;
            $intbookPassenger->nationality = $passenger->nationality;
            $intbookPassenger->passport_no = $passenger->passport_no;
            $intbookPassenger->passport_issue_country = $passenger->passport_issue_country;
            $intbookPassenger->passport_expiry_date = $passenger->passport_expiry_date;
            $intbookPassenger->passenger_type = $passenger->passenger_type;
            $intbookPassenger->passenger_order = $passenger->passenger_order;
            $intbookPassenger->intbookitemair_id = $intbookItemAir->id;

            $intbookPassenger->save();

            $passenger->delete();
        }







        foreach($cart->ticket->route as $route){

            $intbookRoute = new Intbookairroute();

            $intbookRoute->Origin = $route->Origin;
            $intbookRoute->Destination = $route->Destination;
            $intbookRoute->DepartureTime = $route->DepartureTime;
            $intbookRoute->ArrivalTime = $route->ArrivalTime;
            $intbookRoute->Duration = $route->Duration;
            $intbookRoute->ViaStops = $route->ViaStops;
            $intbookRoute->Stops = $route->Stops;
            $intbookRoute->json_content = $route->json_content;
            $intbookRoute->intbookitemair_id = $intbookItemAir->id;
            $intbookRoute->save();

            foreach($route->segment as $segment){

                $intbookRouteSegment = new Intbookairroutesegment();

                $intbookRouteSegment->Key = $segment->Key;
                $intbookRouteSegment->Group = $segment->Group;
                $intbookRouteSegment->Carrier = $segment->Carrier;
                $intbookRouteSegment->FlightNumber = $segment->FlightNumber;
                $intbookRouteSegment->Origin = $segment->Origin;
                $intbookRouteSegment->Destination = $segment->Destination;
                $intbookRouteSegment->DepartureTime = $segment->DepartureTime;
                $intbookRouteSegment->ArrivalTime = $segment->ArrivalTime;
                $intbookRouteSegment->FlightTime = $segment->FlightTime;
                $intbookRouteSegment->Distance = $segment->Distance;
                $intbookRouteSegment->DistanceUnits = $segment->DistanceUnits;
                $intbookRouteSegment->ETicketability = $segment->ETicketability;
                $intbookRouteSegment->Equipment = $segment->Equipment;
                $intbookRouteSegment->ChangeOfPlane = $segment->ChangeOfPlane;
                $intbookRouteSegment->ParticipantLevel = $segment->ParticipantLevel;
                $intbookRouteSegment->LinkAvailability = $segment->LinkAvailability;
                $intbookRouteSegment->PolledAvailabilityOption = $segment->PolledAvailabilityOption;
                $intbookRouteSegment->OptionalServicesIndicator = $segment->OptionalServicesIndicator;
                $intbookRouteSegment->AvailabilitySource = $segment->AvailabilitySource;
                $intbookRouteSegment->AvailabilityDisplayType = $segment->AvailabilityDisplayType;
                $intbookRouteSegment->NumberOfStops = $segment->NumberOfStops;
                $intbookRouteSegment->DestinationTerminal = $segment->DestinationTerminal;
                $intbookRouteSegment->TravelTime = $segment->TravelTime;
                $intbookRouteSegment->BookingCode = $segment->BookingCode;
                $intbookRouteSegment->BookingCount = $segment->BookingCount;
                $intbookRouteSegment->CabinClass = $segment->CabinClass;
                $intbookRouteSegment->NotValidBefore = $segment->NotValidBefore;
                $intbookRouteSegment->NotValidAfter = $segment->NotValidAfter;
                $intbookRouteSegment->FareRuleKey = $segment->FareRuleKey;
                $intbookRouteSegment->MaxWeight = $segment->MaxWeight;
                $intbookRouteSegment->WeightUnits = $segment->WeightUnits;
                $intbookRouteSegment->json_content = $segment->json_content;
                $intbookRouteSegment->intbookairroute_id = $intbookRoute->id;
                $intbookRouteSegment->save();

                $segment->delete();
            }
            $route->delete();


        }




//        =========================New Cart Save End======================================


        $cart->billing_firstname = '';
        $cart->billing_lastname = '';
        $cart->billing_email = '';
        $cart->billing_address = '';
        $cart->billing_city = '';
        $cart->billing_postalcode = '';
        $cart->billing_country = '';
        $cart->billing_state = '';
        $cart->billing_phone = '';
        $cart->card_number = '';
        $cart->cardholder = '';
        $cart->exp_date = '';
        $cart->cvv = '';

        $cart->save();

        $cart->ticket->delete();


        $redata = array(

            'flag' => 'true',
            'invoice' => $intbook->order_number
        );

        $flag = "true";
//        $flag = true;
        return $redata;

    }

    function cardByCheck($card_number, $cardholder, $exp_date, $cvv){

        $cart =  Auth::user()->cart;

        $cardinfo = Cardinfo::where('card_number',$card_number )->first();

        if($cardinfo){

        }
        else{
            $cardinfo = new Cardinfo();

            $cardinfo->billing_firstname = $cart->billing_firstname;
            $cardinfo->billing_lastname = $cart->billing_lastname;
            $cardinfo->billing_email = $cart->billing_email;
            $cardinfo->billing_address = $cart->billing_address;
            $cardinfo->billing_city = $cart->billing_city;
            $cardinfo->billing_postalcode = $cart->billing_postalcode;
            $cardinfo->billing_country = $cart->billing_country;
            $cardinfo->billing_state = $cart->billing_state;
            $cardinfo->billing_phone = $cart->billing_phone;

            $cardinfo->card_number = $card_number;
            $cardinfo->cardholder = $cardholder;
            $cardinfo->exp_date = $exp_date;
            $cardinfo->cvv = $cvv;

            $cardinfo->user_id = Auth::user()->id;

            $cardinfo->save();
        }



        return $cardinfo->id;



    }



    public function TicketBookingAction($invoice){
        $sale = Intbook::where('order_number', $invoice)->with('ticket')->first();
//        ==============

        $customer =  Auth::user()->customer;
        $cuser =  Auth::user();
        $invnum = $sale->order_number;
//        $pdflnk = public_path().'/salepdf/'.$invoice.'.pdf';
//
//        $pdf = PDF::loadView('booking.checkout-pdf',['sale'=>$sale]);
//        $pdf->save(public_path().'/salepdf/'.$sale->order_number.'.pdf');
//
//        Mail::send('booking.bookingmail', ['title' => 'Your Invoice mail title', 'content' => 'Please check your invoice attachment-booking'], function ($message) use ($invoice, $cuser)
//        {
//            $message->sender('vincej1657@gmail.com');
//            $message->subject('Jatry Invoice');
//
//            $message->attach(public_path().'/salepdf/'.$invoice.'.pdf');
//
//            $message->to($cuser->email);
//            $message->cc('zeshaq@gmail.com', $name = null);
//
//        });
////        ==============
//
//        unlink(public_path().'/salepdf/'.$sale->order_number.'.pdf');


        return view('booking.checkout-final', compact('sale'));
    }

    public function TicketBookingPdfAction($invoice){

        $sale = Intbook::where('order_number', $invoice)->with('ticket')->first();
        $pdf = PDF::loadView('booking.checkout-pdf',['sale'=>$sale]);
        return $pdf->stream();
    }

    public function TicketBookingCancleAction($invoice){
        $sale = Intbook::where('order_number', $invoice)->first();
        $sale->is_cancel = true;
        $sale->save();

//        return view('booking.checkout-final', compact('sale'));

        return Redirect::to(URL::previous() . "#cancleBooking");
    }


}
