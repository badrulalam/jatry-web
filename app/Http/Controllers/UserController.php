<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
//use Illuminate\Routing\UrlGenerator;

use App\Http\Requests;
use Session;
use Auth;
use Hash;
use Mail;
//use Request;
use Validator;

class UserController extends Controller
{


    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('auth', ['except' => ['resetPasswordAction', 'resetPassworSendAction', 'resetPassworCodeAction', 'resetPassworCodeDoneAction']]);

    }



    protected function newAction()
    {
        $roles = Role::lists('display_name','id');
        return view('user.new',compact('roles'));
    }

    protected function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }



//    public function index()
//    {
//        $users = \DB::table('users')->get();
//        return view('user.index', ['users' => $users]);
//    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $data = User::orderBy('id','DESC')->paginate(5);
        $data = User::orderBy('id','ASC')->paginate(3);
        return view('user.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);
    }




//    public function edit($id)
//    {
//        $user = User::findOrFail($id);
//        return view('user.edit', ['user' => $user]);
//    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::lists('display_name','id');
        $userRole = $user->roles->lists('id','id')->toArray();

        return view('user.edit',compact('user','roles','userRole'));
    }




//    public function update($id, Request $request)
//    {
//        $user = User::findOrFail($id);
//        $this->validate($request, [
//            'name' => 'required',
//            'email' => 'required'
//        ]);
//        $input = $request->all();
//        $user->fill($input)->save();
//       \Session::flash('flash_message', 'Task successfully added!');
//        return redirect()->back();
//    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
//            $input['password'] = Hash::make($input['password']);
            $input['password'] = bcrypt($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }

        $user = User::findOrFail($id);
        $user->update($input);
        \DB::table('role_user')->where('user_id',$id)->delete();


        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User updated successfully');
    }






    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
//        \Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->route('allusers')
            ->with('success','User deleted successfully');
    }



    public function changePasswordAction(Request $request)
    {
        return view('user.change-password');
    }


    protected function changePasswordSaveAction(Request $request)
    {
        $this->validate($request, [
            'oldpass' => 'required',
            'password' => 'required|same:confirm-password'
        ]);


        $user = Auth::user();

        if (Hash::check($request->input('oldpass'), $user->password)) {
            $user->password = bcrypt($request->input('password'));
            $user->save();
            return redirect()->route('userpasschange')
                ->with('passmessage','Password change success.');
        }
        else{
            return redirect()->route('userpasschange')
                ->with('passmessage','Old password not match');

        }


    }
    protected function changePasswordSaveJWTAction(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'oldPassword' => 'required',
            'newPassword' => 'required|same:confirmPassword',
            'confirmPassword' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage['invalidAttributes'] = $validator->errors();
            return response()->json($errorMessage, 400);
        }

        $user = Auth::user();

        if (Hash::check($request->input('oldPassword'), $user->password)) {
            $user->password = bcrypt($request->input('newPassword'));
            $user->save();
            return response()->json("Password Changed!", 200);
            }
        else{
            return response()->json("Password not matched!", 400);
        }


    }



    public function resetPasswordAction(Request $request)
    {

//        return $request->server->get('SERVER_NAME');
//        return \Request::root();


        return view('user.reset-password');
    }

    protected function resetPassworSendAction(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

//        return date("Ymd").time();

        $semail = $request->input('email');

        $emailuser = User::where('email', $semail)->first();

        if ($emailuser) {
            $emailuser->cngpasscode = date("Ymd").time();
            $emailuser->save();

//            $username = $emailuser-name;
//            $userpasscode = $emailuser->cngpasscode;


//            ============Mail Start====================

            $jhost = \Request::root();

            Mail::send('emails.reset-mail', ['title' => 'Your Password Reset mail title', 'jhost' => $jhost, 'name' => $emailuser->name, 'passcode' => $emailuser->cngpasscode], function ($message) use ($semail)
            {
//            $message->from('no-reply@zeteq.com', 'Christian Nwamba');

                $message->sender('vincej1657@gmail.com');
                $message->subject('Jatry Reset password');
                $message->to($semail);
                $message->cc('zeshaq@gmail.com', $name = null);

            });

//            ============Mail End======================


            return redirect()->route('userpassreset')
                ->with('resetmessage','Email send.please check and click reset link.');
        }
        else{
            return redirect()->route('userpassreset')
                ->with('resetmessage','You are invalid user.');

        }


    }


    protected function resetPassworCodeAction(Request $request, $code)
    {

        $emailuser = User::where('cngpasscode', $code)->first();

        if($emailuser){

            return view('user.reset-change-password',compact('code'));
        }
        else{

            return redirect()->route('userpassreset')
                ->with('resetmessage','Your reset link is invalid .please resend your reset link enter your email address');

        }


    }


    protected function resetPassworCodeDoneAction(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|same:confirm-password'
        ]);


        $pcode = $request->input('pcode');
        $pcodeuser = User::where('cngpasscode', $pcode)->first();


        if ($pcodeuser) {
            $pcodeuser->password = bcrypt($request->input('password'));
            $pcodeuser->cngpasscode = '';
            $pcodeuser->save();
            return redirect()->route('customerlogin');

        }
        else{
            return redirect()->route('userpassreset')
                ->with('resetmessage','Your reset link is invalid .please resend your reset link enter your email address');

        }


    }



}
