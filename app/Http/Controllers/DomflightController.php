<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Role;

use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Domcity;
use App\Domflight;



class DomflightController extends Controller
{


    public function flightTestAction(Request $request)
    {

        $fields_string = '';
        //set POST variables
        $url = 'http://secure.flynovoair.com/bookings/flight_selection.aspx';
        $fields = array(
            //post parameters to be sent to the other website
            'TT'=>'OW',
            'DC'=>'DAC',
            'AC'=>'CGP',
            'PA'=>'1',
            'PC'=>'1',
            'PI'=>'0',
            'FL'=>''
        );

//url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string,'&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);




    }




    public function triggerValidateAction(Request $request)
    {

        $orginId = intval($request->input('origin'));
        $destinationId = intval($request->input('destination'));
        $adults = intval($request->input('adults'));
        $childs = intval($request->input('childs'));
        $infant = intval($request->input('infant'));
        $noOfSegments = intval($request->input('noOfSegments'));
        $tripType = intval($request->input('tripType'));
//        $departureDate = $request->input('departureDate');
        $departureDayname = $request->input('departureDayname');
        $departureDaydate = $request->input('departureDaydate');

//        return $departureDaydate;



        if($noOfSegments === 1)
        {

//            $flight = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
//                      ->where(function($query)
//                {
//                    $query->where('sat',true)->orWhere('daily',true);
//                })->with('domairline')->with('orgincity')->with('destinationcity')->get();

            if($departureDayname=='sat'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('sat',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='sun'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('sun',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='mon'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('mon',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='tue'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('tue',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='wed'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('wed',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='thu'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('thu',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }

            if($departureDayname=='fri'){
                $oneway = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId)
                    ->where(function($query)
                    {
                        $query->where('fri',true)->orWhere('daily',true);
                    })->with('domairline')->with('orgincity')->with('destinationcity')->get();
            }


//            return $oneway->with('domairline')->with('orgincity')->with('destinationcity')->get();
            return $oneway;

        }








    }


    public function triggerValidateAction_old(Request $request)
    {

        $orginId = intval($request->input('origin'));
        $destinationId = intval($request->input('destination'));
        $adults = intval($request->input('adults'));
        $childs = intval($request->input('childs'));
        $infant = intval($request->input('infant'));
        $noOfSegments = intval($request->input('noOfSegments'));
        $tripType = intval($request->input('tripType'));
//        $departureDate = $request->input('departureDate');
        $departureDay = $request->input('departureDate');



        if($noOfSegments === 1)
        {

            $flight = Domflight::where('orgincity_id',$orginId)->where('destinationcity_id',$destinationId);

            if($departureDay=='sat'){
                $oneway = $flight->where('sat',true)->orWhere('daily',true);
            }

            if($departureDay=='sun'){
                $oneway = $flight->where('sun',true)->orWhere('daily',true);
            }

            if($departureDay=='mon'){
                $oneway = $flight->where('mon',true)->orWhere('daily',true);
            }

            if($departureDay=='tue'){
                $oneway = $flight->where('tue',true)->orWhere('daily',true);
            }

            if($departureDay=='wed'){
                $oneway = $flight->where('wed',true)->orWhere('daily',true);
            }

            if($departureDay=='thu'){
                $oneway = $flight->where('thu',true)->orWhere('daily',true);
            }

            if($departureDay=='sat'){
                $oneway = $flight->where('sat',true)->orWhere('daily',true);
            }


            return $oneway->with('domairline')->with('orgincity')->with('destinationcity')->get();

        }








    }
}
