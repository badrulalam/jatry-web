<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;



use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Session;














class AgentController extends Controller
{


    public function __construct()
    {

    }








    public function agentregisterAction()
    {
        return view('agent.agentregister');
    }

    protected function agentregisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'agency_name' => 'required|unique:agency,name',
            'agency_email' => 'required|email|unique:agency,email',
            'agency_url' => 'required|unique:agency,url',

            'first_name' => 'required',
            'agent_email' => 'required|email|unique:agent,email',
            'agent_url' => 'required|unique:agent,url',

        ]);

//        $input = $request->all();
//        $input['password'] = bcrypt($input['password']);



        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();


//        $role = DB::table('users')->where('name', 'John')->first();
        $role = Role::where('name', 'superagent')->first();
        $user->attachRole($role->id);

        $agency = new Agency();
        $agency->name = $request->input('agency_name');
        $agency->email = $request->input('agency_email');
        $agency->url = $request->input('agency_url');
        $agency->isactive = true;
        $agency->user_id = $user->id;
        $agency->save();

//        $user->agency()->save($agency);
        $agency->code = 'ACY-'.$agency->id;;
        $agency->save();


        $agent = new Agent();
        $agent->first_name = $request->input('first_name');
        $agent->last_name = $request->input('last_name');
        $agent->email = $request->input('agent_email');
        $agent->url = $request->input('agent_url');
        $agent->isactive = true;
        $agent->user_id = $user->id;

        $agent->agency_id = $agency->id;
        $agent->save();

        $agent->code = 'AGT-'.$agent->id;
        $agent->save();


//        $user->agent()->save($agent);

//        $agency->agent()->save($agent);
//
//
//
//        $agent->code = 'AGT-'.$agent->id;;
//        $agent->save();




        return redirect()->route('home')
            ->with('success','User, Agency, Agent created successfully');
    }

    public function frontAgentLoginAction(Request $request)
    {
        return view('agent.agentlogin');
    }





    public function agentMyAccountAction(Request $request)
    {
        $user = \Auth::user();
        return view('agent.myaccount', ['user' => $user]);
//        return view('agent.myaccount', compact(user));
    }




 public function agentMyAccountSaveAction(Request $request)
    {

        $user = \Auth::user();
        $this->validate($request, [

            'user_name' => 'required',
            'user_email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'same:confirm-password',

//            'agency_name' => 'required',
//            'agency_email' => 'required|email|unique:agency,email,'.$user->agency->id,
//            'agency_address' => 'required',
//            'agency_city' => 'required',
//            'agency_state' => 'required',
//            'agency_postalcode' => 'required',
//            'agency_country' => 'required',
//            'agency_phone' => 'required',
//            'agency_url' => 'required|unique:agency,url,'.$user->agency->id,


            'first_name' => 'required',
            'agent_email' => 'required|email|unique:agent,email,'.$user->agent->id,
            'agent_address' => 'required',
            'agent_city' => 'required',
            'agent_state' => 'required',
            'agent_postalcode' => 'required',
            'agent_country' => 'required',
            'agent_phone' => 'required',

        ]);






        $user->name = $request->input('user_name');
        $user->email = $request->input('user_email');

        if(!empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();


        if ($user->agency) {
            $this->validate($request, [
                'agency_name' => 'required|unique:agency,name,' . $user->agency->id,
                'agency_email' => 'required|email|unique:agency,email,' . $user->agency->id,
                'agency_address' => 'required',
                'agency_city' => 'required',
                'agency_state' => 'required',
                'agency_postalcode' => 'required',
                'agency_country' => 'required',
                'agency_phone' => 'required',
                'agency_url' => 'required|unique:agency,url,' . $user->agency->id,

            ]);


            $user->agency->name = $request->input('agency_name');
            $user->agency->email = $request->input('agency_email');
            $user->agency->address = $request->input('agency_address');
            $user->agency->city = $request->input('agency_city');
            $user->agency->state = $request->input('agency_state');
            $user->agency->postalcode = $request->input('agency_postalcode');
            $user->agency->country = $request->input('agency_country');
            $user->agency->phone = $request->input('agency_phone');
            $user->agency->url = $request->input('agency_url');

            if ($request->file('agency_image')) {
                \File::delete(storage_path().'/uploads/agency-image/'.$user->agency->image);
                $agencyimage = $request->file('agency_image');
                $agencyimagename = rand(11111,99999).'-'.$agencyimage->getClientOriginalName();
                $agencyimage->move(storage_path().'/uploads/agency-image/', $agencyimagename);
                $user->agency->image = $agencyimagename;
            }

            if ($request->file('agency_logo')) {
                \File::delete(storage_path().'/uploads/agency-logo/'.$user->agency->logo);
                $agencylogo = $request->file('agency_logo');
                $agencylogoname = rand(11111,99999).'-'.$agencylogo->getClientOriginalName();
                $agencylogo->move(storage_path().'/uploads/agency-logo/', $agencylogoname);
                $user->agency->logo = $agencylogoname;
            }
            $user->agency->save();

        }

        $user->agent->first_name = $request->input('first_name');
        $user->agent->last_name = $request->input('last_name');
        $user->agent->email = $request->input('agent_email');
        $user->agent->address = $request->input('agent_address');
        $user->agent->city = $request->input('agent_city');
        $user->agent->state = $request->input('agent_state');
        $user->agent->postalcode = $request->input('agent_postalcode');
        $user->agent->country = $request->input('agent_country');
        $user->agent->phone = $request->input('agent_phone');
        $user->agent->url = $request->input('agent_url');

        if ($request->file('agent_image')) {
            \File::delete(storage_path().'/uploads/agent-image/'.$user->agent->image);
            $agentimage = $request->file('agent_image');
            $agentimagename = rand(11111,99999).'-'.$agentimage->getClientOriginalName();
            $agentimage->move(storage_path().'/uploads/agent-image/', $agentimagename);
            $user->agent->image = $agentimagename;
        }
        $user->agent->save();


        return view('agent.myaccount', ['user' => $user]);
    }



    public function allAgentListAction(Request $request)
    {
        $data = Agent::orderBy('agency_id','ASC')->paginate(10);
        return view('adminarea.agent.allagentlist',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }











}




