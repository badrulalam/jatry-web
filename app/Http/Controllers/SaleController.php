<?php

namespace App\Http\Controllers;


use App\Saleairroute;
use App\Saleairroutesegment;
use Illuminate\Http\Request;

use App\Http\Requests;




use Illuminate\Http\Response;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Redirect;
use URL;

use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Sale;
use App\Cardinfo;
use App\Cartitemair;
use App\Saleitemair;
use App\Saleitemairpassenger;


use App\Intbook;
use App\Intbookitemair;
use App\Intbookairroute;
use App\Intbookairroutesegment;
use App\Intbookitemairpassenger;




class SaleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function CheckoutCompleteAction(Request $request)
    {

//        $card_number = $request->input('card_number');
//        $cardholder = $request->input('cardholder');
//        $exp_date = $request->input('exp_date');
//        $cvv = $request->input('cvv');

        $cart =  Auth::user()->cart;
        $customer =  Auth::user()->customer;


//        $cardinfo = $this->cardByCheck($card_number, $cardholder, $exp_date, $cvv);
        $cardinfo = $this->cardByCheck($cart->card_number, $cart->cardholder, $cart->exp_date, $cart->cvv);
        $sale = new Sale();

//        $sale->order_number = Auth::user()->id.'-'.Auth::user()->cart->id;
        $sale->order_number = 'INV-'.date("Ymd").'-'.time().'-'.Auth::user()->id;
//        $sale->billing_firstname = $cart->billing_firstname;
//        $sale->billing_lastname = $cart->billing_lastname;
//        $sale->billing_email = $cart->billing_email;
//        $sale->billing_address = $cart->billing_address;
//        $sale->billing_city = $cart->billing_city;
//        $sale->billing_postalcode = $cart->billing_postalcode;
//        $sale->billing_country = $cart->billing_country;
//        $sale->billing_state = $cart->billing_state;
//        $sale->billing_phone = $cart->billing_phone;


        $sale->billing_firstname = $customer->first_name;
        $sale->billing_lastname = $customer->last_name;
        $sale->billing_email = $customer->email;
        $sale->billing_address = $customer->address;
        $sale->billing_city = $customer->city;
        $sale->billing_postalcode = $customer->postalcode;
        $sale->billing_country = $customer->country;
        $sale->billing_state = $customer->state;
        $sale->billing_phone = $customer->phone;

//        $sale->shipping_firstname = $cart->shipping_firstname;
//        $sale->shipping_lastname = $cart->shipping_lastname;
//        $sale->shipping_email = $cart->shipping_email;
//        $sale->shipping_address = $cart->shipping_address;
//        $sale->shipping_city = $cart->shipping_city;
//        $sale->shipping_postalcode = $cart->shipping_postalcode;
//        $sale->shipping_country = $cart->shipping_country;
//        $sale->shipping_state = $cart->shipping_state;
//        $sale->shipping_phone = $cart->shipping_phone;

        $sale->cardinfo_id = $cardinfo;

        $sale->user_id = Auth::user()->id;
        $sale->save();


//        =========================New Cart Save Start====================================
        $saleItemAir = new Saleitemair();

        $saleItemAir->Price = $cart->ticket->Price;
        $saleItemAir->Key = $cart->ticket->Key;
        $saleItemAir->TotalPrice = $cart->ticket->TotalPrice;
        $saleItemAir->BasePrice = $cart->ticket->BasePrice;
        $saleItemAir->ApproximateTotalPrice = $cart->ticket->ApproximateTotalPrice;
        $saleItemAir->ApproximateBasePrice = $cart->ticket->ApproximateBasePrice;
        $saleItemAir->EquivalentBasePrice = $cart->ticket->EquivalentBasePrice;
        $saleItemAir->Taxes = $cart->ticket->Taxes;
        $saleItemAir->ApproximateTaxes = $cart->ticket->ApproximateTaxes;
        $saleItemAir->PolicyExclusion = $cart->ticket->PolicyExclusion;
        $saleItemAir->Refundable = $cart->ticket->Refundable;
        $saleItemAir->PlatingCarrier = $cart->ticket->PlatingCarrier;
        $saleItemAir->TravelTime = $cart->ticket->TravelTime;
        $saleItemAir->adults = $cart->ticket->adults;
        $saleItemAir->childs = $cart->ticket->childs;
        $saleItemAir->infant = $cart->ticket->infant;
        $saleItemAir->tripType = $cart->ticket->tripType;
        $saleItemAir->noOfSegments = $cart->ticket->noOfSegments;
        $saleItemAir->class = $cart->ticket->class;
        $saleItemAir->json_content = $cart->ticket->json_content;
//        $saleItemAir->saleprice = $cart->cartitemair->saleprice;
        $saleItemAir->saleprice = 1002.00;
        $saleItemAir->sale_id = $sale->id;
        $saleItemAir->save();



        foreach($cart->ticket->passenger as $passenger){

            $salePassenger = new Saleitemairpassenger();

            $salePassenger->ticket_key = '';
            $salePassenger->pnr_number = time();
            $salePassenger->title = $passenger->title;
            $salePassenger->first_name = $passenger->first_name;
            $salePassenger->middle_name = $passenger->middle_name;
            $salePassenger->last_name = $passenger->last_name;
            $salePassenger->dob = $passenger->dob;
            $salePassenger->nationality = $passenger->nationality;
            $salePassenger->passport_no = $passenger->passport_no;
            $salePassenger->passport_issue_country = $passenger->passport_issue_country;
            $salePassenger->passport_expiry_date = $passenger->passport_expiry_date;
            $salePassenger->passenger_type = $passenger->passenger_type;
            $salePassenger->passenger_order = $passenger->passenger_order;
            $salePassenger->saleitemair_id = $saleItemAir->id;

            $salePassenger->save();

            $passenger->delete();
        }







        foreach($cart->ticket->route as $route){

            $saleRoute = new Saleairroute();

            $saleRoute->Origin = $route->Origin;
            $saleRoute->Destination = $route->Destination;
            $saleRoute->DepartureTime = $route->DepartureTime;
            $saleRoute->ArrivalTime = $route->ArrivalTime;
            $saleRoute->Duration = $route->Duration;
            $saleRoute->ViaStops = $route->ViaStops;
            $saleRoute->Stops = $route->Stops;
            $saleRoute->json_content = $route->json_content;
            $saleRoute->saleitemair_id = $saleItemAir->id;
            $saleRoute->save();

            foreach($route->segment as $segment){

                $saleRouteSegment = new Saleairroutesegment();

                $saleRouteSegment->Key = $segment->Key;
                $saleRouteSegment->Group = $segment->Group;
                $saleRouteSegment->Carrier = $segment->Carrier;
                $saleRouteSegment->FlightNumber = $segment->FlightNumber;
                $saleRouteSegment->Origin = $segment->Origin;
                $saleRouteSegment->Destination = $segment->Destination;
                $saleRouteSegment->DepartureTime = $segment->DepartureTime;
                $saleRouteSegment->ArrivalTime = $segment->ArrivalTime;
                $saleRouteSegment->FlightTime = $segment->FlightTime;
                $saleRouteSegment->Distance = $segment->Distance;
                $saleRouteSegment->DistanceUnits = $segment->DistanceUnits;
                $saleRouteSegment->ETicketability = $segment->ETicketability;
                $saleRouteSegment->Equipment = $segment->Equipment;
                $saleRouteSegment->ChangeOfPlane = $segment->ChangeOfPlane;
                $saleRouteSegment->ParticipantLevel = $segment->ParticipantLevel;
                $saleRouteSegment->LinkAvailability = $segment->LinkAvailability;
                $saleRouteSegment->PolledAvailabilityOption = $segment->PolledAvailabilityOption;
                $saleRouteSegment->OptionalServicesIndicator = $segment->OptionalServicesIndicator;
                $saleRouteSegment->AvailabilitySource = $segment->AvailabilitySource;
                $saleRouteSegment->AvailabilityDisplayType = $segment->AvailabilityDisplayType;
                $saleRouteSegment->NumberOfStops = $segment->NumberOfStops;
                $saleRouteSegment->DestinationTerminal = $segment->DestinationTerminal;
                $saleRouteSegment->TravelTime = $segment->TravelTime;
                $saleRouteSegment->BookingCode = $segment->BookingCode;
                $saleRouteSegment->BookingCount = $segment->BookingCount;
                $saleRouteSegment->CabinClass = $segment->CabinClass;
                $saleRouteSegment->NotValidBefore = $segment->NotValidBefore;
                $saleRouteSegment->NotValidAfter = $segment->NotValidAfter;
                $saleRouteSegment->FareRuleKey = $segment->FareRuleKey;
                $saleRouteSegment->MaxWeight = $segment->MaxWeight;
                $saleRouteSegment->WeightUnits = $segment->WeightUnits;
                $saleRouteSegment->json_content = $segment->json_content;
                $saleRouteSegment->saleairroute_id = $saleRoute->id;
                $saleRouteSegment->save();

                $segment->delete();
            }
            $route->delete();


        }




//        =========================New Cart Save End======================================


        $cart->billing_firstname = '';
        $cart->billing_lastname = '';
        $cart->billing_email = '';
        $cart->billing_address = '';
        $cart->billing_city = '';
        $cart->billing_postalcode = '';
        $cart->billing_country = '';
        $cart->billing_state = '';
        $cart->billing_phone = '';
        $cart->card_number = '';
        $cart->cardholder = '';
        $cart->exp_date = '';
        $cart->cvv = '';

        $cart->save();

        $cart->ticket->delete();


         $redata = array(

             'flag' => 'true',
             'invoice' => $sale->order_number
         );

        $flag = "true";
//        $flag = true;
        return $redata;

    }

    function cardByCheck($card_number, $cardholder, $exp_date, $cvv){

        $cart =  Auth::user()->cart;

        $cardinfo = Cardinfo::where('card_number',$card_number )->first();

        if($cardinfo){

        }
        else{
            $cardinfo = new Cardinfo();

            $cardinfo->billing_firstname = $cart->billing_firstname;
            $cardinfo->billing_lastname = $cart->billing_lastname;
            $cardinfo->billing_email = $cart->billing_email;
            $cardinfo->billing_address = $cart->billing_address;
            $cardinfo->billing_city = $cart->billing_city;
            $cardinfo->billing_postalcode = $cart->billing_postalcode;
            $cardinfo->billing_country = $cart->billing_country;
            $cardinfo->billing_state = $cart->billing_state;
            $cardinfo->billing_phone = $cart->billing_phone;

            $cardinfo->card_number = $card_number;
            $cardinfo->cardholder = $cardholder;
            $cardinfo->exp_date = $exp_date;
            $cardinfo->cvv = $cvv;

            $cardinfo->user_id = Auth::user()->id;

            $cardinfo->save();
        }



        return $cardinfo->id;



    }






    public function TicketSaleVIewAction($invoice){
        $sale = Sale::where('order_number', $invoice)->with('saleitemair')->first();
//        ==============

        $customer =  Auth::user()->customer;
        $cuser =  Auth::user();
        $invnum = $sale->order_number;


        return view('sale.ticket-sale-view', compact('sale'));
    }

    public function TicketSaleAction($invoice){
        $sale = Sale::where('order_number', $invoice)->with('saleitemair')->first();
//        ==============

        $customer =  Auth::user()->customer;
        $cuser =  Auth::user();
        $invnum = $sale->order_number;
        $pdflnk = public_path().'/salepdf/'.$invoice.'.pdf';

        $pdf = PDF::loadView('sale.checkout-pdf',['sale'=>$sale]);
        $pdf->save(public_path().'/salepdf/'.$sale->order_number.'.pdf');

        Mail::send('sale.salemail', ['title' => 'Your Invoice mail title', 'content' => 'Please check your invoice attachment'], function ($message) use ($invoice, $cuser)
        {
//            $message->from('no-reply@zeteq.com', 'Christian Nwamba');

            $message->sender('vincej1657@gmail.com');
            $message->subject('Jatry Invoice');

            $message->attach(public_path().'/salepdf/'.$invoice.'.pdf');

//            $message->to('zqarif@gmail.com');
            $message->to($cuser->email);
//            $message->cc('limon3640@gmail.com', $name = null);
            $message->cc('zeshaq@gmail.com', $name = null);
//            $message->getSwiftMessage();

        });
//        ==============

//        unlink(public_path('file/to/delete'));
        unlink(public_path().'/salepdf/'.$sale->order_number.'.pdf');


        return view('sale.checkout-final', compact('sale'));
    }


    public function TicketSalePdfAction($invoice){
//        return "success";

        $sale = Sale::where('order_number', $invoice)->with('saleitemair')->first();
        $pdf = PDF::loadView('sale.checkout-pdf',['sale'=>$sale]);
//        return $pdf->download('invoice.pdf');
        return $pdf->stream();


    }


    public function SaleAllInvoiceAction(Request $request)
    {
        $userid =  Auth::user()->id;
        $sales = Sale::where('user_id', $userid)->get();
        $dataArr = array(
            'allsale' => $sales
        );
        return $dataArr;
    }


    public function ClickInvoiceAction(Request $request)
    {

        $saleid = $request->input('saleid');

        $sale = Sale::where('id', $saleid)->with('ticket')->with('ticket.route')->with('ticket.route.segment')->with('ticket.passenger')->first();
        $cardinfo = $sale->cardinfo;

//        $saleitemair = Saleitemair::where('sale_id', $saleid)-with('passenger')->get();
//        $saleitemair = Saleitemair::where('sale_id', $saleid)->with('route')->with('route.segment')->first();

//        foreach($saleitemair as $sair){
//            $sair['passenger'] = Saleitemairpassenger::where('saleitemair_id', $sair->id)->get();
//        }

        $ticketArr = array(
//            'allticket' => $saleitemair,
            'sale' => $sale,
            'cardinfo' => $cardinfo
        );
        return $ticketArr;
    }




    public function BookingTicketPurchaseAction(Request $request, $invoice)
    {

        $intbook = Intbook::where('user_id', Auth::user()->id)->where('order_number', $invoice)->with('ticket')->first();


//        $cardinfo = $this->bookingCardByCheck($cart->card_number, $cart->cardholder, $cart->exp_date, $cart->cvv);
        $sale = new Sale();

        $sale->order_number = 'INV-'.date("Ymd").'-'.time().'-'.Auth::user()->id;

        $sale->billing_firstname = $intbook->billing_firstname;
        $sale->billing_lastname = $intbook->billing_lastname;
        $sale->billing_email = $intbook->billing_email;
        $sale->billing_address = $intbook->billing_address;
        $sale->billing_city = $intbook->billing_city;
        $sale->billing_postalcode = $intbook->billing_postalcode;
        $sale->billing_country = $intbook->billing_country;
        $sale->billing_state = $intbook->billing_state;
        $sale->billing_phone = $intbook->billing_phone;

//        $sale->shipping_firstname = $intbook->shipping_firstname;
//        $sale->shipping_lastname = $intbook->shipping_lastname;
//        $sale->shipping_email = $intbook->shipping_email;
//        $sale->shipping_address = $intbook->shipping_address;
//        $sale->shipping_city = $intbook->shipping_city;
//        $sale->shipping_postalcode = $intbook->shipping_postalcode;
//        $sale->shipping_country = $intbook->shipping_country;
//        $sale->shipping_state = $intbook->shipping_state;
//        $sale->shipping_phone = $intbook->shipping_phone;

        $sale->cardinfo_id = $intbook->cardinfo_id;

        $sale->user_id = $intbook->user_id;
        $sale->save();


//        =========================New Cart Save Start====================================
        $saleItemAir = new Saleitemair();

        $saleItemAir->Price = $intbook->ticket->Price;
        $saleItemAir->Key = $intbook->ticket->Key;
        $saleItemAir->TotalPrice = $intbook->ticket->TotalPrice;
        $saleItemAir->BasePrice = $intbook->ticket->BasePrice;
        $saleItemAir->ApproximateTotalPrice = $intbook->ticket->ApproximateTotalPrice;
        $saleItemAir->ApproximateBasePrice = $intbook->ticket->ApproximateBasePrice;
        $saleItemAir->EquivalentBasePrice = $intbook->ticket->EquivalentBasePrice;
        $saleItemAir->Taxes = $intbook->ticket->Taxes;
        $saleItemAir->ApproximateTaxes = $intbook->ticket->ApproximateTaxes;
        $saleItemAir->PolicyExclusion = $intbook->ticket->PolicyExclusion;
        $saleItemAir->Refundable = $intbook->ticket->Refundable;
        $saleItemAir->PlatingCarrier = $intbook->ticket->PlatingCarrier;
        $saleItemAir->TravelTime = $intbook->ticket->TravelTime;
        $saleItemAir->adults = $intbook->ticket->adults;
        $saleItemAir->childs = $intbook->ticket->childs;
        $saleItemAir->infant = $intbook->ticket->infant;
        $saleItemAir->tripType = $intbook->ticket->tripType;
        $saleItemAir->noOfSegments = $intbook->ticket->noOfSegments;
        $saleItemAir->class = $intbook->ticket->class;
        $saleItemAir->json_content = $intbook->ticket->json_content;
//        $saleItemAir->saleprice = $intbook->cartitemair->saleprice;
        $saleItemAir->saleprice = 1002.00;
        $saleItemAir->sale_id = $sale->id;
        $saleItemAir->save();



        foreach($intbook->ticket->passenger as $passenger){

            $salePassenger = new Saleitemairpassenger();

            $salePassenger->ticket_key = '';
            $salePassenger->pnr_number = time();
            $salePassenger->title = $passenger->title;
            $salePassenger->first_name = $passenger->first_name;
            $salePassenger->middle_name = $passenger->middle_name;
            $salePassenger->last_name = $passenger->last_name;
            $salePassenger->dob = $passenger->dob;
            $salePassenger->nationality = $passenger->nationality;
            $salePassenger->passport_no = $passenger->passport_no;
            $salePassenger->passport_issue_country = $passenger->passport_issue_country;
            $salePassenger->passport_expiry_date = $passenger->passport_expiry_date;
            $salePassenger->passenger_type = $passenger->passenger_type;
            $salePassenger->passenger_order = $passenger->passenger_order;
            $salePassenger->saleitemair_id = $saleItemAir->id;

            $salePassenger->save();

        }







        foreach($intbook->ticket->route as $route){

            $saleRoute = new Saleairroute();

            $saleRoute->Origin = $route->Origin;
            $saleRoute->Destination = $route->Destination;
            $saleRoute->DepartureTime = $route->DepartureTime;
            $saleRoute->ArrivalTime = $route->ArrivalTime;
            $saleRoute->Duration = $route->Duration;
            $saleRoute->ViaStops = $route->ViaStops;
            $saleRoute->Stops = $route->Stops;
            $saleRoute->json_content = $route->json_content;
            $saleRoute->saleitemair_id = $saleItemAir->id;
            $saleRoute->save();

            foreach($route->segment as $segment){

                $saleRouteSegment = new Saleairroutesegment();

                $saleRouteSegment->Key = $segment->Key;
                $saleRouteSegment->Group = $segment->Group;
                $saleRouteSegment->Carrier = $segment->Carrier;
                $saleRouteSegment->FlightNumber = $segment->FlightNumber;
                $saleRouteSegment->Origin = $segment->Origin;
                $saleRouteSegment->Destination = $segment->Destination;
                $saleRouteSegment->DepartureTime = $segment->DepartureTime;
                $saleRouteSegment->ArrivalTime = $segment->ArrivalTime;
                $saleRouteSegment->FlightTime = $segment->FlightTime;
                $saleRouteSegment->Distance = $segment->Distance;
                $saleRouteSegment->DistanceUnits = $segment->DistanceUnits;
                $saleRouteSegment->ETicketability = $segment->ETicketability;
                $saleRouteSegment->Equipment = $segment->Equipment;
                $saleRouteSegment->ChangeOfPlane = $segment->ChangeOfPlane;
                $saleRouteSegment->ParticipantLevel = $segment->ParticipantLevel;
                $saleRouteSegment->LinkAvailability = $segment->LinkAvailability;
                $saleRouteSegment->PolledAvailabilityOption = $segment->PolledAvailabilityOption;
                $saleRouteSegment->OptionalServicesIndicator = $segment->OptionalServicesIndicator;
                $saleRouteSegment->AvailabilitySource = $segment->AvailabilitySource;
                $saleRouteSegment->AvailabilityDisplayType = $segment->AvailabilityDisplayType;
                $saleRouteSegment->NumberOfStops = $segment->NumberOfStops;
                $saleRouteSegment->DestinationTerminal = $segment->DestinationTerminal;
                $saleRouteSegment->TravelTime = $segment->TravelTime;
                $saleRouteSegment->BookingCode = $segment->BookingCode;
                $saleRouteSegment->BookingCount = $segment->BookingCount;
                $saleRouteSegment->CabinClass = $segment->CabinClass;
                $saleRouteSegment->NotValidBefore = $segment->NotValidBefore;
                $saleRouteSegment->NotValidAfter = $segment->NotValidAfter;
                $saleRouteSegment->FareRuleKey = $segment->FareRuleKey;
                $saleRouteSegment->MaxWeight = $segment->MaxWeight;
                $saleRouteSegment->WeightUnits = $segment->WeightUnits;
                $saleRouteSegment->json_content = $segment->json_content;
                $saleRouteSegment->saleairroute_id = $saleRoute->id;
                $saleRouteSegment->save();

            }


        }

        $intbook->is_purchase = true;
        $intbook->save();




//        =========================New Cart Save End======================================


        return Redirect::to(URL::previous() . "#cancleBooking");



    }


    public function BookingTicketPurchaseActionJWT(Request $request, $invoice)
    {

        $intbook = Intbook::where('user_id', Auth::user()->id)->where('order_number', $invoice)->with('ticket')->first();
        if(!$intbook){
            return response()->json('No record found with the specified `invoice number`.', 404);
        }

//        $cardinfo = $this->bookingCardByCheck($cart->card_number, $cart->cardholder, $cart->exp_date, $cart->cvv);
        $sale = new Sale();

        $sale->order_number = 'INV-'.date("Ymd").'-'.time().'-'.Auth::user()->id;

        $sale->billing_firstname = $intbook->billing_firstname;
        $sale->billing_lastname = $intbook->billing_lastname;
        $sale->billing_email = $intbook->billing_email;
        $sale->billing_address = $intbook->billing_address;
        $sale->billing_city = $intbook->billing_city;
        $sale->billing_postalcode = $intbook->billing_postalcode;
        $sale->billing_country = $intbook->billing_country;
        $sale->billing_state = $intbook->billing_state;
        $sale->billing_phone = $intbook->billing_phone;

//        $sale->shipping_firstname = $intbook->shipping_firstname;
//        $sale->shipping_lastname = $intbook->shipping_lastname;
//        $sale->shipping_email = $intbook->shipping_email;
//        $sale->shipping_address = $intbook->shipping_address;
//        $sale->shipping_city = $intbook->shipping_city;
//        $sale->shipping_postalcode = $intbook->shipping_postalcode;
//        $sale->shipping_country = $intbook->shipping_country;
//        $sale->shipping_state = $intbook->shipping_state;
//        $sale->shipping_phone = $intbook->shipping_phone;

        $sale->cardinfo_id = $intbook->cardinfo_id;

        $sale->user_id = $intbook->user_id;
        $sale->save();


//        =========================New Cart Save Start====================================
        $saleItemAir = new Saleitemair();

        $saleItemAir->Price = $intbook->ticket->Price;
        $saleItemAir->Key = $intbook->ticket->Key;
        $saleItemAir->TotalPrice = $intbook->ticket->TotalPrice;
        $saleItemAir->BasePrice = $intbook->ticket->BasePrice;
        $saleItemAir->ApproximateTotalPrice = $intbook->ticket->ApproximateTotalPrice;
        $saleItemAir->ApproximateBasePrice = $intbook->ticket->ApproximateBasePrice;
        $saleItemAir->EquivalentBasePrice = $intbook->ticket->EquivalentBasePrice;
        $saleItemAir->Taxes = $intbook->ticket->Taxes;
        $saleItemAir->ApproximateTaxes = $intbook->ticket->ApproximateTaxes;
        $saleItemAir->PolicyExclusion = $intbook->ticket->PolicyExclusion;
        $saleItemAir->Refundable = $intbook->ticket->Refundable;
        $saleItemAir->PlatingCarrier = $intbook->ticket->PlatingCarrier;
        $saleItemAir->TravelTime = $intbook->ticket->TravelTime;
        $saleItemAir->adults = $intbook->ticket->adults;
        $saleItemAir->childs = $intbook->ticket->childs;
        $saleItemAir->infant = $intbook->ticket->infant;
        $saleItemAir->tripType = $intbook->ticket->tripType;
        $saleItemAir->noOfSegments = $intbook->ticket->noOfSegments;
        $saleItemAir->class = $intbook->ticket->class;
        $saleItemAir->json_content = $intbook->ticket->json_content;
//        $saleItemAir->saleprice = $intbook->cartitemair->saleprice;
        $saleItemAir->saleprice = 1002.00;
        $saleItemAir->sale_id = $sale->id;
        $saleItemAir->save();



        foreach($intbook->ticket->passenger as $passenger){

            $salePassenger = new Saleitemairpassenger();

            $salePassenger->ticket_key = '';
            $salePassenger->pnr_number = time();
            $salePassenger->title = $passenger->title;
            $salePassenger->first_name = $passenger->first_name;
            $salePassenger->middle_name = $passenger->middle_name;
            $salePassenger->last_name = $passenger->last_name;
            $salePassenger->dob = $passenger->dob;
            $salePassenger->nationality = $passenger->nationality;
            $salePassenger->passport_no = $passenger->passport_no;
            $salePassenger->passport_issue_country = $passenger->passport_issue_country;
            $salePassenger->passport_expiry_date = $passenger->passport_expiry_date;
            $salePassenger->passenger_type = $passenger->passenger_type;
            $salePassenger->passenger_order = $passenger->passenger_order;
            $salePassenger->saleitemair_id = $saleItemAir->id;

            $salePassenger->save();

        }







        foreach($intbook->ticket->route as $route){

            $saleRoute = new Saleairroute();

            $saleRoute->Origin = $route->Origin;
            $saleRoute->Destination = $route->Destination;
            $saleRoute->DepartureTime = $route->DepartureTime;
            $saleRoute->ArrivalTime = $route->ArrivalTime;
            $saleRoute->Duration = $route->Duration;
            $saleRoute->ViaStops = $route->ViaStops;
            $saleRoute->Stops = $route->Stops;
            $saleRoute->json_content = $route->json_content;
            $saleRoute->saleitemair_id = $saleItemAir->id;
            $saleRoute->save();

            foreach($route->segment as $segment){

                $saleRouteSegment = new Saleairroutesegment();

                $saleRouteSegment->Key = $segment->Key;
                $saleRouteSegment->Group = $segment->Group;
                $saleRouteSegment->Carrier = $segment->Carrier;
                $saleRouteSegment->FlightNumber = $segment->FlightNumber;
                $saleRouteSegment->Origin = $segment->Origin;
                $saleRouteSegment->Destination = $segment->Destination;
                $saleRouteSegment->DepartureTime = $segment->DepartureTime;
                $saleRouteSegment->ArrivalTime = $segment->ArrivalTime;
                $saleRouteSegment->FlightTime = $segment->FlightTime;
                $saleRouteSegment->Distance = $segment->Distance;
                $saleRouteSegment->DistanceUnits = $segment->DistanceUnits;
                $saleRouteSegment->ETicketability = $segment->ETicketability;
                $saleRouteSegment->Equipment = $segment->Equipment;
                $saleRouteSegment->ChangeOfPlane = $segment->ChangeOfPlane;
                $saleRouteSegment->ParticipantLevel = $segment->ParticipantLevel;
                $saleRouteSegment->LinkAvailability = $segment->LinkAvailability;
                $saleRouteSegment->PolledAvailabilityOption = $segment->PolledAvailabilityOption;
                $saleRouteSegment->OptionalServicesIndicator = $segment->OptionalServicesIndicator;
                $saleRouteSegment->AvailabilitySource = $segment->AvailabilitySource;
                $saleRouteSegment->AvailabilityDisplayType = $segment->AvailabilityDisplayType;
                $saleRouteSegment->NumberOfStops = $segment->NumberOfStops;
                $saleRouteSegment->DestinationTerminal = $segment->DestinationTerminal;
                $saleRouteSegment->TravelTime = $segment->TravelTime;
                $saleRouteSegment->BookingCode = $segment->BookingCode;
                $saleRouteSegment->BookingCount = $segment->BookingCount;
                $saleRouteSegment->CabinClass = $segment->CabinClass;
                $saleRouteSegment->NotValidBefore = $segment->NotValidBefore;
                $saleRouteSegment->NotValidAfter = $segment->NotValidAfter;
                $saleRouteSegment->FareRuleKey = $segment->FareRuleKey;
                $saleRouteSegment->MaxWeight = $segment->MaxWeight;
                $saleRouteSegment->WeightUnits = $segment->WeightUnits;
                $saleRouteSegment->json_content = $segment->json_content;
                $saleRouteSegment->saleairroute_id = $saleRoute->id;
                $saleRouteSegment->save();

            }


        }

        $intbook->is_purchase = true;
        $intbook->save();




//        =========================New Cart Save End======================================
        if($intbook){
            return response()->json("Purchased!", 200);
        }else{
            return response()->json("Interal Server Error", 500);
        }




    }
    
    
    
    function bookingCardByCheck($card_number, $cardholder, $exp_date, $cvv){

        $cart =  Auth::user()->cart;

        $cardinfo = Cardinfo::where('card_number',$card_number )->first();

        if($cardinfo){

        }
        else{
            $cardinfo = new Cardinfo();

            $cardinfo->billing_firstname = $cart->billing_firstname;
            $cardinfo->billing_lastname = $cart->billing_lastname;
            $cardinfo->billing_email = $cart->billing_email;
            $cardinfo->billing_address = $cart->billing_address;
            $cardinfo->billing_city = $cart->billing_city;
            $cardinfo->billing_postalcode = $cart->billing_postalcode;
            $cardinfo->billing_country = $cart->billing_country;
            $cardinfo->billing_state = $cart->billing_state;
            $cardinfo->billing_phone = $cart->billing_phone;

            $cardinfo->card_number = $card_number;
            $cardinfo->cardholder = $cardholder;
            $cardinfo->exp_date = $exp_date;
            $cardinfo->cvv = $cvv;

            $cardinfo->user_id = Auth::user()->id;

            $cardinfo->save();
        }



        return $cardinfo->id;



    }


}
