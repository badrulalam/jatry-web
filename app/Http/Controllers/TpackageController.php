<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Pcategory;
use App\Jcontinent;
use App\Jcountry;
use App\Tpackage;
use Auth;
use Session;

class TpackageController extends Controller
{







    public function indexAction(Request $request)
    {
//        $data = User::orderBy('id','DESC')->paginate(5);
        $data = Tpackage::orderBy('id','ASC')->paginate(10);
        return view('adminarea.tpackage.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }



    public function newAction()
    {
        $pcategory = Pcategory::lists('name','id');

        $continent = Jcontinent::lists('name','id');
        $continent->prepend('Select Continent', '');

        $country = Jcountry::lists('name','id');
        $country->prepend('Select Country', '');

        return view('adminarea.tpackage.new',compact('pcategory', 'continent', 'country'));
    }


    public function createAction(Request $request)
    {
        $this->validate($request, [
//            'name' => 'required|unique:pcategory,name',
            'title' => 'required',
            'image1' => 'required',
            'price' => 'required',
            'country' => 'required',
        ]);

        $tpackage = new Tpackage();
        $tpackage->title = $request->input('title');
        $tpackage->description = $request->input('description');

        if ($request->file('image1')) {
//            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image1);
            $upimage1 = $request->file('image1');
            $upimagename1 = rand(11111,99999).'-'.$upimage1->getClientOriginalName();
            $upimage1->move(storage_path().'/uploads/tpackage-image/', $upimagename1);
            $tpackage->image1 = $upimagename1;
        }

        if ($request->file('image2')) {
//            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image2);
            $upimage2 = $request->file('image2');
            $upimagename2 = rand(11111,99999).'-'.$upimage2->getClientOriginalName();
            $upimage2->move(storage_path().'/uploads/tpackage-image/', $upimagename2);
            $tpackage->image2 = $upimagename2;
        }

        if ($request->file('image3')) {
//            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image3);
            $upimage3 = $request->file('image3');
            $upimagename3 = rand(11111,99999).'-'.$upimage3->getClientOriginalName();
            $upimage3->move(storage_path().'/uploads/tpackage-image/', $upimagename3);
            $tpackage->image3 = $upimagename3;
        }

        if ($request->file('image4')) {
//            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image4);
            $upimage4 = $request->file('image4');
            $upimagename4 = rand(11111,99999).'-'.$upimage4->getClientOriginalName();
            $upimage4->move(storage_path().'/uploads/tpackage-image/', $upimagename4);
            $tpackage->image4 = $upimagename4;
        }


        $tpackage->price = $request->input('price');

        if($request->has('airfare_include')){
            $tpackage->airfare_include = true;
        }
        else{
            $tpackage->airfare_include = false;
        }

        if($request->has('airfare_exclude')){
            $tpackage->airfare_exclude = true;
        }
        else{
            $tpackage->airfare_exclude = false;
        }

        if($request->has('transport_include')){
            $tpackage->transport_include = true;
        }
        else{
            $tpackage->transport_include = false;
        }

        if($request->has('transport_exclude')){
            $tpackage->transport_exclude = true;
        }
        else{
            $tpackage->transport_exclude = false;
        }

        if($request->has('isactive')){
            $tpackage->isactive = true;
        }
        else{
            $tpackage->isactive = false;
        }


        $tpackage->property_type = $request->input('property_type');
        $tpackage->hotel_rating = $request->input('hotel_rating');
        $tpackage->check_in = $request->input('check_in');
        $tpackage->check_out = $request->input('check_out');
        $tpackage->pets = $request->input('pets');
        $tpackage->laundry_service = $request->input('laundry_service');
        $tpackage->free_wifi = $request->input('free_wifi');
        $tpackage->smoking_area = $request->input('smoking_area');
        $tpackage->resturent = $request->input('resturent');
        $tpackage->dry_cleaning = $request->input('dry_cleaning');
        $tpackage->hotel_description = $request->input('hotel_description');

        if($request->has('isactive')){
            $tpackage->isactive = true;
        }
        else{
            $tpackage->isactive = false;
        }

        $tpackage->jcountry_id = $request->input('country');

        $country = Jcountry::findOrFail($request->input('country'));
        $tpackage->jcontinent_id = $country->jcontinent->id;


        $tpackage->save();

//        $user = Tpackage::findOrFail($id);

        foreach ($request->input('pcategory') as $key => $value) {
            $tpackage->pcategory()->attach($value);
//            $tpackage->save();
//            $tpackage->attach($value);
        }


        return redirect()->route('alltpackage')
            ->with('success','Package created successfully');
    }


    public function editAction($id)
    {
        $tpackage = Tpackage::findOrFail($id);

        $pcategory = Pcategory::lists('name','id');
        $pkgCat = $tpackage->pcategory->lists('id','id')->toArray();


        $continent = Jcontinent::lists('name','id');
        $continent->prepend('Select Continent', '');

        $country = Jcountry::lists('name','id');
        $country->prepend('Select Country', '');


        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.tpackage.edit',compact('tpackage', 'pcategory', 'pkgCat', 'continent', 'country'));
        }

        else{
            return redirect()->back();
        }

    }


    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
//            'name' => 'required|unique:pcategory,name',
            'title' => 'required',
//            'image1' => 'required',
            'price' => 'required',
            'country' => 'required',
        ]);

        $tpackage = Tpackage::findOrFail($id);


        $tpackage->slug = null;
        $tpackage->title = $request->input('title');
        $tpackage->description = $request->input('description');

        if ($request->file('image1')) {
            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image1);
            $upimage1 = $request->file('image1');
            $upimagename1 = rand(11111,99999).'-'.$upimage1->getClientOriginalName();
            $upimage1->move(storage_path().'/uploads/tpackage-image/', $upimagename1);
            $tpackage->image1 = $upimagename1;
        }

        if ($request->file('image2')) {
            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image2);
            $upimage2 = $request->file('image2');
            $upimagename2 = rand(11111,99999).'-'.$upimage2->getClientOriginalName();
            $upimage2->move(storage_path().'/uploads/tpackage-image/', $upimagename2);
            $tpackage->image2 = $upimagename2;
        }

        if ($request->file('image3')) {
            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image3);
            $upimage3 = $request->file('image3');
            $upimagename3 = rand(11111,99999).'-'.$upimage3->getClientOriginalName();
            $upimage3->move(storage_path().'/uploads/tpackage-image/', $upimagename3);
            $tpackage->image3 = $upimagename3;
        }

        if ($request->file('image4')) {
            \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image4);
            $upimage4 = $request->file('image4');
            $upimagename4 = rand(11111,99999).'-'.$upimage4->getClientOriginalName();
            $upimage4->move(storage_path().'/uploads/tpackage-image/', $upimagename4);
            $tpackage->image4 = $upimagename4;
        }


        $tpackage->price = $request->input('price');

        if($request->has('airfare_include')){
            $tpackage->airfare_include = true;
        }
        else{
            $tpackage->airfare_include = false;
        }

        if($request->has('airfare_exclude')){
            $tpackage->airfare_exclude = true;
        }
        else{
            $tpackage->airfare_exclude = false;
        }

        if($request->has('transport_include')){
            $tpackage->transport_include = true;
        }
        else{
            $tpackage->transport_include = false;
        }

        if($request->has('transport_exclude')){
            $tpackage->transport_exclude = true;
        }
        else{
            $tpackage->transport_exclude = false;
        }

        if($request->has('isactive')){
            $tpackage->isactive = true;
        }
        else{
            $tpackage->isactive = false;
        }


        $tpackage->property_type = $request->input('property_type');
        $tpackage->hotel_rating = $request->input('hotel_rating');
        $tpackage->check_in = $request->input('check_in');
        $tpackage->check_out = $request->input('check_out');
        $tpackage->pets = $request->input('pets');
        $tpackage->laundry_service = $request->input('laundry_service');
        $tpackage->free_wifi = $request->input('free_wifi');
        $tpackage->smoking_area = $request->input('smoking_area');
        $tpackage->resturent = $request->input('resturent');
        $tpackage->dry_cleaning = $request->input('dry_cleaning');
        $tpackage->hotel_description = $request->input('hotel_description');

        if($request->has('isactive')){
            $tpackage->isactive = true;
        }
        else{
            $tpackage->isactive = false;
        }

        $tpackage->jcountry_id = $request->input('country');

        $country = Jcountry::findOrFail($request->input('country'));
        $tpackage->jcontinent_id = $country->jcontinent->id;

        $tpackage->save();

        \DB::table('pcategory_tpackage')->where('tpackage_id',$id)->delete();
        foreach ($request->input('pcategory') as $key => $value) {
            $tpackage->pcategory()->attach($value);
        }


        return redirect()->route('alltpackage')
            ->with('success','Package update successfully');


    }

    public function destroyAction($id)
    {

        $tpackage = Tpackage::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){

            if($tpackage->image1){
                \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image1);
            }
            if($tpackage->image2){
                \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image2);
            }
            if($tpackage->image3){
                \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image3);
            }
            if($tpackage->image4){
                \File::delete(storage_path().'/uploads/tpackage-image/'.$tpackage->image4);
            }

            $tpackage->delete();
            return redirect()->route('alltpackage')
                ->with('success','Package deleted successfully');
        }

        else{
            return redirect()->back();
        }






    }

}
