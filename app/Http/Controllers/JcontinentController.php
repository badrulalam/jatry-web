<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Tpackage;
use App\Jcontinent;
use App\Jcountry;
use Auth;
use Session;


class JcontinentController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAction(Request $request)
    {

        $user = \Auth::user();
        if($user->hasRole('superadmin')){
            $data = Jcontinent::orderBy('name','ASC')
                ->paginate(10);
            return view('adminarea.jcontinent.index',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 10);

        }
        else{
            return redirect()->back();
        }




    }



    public function newAction()
    {
        return view('adminarea.jcontinent.new');
    }


    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:jcontinent,name',
            'image' => 'required'
        ]);

        $jcontinent = new Jcontinent();
        $jcontinent->name = $request->input('name');
        $jcontinent->description = $request->input('description');

        if ($request->file('image')) {
//            \File::delete(storage_path().'/uploads/agent-image/'.$user->agent->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/jcontinent-image/', $upimagename);
            $jcontinent->image = $upimagename;
        }

        $jcontinent->save();
        return redirect()->route('alljcontinent')
            ->with('success','Continent created successfully');
    }


    public function editAction($id)
    {
        $jcontinent = Jcontinent::findOrFail($id);


        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.jcontinent.edit',compact('jcontinent'));
        }

        else{
            return redirect()->back();
        }

    }


    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required|unique:jcontinent,name,'.$id
//            'image' => 'required'
        ]);

        $jcontinent = Jcontinent::findOrFail($id);

        $jcontinent->slug = null;
        $jcontinent->name = $request->input('name');
        $jcontinent->description = $request->input('description');
        if ($request->file('image')) {
            \File::delete(storage_path().'/uploads/jcontinent-image/'.$jcontinent->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/jcontinent-image/', $upimagename);
            $jcontinent->image = $upimagename;
        }
        $jcontinent->save();
        return redirect()->route('alljcontinent')
            ->with('success','Continent created successfully');
    }


    public function destroyAction($id)
    {

        $jcontinent = Jcontinent::findOrFail($id);
        if(\Auth::user()->hasRole('superadmin')){
            \File::delete(storage_path().'/uploads/jcontinent-image/'.$jcontinent->image);
            $jcontinent->delete();
            return redirect()->route('alljcontinent')
                ->with('success','Continent deleted successfully');
        }
        else{
            return redirect()->back();
        }






    }

}
