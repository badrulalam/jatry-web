<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Pcategory;
use App\Jcontinent;
use App\Jcountry;
use App\Tpackage;
use Auth;
use Session;



class TourController extends Controller
{
    public function __construct()
    {

    }



    public function allTourAction(Request $request)
    {
        $fepackage = Tpackage::where('isactive', 1)
            ->where('isfeatured', 0)
            ->orderBy('id','ASC')->paginate(3);

        $data = Jcountry::orderBy('id','ASC')->paginate(10);
        return view('frontend.tour.country-tour',compact('data','fepackage'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


    public function singleCountryTourAction(Request $request,$slug)
    {
        return view('frontend.tour.single-country-tour');

    }

    public function singleCountryTourApiAction(Request $request,$slug)
    {
        $jcountry = Jcountry::where('slug', $slug)->first();
if($jcountry){
    $qry = Tpackage::where('isactive', 1)
        ->where('jcountry_id', $jcountry->id);
//        ->orderBy('id','ASC');

    $filterArr = array(
    'country' => $jcountry->name,
    'airfare_include' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('airfare_include', 1)->count(),
    'airfare_exclude' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('airfare_exclude', 1)->count(),
    'transport_include' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('transport_include', 1)->count(),
    'transport_exclude' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('transport_exclude', 1)->count(),
    'hotel' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('property_type', 'hotel')->count(),
    'resorts' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('property_type', 'resorts')->count(),
    'cruiseship' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('property_type', 'cruiseship')->count(),
    'hotel_rating1' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('hotel_rating', 1)->count(),
    'hotel_rating2' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('hotel_rating', 2)->count(),
    'hotel_rating3' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('hotel_rating', 3)->count(),
    'hotel_rating4' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('hotel_rating', 4)->count(),
    'hotel_rating5' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->where('hotel_rating', 5)->count(),
    'min_price' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->min('price'),
    'max_price' => Tpackage::where('isactive', 1)->where('jcountry_id', $jcountry->id)->max('price')
);

    $data = Tpackage::where('isactive', 1)
        ->where('jcountry_id', $jcountry->id)
        ->orderBy('id','ASC');
//    return $data->get();

    $dataArr = array(
        'filterData' =>$filterArr,
        'showData' =>$data->get()
    );
    return $dataArr;
}
else{
    return "not found";}
}

    public function allCountryTourAction(Request $request)
    {
        return view('frontend.tour.all-country-tour');

    }

    public function allCountryTourApiAction(Request $request)
    {
        $jcountry = Jcountry::where('isactive', 1)
            ->orderBy('id','ASC');


        if($jcountry){

            $filterContryArr = array();
            $qry = Tpackage::where('isactive', 1);


            foreach($jcountry->get() as $jcon){
                $filterContryArr[$jcon->name] = Tpackage::where('isactive', 1)->where('jcountry_id', $jcon->id)->count();
            }

            $filterArr = array(
                'airfare_include' => Tpackage::where('isactive', 1)->where('airfare_include', 1)->count(),
                'airfare_exclude' => Tpackage::where('isactive', 1)->where('airfare_exclude', 1)->count(),
                'transport_include' => Tpackage::where('isactive', 1)->where('transport_include', 1)->count(),
                'transport_exclude' => Tpackage::where('isactive', 1)->where('transport_exclude', 1)->count(),
                'hotel' => Tpackage::where('isactive', 1)->where('property_type', 'hotel')->count(),
                'resorts' => Tpackage::where('isactive', 1)->where('property_type', 'resorts')->count(),
                'cruiseship' => Tpackage::where('isactive', 1)->where('property_type', 'cruiseship')->count(),
                'hotel_rating1' => Tpackage::where('isactive', 1)->where('hotel_rating', 1)->count(),
                'hotel_rating2' => Tpackage::where('isactive', 1)->where('hotel_rating', 2)->count(),
                'hotel_rating3' => Tpackage::where('isactive', 1)->where('hotel_rating', 3)->count(),
                'hotel_rating4' => Tpackage::where('isactive', 1)->where('hotel_rating', 4)->count(),
                'hotel_rating5' => Tpackage::where('isactive', 1)->where('hotel_rating', 5)->count(),
                'min_price' => Tpackage::where('isactive', 1)->min('price'),
                'max_price' => Tpackage::where('isactive', 1)->max('price')
            );

            $data = Tpackage::where('isactive', 1)
                ->orderBy('id','ASC');

            $dataArr = array(
//                'countFiltr'=>$filterContryArr,
                'countFiltr'=>$filterContryArr,
                'allcountry' => $jcountry->get(),
                'filterData' =>$filterArr,
                'showData' =>$data->get(),
//                'countFiltr'=>$filterContryArr
            );
            return $dataArr;
        }
        else{
            return "not found";}
    }


    public function packageDetailsAction(Request $request,$slug)
    {
        $tpackage = Tpackage::where('slug', $slug);

        return view('frontend.tour.package-details');
//        return $tpackage;

    }

    public function selectPackageDetailsApiAction(Request $request,$slug)
    {

        $jcountry = Jcountry::where('isactive', 1)
            ->orderBy('id','ASC');

//        $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug)->first();
        $tpackage = Tpackage::where('isactive', 1)->where('slug', $slug);



        if($tpackage){

            $filterContryArr = array();
            foreach($jcountry->get() as $jcon){
                $filterContryArr[$jcon->name] = $jcon->slug;
            }

//            $data = Tpackage::where('isactive', 1)
//                ->where('jcountry_id', $jcountry->id)
//                ->orderBy('id','ASC');

            $dataArr = array(
                'countFiltr'=>$filterContryArr,
                'showData' =>$tpackage->first()
            );
            return $dataArr;
        }
        else{
            return "not found";}
    }



}
