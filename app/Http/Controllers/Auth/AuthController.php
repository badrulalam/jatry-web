<?php

namespace App\Http\Controllers\Auth;

use Auth;
//use Illuminate\Auth\Access\Response;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
//use Illuminate\Routing\Router;

use App\User;
use App\Role;
use App\Agency;
use DB;
use Mail;
use App\Customer;
use App\Agent;
use Session;
use MyFuncs;
use Socialite;


use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';
    protected $redirectTo = '/home';
//    protected $loginPath = '/customer-login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'userregister', 'userregistersave', 'frontAgentLoginAction', 'handleProviderCallback']]);
    }





/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function doLogin_org(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

//        $email = Request::input('email');
        $email = $request->input('email');

//        $password = Request::input('password');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
//            return redirect()->intended('dashboard');
            return redirect()->route('allusers');
        }
    }




    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

//        if ($this->auth->attempt($credentials, $request->has('remember')))
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole('superadmin')){
                return redirect()->route('admindashboard');
            }
           elseif(Auth::user()->hasRole('superagent')){
                return redirect()->route('agencydashboard');
            }
            elseif(Auth::user()->hasRole('agent')){
                return redirect()->route('agentdashboard');
            }
//            return redirect()->back();
//            return redirect()->route('admindashboard');
            return 'You are may be customer';

        }

        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
//                'email' => $this->getFailedLoginMesssage(),
//                'crederr' => $this->getFailedLoginMesssage(),
                  'crederr' => $this->getFailedLoginMessage(),
            ]);
    }

//    protected function getFailedLoginMesssage()
//    {
//        return Lang::has('auth.failed')
//            ? Lang::get('auth.failed')
//            : 'These credentials do not match our recordsDDD.';
//    }






















    protected function userregister()
    {
        $roles = Role::lists('display_name','id');
        return view('auth.userregister',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function userregistersave(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }


    protected function agentRegisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }







    public function frontAgentLoginDOAction(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole(['superagent', 'agent']) ){
//                return 'superagent'.Auth::user()->agency->id;
                return redirect()->route('agentmyaccount');
            }
        }
        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'crederr' => $this->getFailedLoginMessage(),
            ]);
    }




    public function customerLoginDOAction(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole('customer')){
                /*
                 * redirect to flight page ...
                 * */
                if(Session::get('redirect')){
                    $abs_url = Session::get('redirect');
                    Session::forget('redirect');
                    return redirect($abs_url);
                }
                return redirect()->route('home');
//                return redirect()->route('customermyaccount');
//                return redirect()->route('customermyaccount')
//                    ->with('success','Customer created successfully.Now u can login with your user/pass');
            }
        }
        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'crederr' => $this->getFailedLoginMessage(),
            ]);
    } 
    public function customerLoginDoWithBookNowAction(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            return (new Response(json_decode(1), 200))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
        return (new Response(json_decode(0), 200))
            ->header('Content-Type', 'application/json; charset=utf-8');
    }


    public function customerpackageLoginDoAction(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {


//            return $request->input('package');

            if(Auth::user()->hasRole('customer')){
                return redirect()->route('package-details',['slug' => $request->input('package')]);


//                return redirect()->route('customermyaccount')
//                    ->with('success','Customer created successfully.Now u can login with your user/pass');
            }
        }
        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'crederr' => $this->getFailedLoginMessage(),
            ]);
    }


//    =========================Facebook Login Start===================================================

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {


       try{
           $fbUser = Socialite::driver('facebook')->user();

       }
       catch( \Exception $e){

          return redirect('home');
       }

        $usermail = User::where('email', $fbUser->getEmail())->first();

        if($usermail){
            if($usermail->facebook_id == null || $usermail->facebook_id == ''){
                $usermail->facebook_id = $fbUser->getId();
                $usermail->save();
            }
        }






       $findfbuser = User::where('facebook_id', $fbUser->getId())->first();

        if(!$findfbuser){
            $svuser =  User::create([
                'facebook_id' => $fbUser->getId(),
                'name' => $fbUser->getName(),
                'email' => $fbUser->getEmail(),
            ]);


//            ----------------




//            ----------------





            auth()->login($svuser);
//            if(Session::get('redirect')){
//                $abs_url = Session::get('redirect');
//                Session::forget('redirect');
//                return redirect($abs_url);
//            }
            return redirect()->route('home');

        }
        else{
            auth()->login($findfbuser);

//            if(Session::get('redirect')){
//                $abs_url = Session::get('redirect');
//                Session::forget('redirect');
//                return redirect($abs_url);
//            }
            return redirect()->route('home');
        }







//        $user = Socialite::driver('facebook')->user();
//        $fbArr = array(
//            'token' => $user->token,
//            'getId' => $user->getId(),
//            'getNickname' => $user->getNickname(),
//            'getName' => $user->getName(),
//            'getEmail' => $user->getEmail(),
//            'getAvatar' => $user->getAvatar(),
//        );
//        return $fbArr;

//        return $user->getEmail();


        // $user->token;
    }



//    =========================Facebook Login End=====================================================



    protected function customerRegisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);


        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $role = Role::where('name', 'customer')->first();
        $user->attachRole($role->id);

        $customer = new Customer();
        $customer->email = $request->input('email');
        $customer->user_id = $user->id;

        if(MyFuncs::getAgency()){
            $customer->agency_id = MyFuncs::getAgency()->id;
            $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

            $customer->byjatry = false;
            $customer->byagency = true;
            $customer->byagent = false;
        }
        else{
            $customer->agency_id = null;
            $customer->agent_id = null;

            $customer->byjatry = true;
            $customer->byagency = false;
            $customer->byagent = false;

        }

        $customer->save();
        $customer->code = 'CUS-'.$customer->id;;
        $customer->save();

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole('customer')){

                $reguser = Auth::user();
//               ========Mail Start=============
                Mail::send('emails.registermail', ['name' => $reguser->name, 'content' => 'Thank you for registration.'], function ($message) use ($reguser)
                {
//            $message->from('no-reply@zeteq.com', 'Christian Nwamba');
                    $message->sender('vincej1657@gmail.com');
                    $message->subject('Jatry Registration');
                    $message->to($reguser->email);
//            $message->cc('limon3640@gmail.com', $name = null);
//                    $message->cc('zeshaq@gmail.com', $name = null);
//            $message->getSwiftMessage();

                });

//               ========Mail End=============





                if(Session::get('redirect')){
                    $abs_url = Session::get('redirect');
                    Session::forget('redirect');
                    return redirect($abs_url);
                }



                        return redirect()->route('customerregistersuccess')
            ->with('success','welcome to jatry.com');




//                return redirect()->route('customermyaccount');
            }
        }

//        return redirect()->route('customerlogin')
//            ->with('success','Customer created successfully.Now u can login with your user/pass');
    }
    protected function customerRegisterSaveWithBookingAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);


        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $role = Role::where('name', 'customer')->first();
        $user->attachRole($role->id);

        $customer = new Customer();
        $customer->email = $request->input('email');
        $customer->user_id = $user->id;

        if(MyFuncs::getAgency()){
            $customer->agency_id = MyFuncs::getAgency()->id;
            $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

            $customer->byjatry = false;
            $customer->byagency = true;
            $customer->byagent = false;
        }
        else{
            $customer->agency_id = null;
            $customer->agent_id = null;

            $customer->byjatry = true;
            $customer->byagency = false;
            $customer->byagent = false;

        }

        $customer->save();
        $customer->code = 'CUS-'.$customer->id;;
        $customer->save();

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole('customer')){

                $reguser = Auth::user();
//               ========Mail Start=============
                Mail::send('emails.registermail', ['name' => $reguser->name, 'content' => 'Thank you for registration.'], function ($message) use ($reguser)
                {
//            $message->from('no-reply@zeteq.com', 'Christian Nwamba');
                    $message->sender('vincej1657@gmail.com');
                    $message->subject('Jatry Registration');
                    $message->to($reguser->email);
//            $message->cc('limon3640@gmail.com', $name = null);
//                    $message->cc('zeshaq@gmail.com', $name = null);
//            $message->getSwiftMessage();

                });

//               ========Mail End=============




                if (Auth::attempt($credentials, $request->has('remember')))
                {
                    return (new Response(json_decode(1), 200))
                        ->header('Content-Type', 'application/json; charset=utf-8');
                }
                return (new Response(json_decode(0), 200))
                    ->header('Content-Type', 'application/json; charset=utf-8');




//                return redirect()->route('customermyaccount');
            }
        }

//        return redirect()->route('customerlogin')
//            ->with('success','Customer created successfully.Now u can login with your user/pass');
    }



}
