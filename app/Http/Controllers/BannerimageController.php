<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Bannerimage;
use DB;
use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use Auth;

use Session;


class BannerimageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function indexAction(Request $request)
    {
        $user = \Auth::user();


        if($user->hasRole(['superadmin', 'superagent'])){
            if($user->agency){
                $data = Bannerimage::where('agency_id', $user->agency->id)
                ->orderBy('id','ASC')
                    ->paginate(10);
            }
            else{
                $data = Bannerimage::orderBy('agency_id','ASC')->paginate(10);

            }

        }



        return view('adminarea.bannerimage.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


    public function newAction()
    {
        return view('adminarea.bannerimage.new');
    }

    public function createAction(Request $request)
    {

        $user = \Auth::user();

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required'
        ]);

        $bannerimage = new Bannerimage();
        $bannerimage->title = $request->input('title');

//        if ($request->file('image')) {
//            $bannerimage = $request->file('image');
//            $bannerimagename = rand(11111,99999).'-'.$bannerimage->getClientOriginalName();
//            $bannerimage->move(storage_path().'/uploads/banner-image/', $bannerimagename);
//            $bannerimage->image = $bannerimagename;
//        }


        if ($request->file('image')) {
//            \File::delete(storage_path().'/uploads/agent-image/'.$user->agent->image);
            $agentimage = $request->file('image');
            $agentimagename = rand(11111,99999).'-'.$agentimage->getClientOriginalName();
            $agentimage->move(storage_path().'/uploads/banner-image/', $agentimagename);
            $bannerimage->image = $agentimagename;
        }


        if($request->has('isactive')){
            $bannerimage->isactive = true;
        }
        else{
            $bannerimage->isactive = false;
        }


        if($user->hasRole(['superadmin', 'superagent'])){
            if($user->agency){
                $bannerimage->agency_id = $user->agency->id;
                $bannerimage->isjatry = false;
            }
            else{
                $bannerimage->isjatry = true;

            }

        }

        $bannerimage->save();
        return redirect()->route('allbannerimage')
            ->with('success','Banerimage created successfully');
    }


    public function editAction($id)
    {
        $bannerimage = Bannerimage::findOrFail($id);


        if(\Auth::user()->hasRole('superadmin') && ($bannerimage->isjatry)){
            return view('adminarea.bannerimage.edit',compact('bannerimage'));
        }
        elseif(\Auth::user()->hasRole('superagent') && ($bannerimage->agency_id == \Auth::user()->agency->id)){
            return view('adminarea.bannerimage.edit',compact('bannerimage'));
        }

        else{
            return redirect()->route('allbannerimage')
                ->with('success','No right to edit');
        }

    }


    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'title' => 'required',
//            'image' => 'required'
        ]);

        $bannerimage = Bannerimage::find($id);
        $bannerimage->title = $request->input('title');
        if ($request->file('image')) {
            \File::delete(storage_path().'/uploads/banner-image/'.$bannerimage->image);
            $agentimage = $request->file('image');
            $agentimagename = rand(11111,99999).'-'.$agentimage->getClientOriginalName();
            $agentimage->move(storage_path().'/uploads/banner-image/', $agentimagename);
            $bannerimage->image = $agentimagename;
        }


        if($request->has('isactive')){
            $bannerimage->isactive = true;
        }
        else{
            $bannerimage->isactive = false;
        }
        $bannerimage->save();



        return redirect()->route('allbannerimage')
            ->with('success','Banerimage updated successfully');
    }


    public function destroyAction($id)
    {

        $bannerimage = Bannerimage::findOrFail($id);

//        DB::table("bannerimage")->where('id',$id)->delete();


        if(\Auth::user()->hasRole('superadmin') && ($bannerimage->isjatry)){
            $bannerimage->delete();
            return redirect()->route('allbannerimage')
                ->with('success','Banner image deleted successfully');
        }
        elseif(\Auth::user()->hasRole('superagent') && ($bannerimage->agency_id == \Auth::user()->agency->id)){
            $bannerimage->delete();
            return redirect()->route('allbannerimage')
                ->with('success','Banner image deleted successfully');
        }

        else{
            return redirect()->route('allbannerimage')
                ->with('success','Banner image deleted successfully');
        }



    }
}
