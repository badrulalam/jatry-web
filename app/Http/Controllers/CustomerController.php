<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Routesearch;
use Illuminate\Http\Request;

use App\Http\Requests;

use Redirect;
use URL;
use Validator;


use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use App\Passengerforselect;
use App\Cardinfo;

use Session;
use MyFuncs;

use Socialite;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class CustomerController extends Controller
{


    public function __construct()
    {

    }

    public function customerRegisterAction()
    {
        return view('customer.customerregister');
    }

    public function customerRegisterSuccessAction()
    {
        return view('customer.register-success');
    }


    protected function customerRegisterSaveAction_bf_Use(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',


            'customer_firstname' => 'required',
//            'customer_lastname' => 'required',
            'customer_email' => 'required|email|unique:customer,email',
            'customer_address' => 'required',
            'customer_city' => 'required',
            'customer_state' => 'required',
            'customer_postalcode' => 'required',
            'customer_country' => 'required',
            'customer_phone' => 'required',
            'customer_image' => 'required',

        ]);


        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();


        $role = Role::where('name', 'customer')->first();
        $user->attachRole($role->id);


        $customer = new Customer();

        $customer->first_name = $request->input('customer_firstname');
        $customer->last_name = $request->input('customer_lastname');
        $customer->email = $request->input('customer_email');
        $customer->address = $request->input('customer_address');
        $customer->city = $request->input('customer_city');
        $customer->state = $request->input('customer_state');
        $customer->postalcode = $request->input('customer_postalcode');
        $customer->country = $request->input('customer_country');
        $customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

        if ($request->file('customer_image')) {
//            \File::delete(storage_path().'/uploads/customer-image/'.$user->agent->image);
            $customerimage = $request->file('customer_image');
            $customerimagename = rand(11111, 99999) . '-' . $customerimage->getClientOriginalName();
            $customerimage->move(storage_path() . '/uploads/customer-image/', $customerimagename);
            $customer->image = $customerimagename;
        }

        $customer->user_id = $user->id;

        if (MyFuncs::getAgency()) {
            $customer->agency_id = MyFuncs::getAgency()->id;
            $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

            $customer->byjatry = false;
            $customer->byagency = true;
            $customer->byagent = false;
        } else {
            $customer->agency_id = null;
            $customer->agent_id = null;

            $customer->byjatry = true;
            $customer->byagency = false;
            $customer->byagent = false;

        }

        $customer->save();
        $customer->code = 'CUS-' . $customer->id;;
        $customer->save();


//        $agency = new Agency();
//        $agency->name = $request->input('agency_name');
//        $agency->email = $request->input('agency_email');
//        $agency->url = $request->input('agency_url');
//        $agency->isactive = true;
//        $agency->user_id = $user->id;
//        $agency->save();
//
//        $agency->code = 'ACY-'.$agency->id;;
//        $agency->save();


        return redirect()->route('customerlogin')
            ->with('success', 'Customer created successfully.Now u can login with your user/pass');
    }


    public function customerLoginAction(Request $request)
    {
        if ($request->input('redirect')) {
            Session::put('redirect', $request->input('redirect'));
        }


        return view('customer.customerlogin');
    }

    public function customerMyAccountAction(Request $request)
    {
        $user = \Auth::user();

        $selpassenegr = Passengerforselect::where('user_id', $user->id)
            ->orderBy('id', 'ASC')->get();

        $cardinfo = Cardinfo::where('user_id', $user->id)
            ->orderBy('id', 'ASC')->get();
        $routeSearch = Routesearch::where('user_id', $user->id)
            ->orderBy('id', 'ASC')->get();

        return view('customer.myaccount', ['user' => $user, 'selpassenegr' => $selpassenegr, 'cardinfo' => $cardinfo, 'routeSearch'=>$routeSearch]);
//        return view('agent.myaccount', compact(user));
    }

    public function MyAccountSaveAction_bf_change(Request $request)
    {

        $user = \Auth::user();
        $this->validate($request, [

            'user_name' => 'required',
            'user_email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'same:confirm-password',


            'customer_firstname' => 'required',
//            'customer_lastname' => 'required',
            'customer_email' => 'required|email|unique:customer,email,' . $user->customer->id,
            'customer_address' => 'required',
            'customer_city' => 'required',
            'customer_state' => 'required',
            'customer_postalcode' => 'required',
            'customer_country' => 'required',
            'customer_phone' => 'required',
//            'customer_image' => 'required',

        ]);


        $user->name = $request->input('user_name');
        $user->email = $request->input('user_email');

        if (!empty($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();


        $user->customer->first_name = $request->input('customer_firstname');
        $user->customer->last_name = $request->input('customer_lastname');
        $user->customer->email = $request->input('customer_email');
        $user->customer->address = $request->input('customer_address');
        $user->customer->city = $request->input('customer_city');
        $user->customer->state = $request->input('customer_state');
        $user->customer->postalcode = $request->input('customer_postalcode');
        $user->customer->country = $request->input('customer_country');
        $user->customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

        if ($request->file('customer_image')) {
            \File::delete(storage_path() . '/uploads/customer-image/' . $user->customer->image);
            $customerimage = $request->file('customer_image');
            $customerimagename = rand(11111, 99999) . '-' . $customerimage->getClientOriginalName();
            $customerimage->move(storage_path() . '/uploads/customer-image/', $customerimagename);
            $user->customer->image = $customerimagename;
        }
        $user->customer->save();


        return view('customer.myaccount', ['user' => $user]);
    }


    public function allCustomerListAction(Request $request)
    {
        $data = Customer::orderBy('id', 'ASC')->paginate(10);
        return view('adminarea.customer.allcustomerlist', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


//    =======================Facebook Start=============

    public function handleProviderCallback()
    {
        try {
            $fbUser = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            return redirect('home');
        }

        $usermail = User::where('email', $fbUser->getEmail())->first();

        if ($usermail) {
            if ($usermail->facebook_id == null || $usermail->facebook_id == '') {
                $usermail->facebook_id = $fbUser->getId();
                $usermail->save();
//                $usermail->customer->image = $fbUser->getAvatar();
//                $usermail->customer->save();
            }
        }

        $findfbuser = User::where('facebook_id', $fbUser->getId())->first();
        if (!$findfbuser) {
//            $svuser =  User::create([
//                'facebook_id' => $fbUser->getId(),
//                'name' => $fbUser->getName(),
//                'email' => $fbUser->getEmail(),
//            ]);

//            ----------------
            $user = new User();
            $user->facebook_id = $fbUser->getId();
            $user->name = $fbUser->getName();
            $user->email = $fbUser->getEmail();
//            $user->password = bcrypt($request->input('password'));
            $user->save();

            $role = Role::where('name', 'customer')->first();
            $user->attachRole($role->id);

            $customer = new Customer();

            $customer->first_name = $fbUser->getName();
//            $customer->last_name = $request->input('customer_lastname');
            $customer->email = $fbUser->getEmail();
//            $customer->address = $request->input('customer_address');
//            $customer->city = $request->input('customer_city');
//            $customer->state = $request->input('customer_state');
//            $customer->postalcode = $request->input('customer_postalcode');
//            $customer->country = $request->input('customer_country');
//            $customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

//            $customer->image = $fbUser->getAvatar();

            $customer->user_id = $user->id;
            if (MyFuncs::getAgency()) {
                $customer->agency_id = MyFuncs::getAgency()->id;
                $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

                $customer->byjatry = false;
                $customer->byagency = true;
                $customer->byagent = false;
            } else {
                $customer->agency_id = null;
                $customer->agent_id = null;

                $customer->byjatry = true;
                $customer->byagency = false;
                $customer->byagent = false;

            }

            $customer->save();
            $customer->code = 'CUS-' . $customer->id;;
            $customer->save();


//            ----------------

            auth()->login($user);
            return redirect()->route('home');

        } else {
            auth()->login($findfbuser);
            return redirect()->route('home');
        }


    }
    public function handleProviderCallbackJWT(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage['invalidAttributes'] = $validator->errors();
            return response()->json($errorMessage, 400);
        }




        $usermail = User::where('email', $request->input('email'))->first();

        if ($usermail) {
            if ($usermail->facebook_id == null || $usermail->facebook_id == '') {
                $usermail->facebook_id = $request->input('id');
                $usermail->save();
            }
        }

        $findfbuser = User::where('facebook_id', $request->input('id'))->first();
        if (!$findfbuser) {
//            $svuser =  User::create([
//                'facebook_id' => $fbUser->getId(),
//                'name' => $fbUser->getName(),
//                'email' => $fbUser->getEmail(),
//            ]);

//            ----------------
            $user = new User();
            $user->facebook_id = $request->input('id');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
//            $user->password = bcrypt($request->input('password'));
            $user->save();

            $role = Role::where('name', 'customer')->first();
            $user->attachRole($role->id);

            $customer = new Customer();

            $customer->first_name =  $request->input('name');
//            $customer->last_name = $request->input('customer_lastname');
            $customer->email = $request->input('email');
//            $customer->address = $request->input('customer_address');
//            $customer->city = $request->input('customer_city');
//            $customer->state = $request->input('customer_state');
//            $customer->postalcode = $request->input('customer_postalcode');
//            $customer->country = $request->input('customer_country');
//            $customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

//            $customer->image = $fbUser->getAvatar();

            $customer->user_id = $user->id;
            if (MyFuncs::getAgency()) {
                $customer->agency_id = MyFuncs::getAgency()->id;
                $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

                $customer->byjatry = false;
                $customer->byagency = true;
                $customer->byagent = false;
            } else {
                $customer->agency_id = null;
                $customer->agent_id = null;

                $customer->byjatry = true;
                $customer->byagency = false;
                $customer->byagent = false;

            }

            $customer->save();
            $customer->code = 'CUS-' . $customer->id;;
            $customer->save();


//            ----------------

            auth()->login($user);
            $token = JWTAuth::fromUser($user);
            return response()->json($token);

        } else {
            auth()->login($findfbuser);

            $token = JWTAuth::fromUser($findfbuser);
            return response()->json(['token' => $token]);
        }


    }

//    =======================Facebook End===============

    public function MyAccountLoginInfoSaveAction(Request $request)
    {

        $user = \Auth::user();
        $this->validate($request, [

            'user_name' => 'required',
            'user_email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'same:confirm-password',
        ]);


        $user->name = $request->input('user_name');
        $user->email = $request->input('user_email');

        if (!empty($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

//        return Redirect::to('/customer-my-account#myAccount');
        return Redirect::to(URL::previous() . "#myAccount")->with('loginsuccess', 'Login information saved successfully');
    }

    public function getProfileJWT()
    {
        $user = \Auth::user()->customer;

        return response()->json( $user,200);
    }

    public function setProfileJWT(Request $request)
    {
        $user = \Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:customer,email,' . $user->customer->id,
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postalcode' => 'required',
            'country' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            $errorMessage['invalidAttributes'] = $validator->errors();
            return response()->json($errorMessage, 400);
        }

        $user->customer->first_name = $request->input('first_name');
        $user->customer->last_name = $request->input('last_name');
        $user->customer->email = $request->input('email');
        $user->customer->address = $request->input('address');
        $user->customer->city = $request->input('city');
        $user->customer->state = $request->input('state');
        $user->customer->postalcode = $request->input('postalcode');
        $user->customer->country = $request->input('country');
        $user->customer->phone = $request->input('phone');
        $user->customer->save();

        return response()->json($user->customer,200);
    }

    public function MyAccountBillingInfoSaveAction(Request $request)
    {

        $user = \Auth::user();

        $validator = Validator::make($request->all(), [
            'customer_firstname' => 'required',
            'customer_email' => 'required|email|unique:customer,email,' . $user->customer->id,
            'customer_address' => 'required',
            'customer_city' => 'required',
            'customer_state' => 'required',
            'customer_postalcode' => 'required',
            'customer_country' => 'required',
            'customer_phone' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/customer-my-account#billingAddress')->withErrors($validator)->withInput();
        }

        $user->customer->first_name = $request->input('customer_firstname');
        $user->customer->last_name = $request->input('customer_lastname');
        $user->customer->email = $request->input('customer_email');
        $user->customer->address = $request->input('customer_address');
        $user->customer->city = $request->input('customer_city');
        $user->customer->state = $request->input('customer_state');
        $user->customer->postalcode = $request->input('customer_postalcode');
        $user->customer->country = $request->input('customer_country');
        $user->customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

        if ($request->file('customer_image')) {
            \File::delete(storage_path() . '/uploads/customer-image/' . $user->customer->image);
            $customerimage = $request->file('customer_image');
            $customerimagename = rand(11111, 99999) . '-' . $customerimage->getClientOriginalName();
            $customerimage->move(storage_path() . '/uploads/customer-image/', $customerimagename);
            $user->customer->image = $customerimagename;
        }
        $user->customer->save();


        return Redirect::to('/customer-my-account#billingAddress')->with('billingsuccess', 'Billing information saved successfully');
    }


    public function MyAccountShippingInfoSaveAction(Request $request)
    {

        $user = \Auth::user();


        $validator = Validator::make($request->all(), [
            'shipping_firstname' => 'required',
            'shipping_email' => 'required|email|unique:customer,shipping_email,' . $user->customer->id,
            'shipping_address' => 'required',
            'shipping_city' => 'required',
            'shipping_state' => 'required',
            'shipping_postalcode' => 'required',
            'shipping_country' => 'required',
            'shipping_phone' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/customer-my-account#shippingAddress')->withErrors($validator)->withInput();
        }


        $user->customer->shipping_firstname = $request->input('shipping_firstname');
        $user->customer->shipping_lastname = $request->input('shipping_lastname');
        $user->customer->shipping_email = $request->input('shipping_email');
        $user->customer->shipping_address = $request->input('shipping_address');
        $user->customer->shipping_city = $request->input('shipping_city');
        $user->customer->shipping_state = $request->input('shipping_state');
        $user->customer->shipping_postalcode = $request->input('shipping_postalcode');
        $user->customer->shipping_country = $request->input('shipping_country');
        $user->customer->shipping_phone = $request->input('shipping_phone');
        $user->customer->save();

        return Redirect::to('/customer-my-account#shippingAddress')->with('shippingsuccess', 'Shipping information saved successfully');
    }


    public function customerAddTravelerAction(Request $request)
    {
        return view('customer.add-tarveler');
    }


    public function customerAddTravelerSaveAction(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'title' => 'required',
//            'shipping_email' => 'required|email|unique:customer,shipping_email,'.$user->customer->id,
            'first_name' => 'required',
            'dob' => 'required',
        ]);
        $passengerforselect = new Passengerforselect();
        $passengerforselect->user_id = $user->id;
        $passengerforselect->title = $request->input('title');
        $passengerforselect->first_name = $request->input('first_name');
        $passengerforselect->middle_name = $request->input('middle_name');
        $passengerforselect->last_name = $request->input('last_name');
        $passengerforselect->dob = $request->input('dob');
        $passengerforselect->save();

        return Redirect::to('/customer-my-account#traveler')->with('travelersuccess', 'Traveler saved successfully');
    }

    public function customerAddTravelerEditAction($id)
    {

        $passengerforselect = Passengerforselect::findOrFail($id);
        return view('customer.edit-tarveler', compact('passengerforselect'));
    }


    public function customerAddTravelerUpdateAction(Request $request, $id)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'title' => 'required',
//            'shipping_email' => 'required|email|unique:customer,shipping_email,'.$user->customer->id,
            'first_name' => 'required',
            'dob' => 'required',
        ]);


        $passengerforselect = Passengerforselect::findOrFail($id);


        $passengerforselect->user_id = $user->id;
        $passengerforselect->title = $request->input('title');
        $passengerforselect->first_name = $request->input('first_name');
        $passengerforselect->middle_name = $request->input('middle_name');
        $passengerforselect->last_name = $request->input('last_name');
        $passengerforselect->dob = $request->input('dob');
        $passengerforselect->save();

        return Redirect::to('/customer-my-account#traveler')->with('travelersuccess', 'Traveler saved successfully');
    }

    public function customerAddcardAction(Request $request)
    {
        return view('customer.add-card-info');
    }

    public function customerAddcardSaveAction(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'card_firstname' => 'required',
            'card_lastname' => 'required',
            'card_email' => 'required|email',
            'card_address' => 'required',
            'card_city' => 'required',
            'card_state' => 'required',
            'card_postalcode' => 'required',
            'card_country' => 'required',
            'card_phone' => 'required',
            'card_holder' => 'required',
            'card_number' => 'required',
            'card_cvv' => 'required',
            'card_expdate' => 'required',
        ]);


        $cardinfo = new Cardinfo();


        $cardinfo->user_id = $user->id;
        $cardinfo->billing_firstname = $request->input('card_firstname');
        $cardinfo->billing_lastname = $request->input('card_lastname');
        $cardinfo->billing_email = $request->input('card_email');
        $cardinfo->billing_address = $request->input('card_address');
        $cardinfo->billing_city = $request->input('card_city');
        $cardinfo->billing_postalcode = $request->input('card_state');
        $cardinfo->billing_country = $request->input('card_postalcode');
        $cardinfo->billing_state = $request->input('card_country');
        $cardinfo->billing_phone = $request->input('card_phone');
        $cardinfo->cardholder = $request->input('card_holder');
        $cardinfo->exp_date = $request->input('card_expdate');

        $cardinfo->cvv = $request->input('card_cvv');
        $cardinfo->card_number = $request->input('card_number');
//        $cardinfo->cart_id = $request->input('dob');


        $cardinfo->cardtrak = 'CRD-' . date("Ymd") . time() . $user->id;

        $cardinfo->save();

        return Redirect::to('/customer-my-account#billingInformation')->with('billinginfosuccess', 'Card saved successfully');
    }

    public function customerEditcardAction($trac)
    {
        $user = \Auth::user();
        $cardinfo = Cardinfo::where('user_id', $user->id)->where('cardtrak', $trac)->first();
        return view('customer.add-card-info-edit', compact('cardinfo'));
    }


    public function customerUpdatecardAction(Request $request, $trac)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'card_firstname' => 'required',
            'card_lastname' => 'required',
            'card_email' => 'required|email',
            'card_address' => 'required',
            'card_city' => 'required',
            'card_state' => 'required',
            'card_postalcode' => 'required',
            'card_country' => 'required',
            'card_phone' => 'required',
            'card_holder' => 'required',
            'card_number' => 'required',
            'card_cvv' => 'required',
            'card_expdate' => 'required',
        ]);


        $cardinfo = Cardinfo::where('user_id', $user->id)->where('cardtrak', $trac)->first();


        $cardinfo->user_id = $user->id;
        $cardinfo->billing_firstname = $request->input('card_firstname');
        $cardinfo->billing_lastname = $request->input('card_lastname');
        $cardinfo->billing_email = $request->input('card_email');
        $cardinfo->billing_address = $request->input('card_address');
        $cardinfo->billing_city = $request->input('card_city');
        $cardinfo->billing_postalcode = $request->input('card_state');
        $cardinfo->billing_country = $request->input('card_postalcode');
        $cardinfo->billing_state = $request->input('card_country');
        $cardinfo->billing_phone = $request->input('card_phone');
        $cardinfo->cardholder = $request->input('card_holder');
        $cardinfo->exp_date = $request->input('card_expdate');

        $cardinfo->cvv = $request->input('card_cvv');
        $cardinfo->card_number = $request->input('card_number');


        $cardinfo->save();

        return Redirect::to('/customer-my-account#billingInformation')->with('billinginfosuccess', 'Card update successfully');
    }


    public function getCreditCardListByUserJWT(Request $request)
    {
        $user = \Auth::user();


        $cardinfo = Cardinfo::where('user_id', $user->id)
            ->orderBy('id', 'ASC')->get();
        if($cardinfo){
            return response()->json($cardinfo,200);
        }else{
            return response()->json($cardinfo,500);
        }

    }
    public function getSingleCreditCardJWT($trac)
    {
        $user = \Auth::user();
        $data = Cardinfo::where('user_id', $user->id)->where('cardtrak', $trac)->first();



        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json('No record found with the specified `id`.', 404);
        }

    }
    public function createSingleCreditCardJWT(Request $request)
    {
        $user = \Auth::user();
        $validator = Validator::make($request->all(), [
            'billing_firstname' => 'required',
            'billing_lastname' => 'required',
            'billing_email' => 'required|email',
            'billing_address' => 'required',
            'billing_city' => 'required',
            'billing_postalcode' => 'required',
            'billing_country' => 'required',
            'billing_state' => 'required',
            'billing_phone' => 'required',
            'cardholder' => 'required',
            'exp_date' => 'required',
            'cvv' => 'required',
            'card_number' => 'required'

        ]);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }
        


        $cardinfo = new Cardinfo();


        $cardinfo->user_id = $user->id;
        $cardinfo->billing_firstname = $request->input('billing_firstname');
        $cardinfo->billing_lastname = $request->input('billing_lastname');
        $cardinfo->billing_email = $request->input('billing_email');
        $cardinfo->billing_address = $request->input('billing_address');
        $cardinfo->billing_city = $request->input('billing_city');
        $cardinfo->billing_postalcode = $request->input('billing_postalcode');
        $cardinfo->billing_country = $request->input('billing_country');
        $cardinfo->billing_state = $request->input('billing_state');
        $cardinfo->billing_phone = $request->input('billing_phone');
        $cardinfo->cardholder = $request->input('cardholder');
        $cardinfo->exp_date = $request->input('exp_date');

        $cardinfo->cvv = $request->input('cvv');
        $cardinfo->card_number = $request->input('card_number');
//        $cardinfo->cart_id = $request->input('dob');


        $cardinfo->cardtrak = 'CRD-' . date("Ymd") . time() . $user->id;

        $cardinfo->save();

        if($cardinfo){
            return response()->json($cardinfo, 201);
        }else{
            return response()->json($cardinfo, 500);
        }
    }
    
    public function updateSingleCreditCardJWT(Request $request,$trac)
    {
        $user = \Auth::user();
        $validator = Validator::make($request->all(), [
            'billing_firstname' => 'required',
            'billing_lastname' => 'required',
            'billing_email' => 'required|email',
            'billing_address' => 'required',
            'billing_city' => 'required',
            'billing_postalcode' => 'required',
            'billing_country' => 'required',
            'billing_state' => 'required',
            'billing_phone' => 'required',
            'cardholder' => 'required',
            'exp_date' => 'required',
            'cvv' => 'required',
            'card_number' => 'required'

        ]);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }



        $cardinfo = Cardinfo::where('user_id', $user->id)->where('cardtrak', $trac)->first();

        if(!$cardinfo){
            return response()->json('No record found with the specified `id`.', 404);
        }

        $cardinfo->user_id = $user->id;
        $cardinfo->billing_firstname = $request->input('billing_firstname');
        $cardinfo->billing_lastname = $request->input('billing_lastname');
        $cardinfo->billing_email = $request->input('billing_email');
        $cardinfo->billing_address = $request->input('billing_address');
        $cardinfo->billing_city = $request->input('billing_city');
        $cardinfo->billing_postalcode = $request->input('billing_postalcode');
        $cardinfo->billing_country = $request->input('billing_country');
        $cardinfo->billing_state = $request->input('billing_state');
        $cardinfo->billing_phone = $request->input('billing_phone');
        $cardinfo->cardholder = $request->input('cardholder');
        $cardinfo->exp_date = $request->input('exp_date');

        $cardinfo->cvv = $request->input('cvv');
        $cardinfo->card_number = $request->input('card_number');


        $cardinfo->save();

        if($cardinfo){
            return response()->json($cardinfo, 200);
        }else{
            return response()->json($cardinfo, 500);
        }
    }
    public function deleteSingleCreditCardJWT($trac)
    {
        $user = \Auth::user();
        $data = Cardinfo::where('user_id', $user->id)->where('cardtrak', $trac)->first();

        $data->delete();

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json('No record found with the specified `id`.', 404);
        }

    }

    public function getShippingInfoActionJWT(Request $request)
    {
        $user = \Auth::user();

        return response()->json($user->customer, 200);
    }
    public function setShippingInfoActionJWT(Request $request)
    {
        $user = \Auth::user();
        $validator = Validator::make($request->all(), [
            'shipping_firstname' => 'required',
            'shipping_email' => 'required|email|unique:customer,shipping_email,' . $user->customer->id,
            'shipping_address' => 'required',
            'shipping_city' => 'required',
            'shipping_state' => 'required',
            'shipping_postalcode' => 'required',
            'shipping_country' => 'required',
            'shipping_phone' => 'required',
        ]);
        if ($validator->fails()) {
            $errorMessage['invalidAttributes'] = $validator->errors();
            return response()->json($errorMessage, 400);
        }

        $user->customer->shipping_firstname = $request->input('shipping_firstname');
        $user->customer->shipping_lastname = $request->input('shipping_lastname');
        $user->customer->shipping_email = $request->input('shipping_email');
        $user->customer->shipping_address = $request->input('shipping_address');
        $user->customer->shipping_city = $request->input('shipping_city');
        $user->customer->shipping_state = $request->input('shipping_state');
        $user->customer->shipping_postalcode = $request->input('shipping_postalcode');
        $user->customer->shipping_country = $request->input('shipping_country');
        $user->customer->shipping_phone = $request->input('shipping_phone');
        $user->customer->save();

        return response()->json($user->customer,200);
    }
   
}
