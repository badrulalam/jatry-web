<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function newAction()
    {
        return view('permission.new');
    }


//    public function create(Request $request)
//    {
//
//        $this->validate($request, [
//            'name' => 'required',
//            'display_name' => 'required',
//            'description' => 'required'
//        ]);
//
////        $role = new \App\Role();
//        $permission = new Permission();
//
//        $permission->name = $request->input('name');
//        $permission->display_name = $request->input('display_name');
//        $permission->description = $request->input('description');
//        $permission->save();
//
//        \Session::flash('flash_message', 'Task successfully added!');
//
//        return redirect()->route('allpermission');
//    }


    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
            'display_name' => 'required',
            'description' => 'required'
        ]);

        $permission = new Permission();
        $permission->name = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description = $request->input('description');
        $permission->save();
        return redirect()->route('allpermission')
            ->with('success','Permission created successfully');
    }

    public function index()
    {
        $permissions = \DB::table('permissions')->get();
        return view('permission.index', ['permissions' => $permissions]);
//        return $users;
    }

    public function indexAction(Request $request)
    {
        $permissions = Permission::orderBy('id','ASC')->paginate(10);
        return view('permission.index',compact('permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


//    public function edit($id)
//    {
//        $permission = Permission::findOrFail($id);
//        return view('permission.edit', ['permission' => $permission]);
////        return $user;
//    }


    public function editAction($id)
    {
        $permission = Permission::findOrFail($id);
        return view('permission.edit',compact('permission'));
    }




    public function update($id, Request $request)
    {
        $permission = Permission::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required'
        ]);

        $input = $request->all();

        $permission->fill($input)->save();

        \Session::flash('flash_message', 'Task successfully added!');

        return redirect()->back();
    }


    public function updateAction(Request $request, $id)
    {
        $this->validate($request, [
            'display_name' => 'required',
            'description' => 'required'

        ]);

        $permission = Permission::find($id);
        $permission->display_name = $request->input('display_name');
        $permission->description = $request->input('description');
        $permission->save();



        return redirect()->route('allpermission')
            ->with('success','Permission updated successfully');
    }



//    public function destroy($id)
//    {
//        $permission = Permission::findOrFail($id);
//
//        $permission->delete();
//
//        \Session::flash('flash_message', 'Task successfully deleted!');
//
//        return redirect()->route('allpermission');
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAction($id)
    {
        DB::table("permissions")->where('id',$id)->delete();
        return redirect()->route('allpermission')
            ->with('success','Permission deleted successfully');
    }
}
