<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Pcategory;
use App\Tpackage;
use App\Jcountry;
use Auth;
use Session;







class PcategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }


    public function indexAction(Request $request)
    {

//        $jcoun = new Jcountry();
//        $jcoun->name = 'Arif iftekhar dss  123 sdsd';
//        $jcoun->save();

//        $jcoun = Jcountry::findOrFail(1);
//        $jcoun->slug = null;
//        $jcoun->name = 'sssArif ift';
//        $jcoun->save();



        $user = \Auth::user();


        if($user->hasRole('superadmin')){
            $data = Pcategory::orderBy('id','ASC')
                ->paginate(10);
            return view('adminarea.pcategory.index',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 10);

        }
        else{
            return redirect()->back();
        }




    }


    public function newAction()
    {
        return view('adminarea.pcategory.new');
    }

    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:pcategory,name',
            'image' => 'required'
        ]);

        $pcategory = new Pcategory();
        $pcategory->name = $request->input('name');
        $pcategory->description = $request->input('description');


        if ($request->file('image')) {
//            \File::delete(storage_path().'/uploads/agent-image/'.$user->agent->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/pcategory-image/', $upimagename);
            $pcategory->image = $upimagename;
        }

        $pcategory->save();
        return redirect()->route('allpcategory')
            ->with('success','Category created successfully');
    }


    public function editAction($id)
    {
        $pcategory = Pcategory::findOrFail($id);


        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.pcategory.edit',compact('pcategory'));
        }

        else{
            return redirect()->back();
        }

    }

    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required|unique:pcategory,name,'.$id
//            'image' => 'required'
        ]);

        $pcategory = Pcategory::findOrFail($id);
        $pcategory->name = $request->input('name');
        $pcategory->description = $request->input('description');
        if ($request->file('image')) {
            \File::delete(storage_path().'/uploads/pcategory-image/'.$pcategory->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/pcategory-image/', $upimagename);
            $pcategory->image = $upimagename;
        }
        $pcategory->save();
        return redirect()->route('allpcategory')
            ->with('success','Category created successfully');
    }

    public function destroyAction($id)
    {

        $pcategory = Pcategory::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){
            \File::delete(storage_path().'/uploads/pcategory-image/'.$pcategory->image);
            $pcategory->delete();
            return redirect()->route('allpcategory')
                ->with('success','Category deleted successfully');
        }

        else{
            return redirect()->back();
        }






    }
}
