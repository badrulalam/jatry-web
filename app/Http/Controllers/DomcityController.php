<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Role;

use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Domcity;



class DomcityController extends Controller
{


    public function cityListAction(Request $request)
    {
        $city = Domcity::orderBy('id','ASC')->get(['id', 'name']);
        return $city;
    }


}
