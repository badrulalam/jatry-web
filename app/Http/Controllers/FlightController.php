<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Intbookairroute;
use App\Intbookairroutesegment;

use Illuminate\Http\Response;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use App\Sale;

use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Intbook;
use App\Cardinfo;
use App\Cartitemair;
use App\Intbookitemair;
use App\Intbookitemairpassenger;


class FlightController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function BookingStatusAction(Request $request)
    {

//        $cart =  Auth::user()->cart;

        $intBookActive = Intbook::where('user_id', Auth::user()->id)->where('is_cancel', false)->get();
        $intBookCancle = Intbook::where('user_id', Auth::user()->id)->where('is_cancel', true)->get();

//        return view('frontend.checkoutfinal', compact('cart'));
        return view('flight.booking-statas', compact('intBookActive', 'intBookCancle'));
    }


    public function FlightStatusAction(Request $request)
    {

        return view('flight.flight-statas');
    }

    public function MyTicketsAction(Request $request)
    {
        $user = \Auth::user();
        $sele = Sale::where('user_id', $user->id)->orderBy('id', 'ASC')->get();
        return view('flight.my-tickets', ['sale' => $sele]);
    }

    public function getCurrentBooingListActionJWT(Request $request)
    {
        $intBookActive = Intbook::where('user_id', Auth::user()->id)->where('is_cancel', false)->get();
        return response()->json($intBookActive, 200);
    }

    public function getCanceledBooingListActionJWT(Request $request)
    {
        $intBookCancle = Intbook::where('user_id', Auth::user()->id)->where('is_cancel', true)->get();
        return response()->json($intBookCancle, 200);
    }


    public function viewCurrentBooingSingleActionJWT($invoice){
        $sale = Intbook::where('user_id', Auth::user()->id)->where('order_number', $invoice)->with('ticket')->with('ticket.route')->with('ticket.passenger')->with('ticket.route.segment')->first();
        if(!$sale){
            return response()->json('No record found with the specified `invoice number`.', 404);
        }


        if($sale){
            return response()->json($sale, 200);
        }else{
            return response()->json('No record found with the specified `invoice number`.', 500);
        }
    }
    public function viewCanceledBooingSingleActionJWT($invoice){
        $sale = Intbook::where('user_id', Auth::user()->id)->where('order_number', $invoice)->with('ticket')->with('ticket.route')->with('ticket.passenger')->with('ticket.route.segment')->first();
        if(!$sale){
            return response()->json('No record found with the specified `invoice number`.', 404);
        }


        if($sale){
            return response()->json($sale, 200);
        }else{
            return response()->json('No record found with the specified `invoice number`.', 500);
        }
    }


    public function cancelCurrentBooingSingleActionJWT($invoice){
        $sale = Intbook::where('user_id', Auth::user()->id)->where('order_number', $invoice)->first();

        if(!$sale){
            return response()->json('No record found with the specified `invoice number`.', 404);
        }


        $sale->is_cancel = true;
        $sale->save();

        if($sale){
            return response()->json('Booking canceled', 200);
        }else{
            return response()->json('No record found with the specified `invoice number`.', 500);
        }
    }






}
