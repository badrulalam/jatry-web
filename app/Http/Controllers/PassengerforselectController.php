<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Response;
use App\Passengerforselect;
use App\Cardinfo;

class PassengerforselectController extends Controller
{
    public function getPassengerListByUser(){
        if(Auth::guest()){
            return 'not-login';
        }elseif(Auth::user()){

            return Passengerforselect::where('user_id', Auth::user()->id)->get();

        }

    }



    public function getCardListByUser(){
        if(Auth::guest()){
            return 'not-login';
        }elseif(Auth::user()){

            return Cardinfo::where('user_id', Auth::user()->id)->get();

        }

    }




}
