<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cartitempkg()
    {
        return $this->hasMany('App\Cartitempkg');
    }

    public function cartitemair()
    {
        return $this->hasOne('App\Cartitemair');
    }
    public function ticket()
    {
        return $this->hasOne('App\Cartitemair');
    }

//    public function cartitemairpassenger()
//    {
//        return $this->hasMany('App\Cartitemairpassenger');
//    }


    public function cartairpassengeritem()
    {
        return $this->hasMany('App\Cartairpassengeritem');
    }
}
