<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sale';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function saleitemair()
    {
        return $this->hasOne('App\Saleitemair');
    }

    public function ticket()
    {
        return $this->hasOne('App\Saleitemair');
    }



    public function cardinfo()
    {
        return $this->belongsTo('App\Cardinfo');
    }



}
