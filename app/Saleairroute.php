<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saleairroute extends Model
{

    protected $table = 'saleairroute';

    public function saleitemair()
    {
        return $this->belongsTo('App\Saleitemair');
    }

    public function segment()
    {
        return $this->hasMany('App\Saleairroutesegment');
    }

}
