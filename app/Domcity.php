<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domcity extends Model
{
    protected $table = 'domcity';

    public function orgin()
    {
        return $this->hasMany('App\Domflight', 'orgincity_id');
    }

    public function destination()
    {
        return $this->hasMany('App\Domflight', 'destinationcity_id');
    }

}
