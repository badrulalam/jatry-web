<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartitempkg extends Model
{
    protected $table = 'cartitempkg';


    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function tpackage()
    {
        return $this->belongsTo('App\Tpackage');
    }
}
