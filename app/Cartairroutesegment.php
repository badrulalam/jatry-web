<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartairroutesegment extends Model
{
    protected $table = 'cartairroutesegment';

    public function cartairroute()
    {
        return $this->belongsTo('App\Cartairroute');
    }

    public function cartairseatmap()
    {
        return $this->hasMany('App\Cartairseatmap');
    }



}
