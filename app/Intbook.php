<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intbook extends Model
{
    protected $table = 'intbook';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cardinfo()
    {
        return $this->belongsTo('App\Cardinfo');
    }

    public function ticket()
    {
        return $this->hasOne('App\Intbookitemair');
    }
}
