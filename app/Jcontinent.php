<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Jcontinent extends Model
{
    use Sluggable;

    protected $table = 'jcontinent';


    public function jcountry()
    {
        return $this->hasMany('App\Jcountry');
    }

    public function tpackage()
    {
        return $this->hasMany('App\Tpackage');
    }


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
