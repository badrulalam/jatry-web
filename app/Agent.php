<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agent';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }

    public function customer()
    {
        return $this->hasMany('App\Customer');
    }



}
