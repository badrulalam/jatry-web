var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    mix.styles([
        './vendor/bower_components/angular/angular-csp.css',
        './vendor/bower_components/angular-bootstrap/ui-bootstrap-csp.css',
        './vendor/bower_components/angular-loading-bar/build/loading-bar.min.css',
        // './vendor/bower_components/angular-material/angular-material.min.css',
        './vendor/bower_components/bootstrap/dist/css/bootstrap.min.css',
        './vendor/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        './vendor/bower_components/owl.carousel/dist/assets/owl.theme.default.min.css',
        './vendor/bower_components/owl.carousel/dist/assets/owl.theme.green.min.css',
        './vendor/bower_components/angularjs-slider/dist/rzslider.min.css',
        './vendor/bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
        './vendor/bower_components/components-font-awesome/css/font-awesome.min.css',
        './vendor/bower_components/flag-icon-css/css/flag-icon.min.css',
        './vendor/bower_components/animate.css/animate.min.css',
        './vendor/bower_components/hover/css/hover-min.css'
    ], './public/css/vendor.css', './public/css');
    
    mix.styles([
        './resources/assets/css/*.css',
        './resources/assets/css/**/*.css',
    ], './public/css/resources.css', './public/css');

    mix.scripts([
        './resources/assets/js/polyfill.js',
        './vendor/bower_components/jquery/dist/jquery.min.js',
        './vendor/bower_components/angular/angular.min.js',
        './vendor/bower_components/angular-aria/angular-aria.min.js',
        './vendor/bower_components/angular-animate/angular-animate.min.js',
        // './vendor/bower_components/angular-material/angular-material.min.js',
        './vendor/bower_components/angular-bootstrap/ui-bootstrap.min.js',
        './vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/bower_components/angular-messages/angular-messages.min.js',
        './vendor/bower_components/angular-sanitize/angular-sanitize.min.js',
        './vendor/bower_components/angular-translate/angular-translate.min.js',
        './vendor/bower_components/angular-filter/dist/angular-filter.min.js',
        './vendor/bower_components/angular-loading-bar/build/loading-bar.min.js',
        './vendor/bower_components/jquery/dist/jquery.slim.min.js',
        './vendor/bower_components/bootstrap/dist/js/bootstrap.min.js',
        './vendor/bower_components/angularjs-slider/dist/rzslider.min.js',
        './vendor/bower_components/owl.carousel/dist/owl.carousel.min.js',
        // './vendor/bower_components/angular/angular.js',

    ], './public/js/vendor.js', './public/js');
    
    mix.scripts([
        './resources/assets/js/*.js',
        './resources/assets/js/**/*.js'

    ], './public/js/resources.js', './public/js');

    mix.copy('./vendor/bower_components/bootstrap/fonts/','public/fonts/');
    mix.copy('./vendor/bower_components/components-font-awesome/fonts','public/fonts/');
    mix.copy('./resources/assets/css/font','public/fonts/');
    mix.copy('./resources/assets/template','public/template/');
    mix.copy('./vendor/bower_components/flag-icon-css/flags','public/flags/');

});
