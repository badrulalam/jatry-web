<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{--<link rel="stylesheet" type="text/css" href="css/bootstrap-pdf.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-pdf.css">--}}
    <link rel="stylesheet" type="text/css" href="css/pdf.css">

    <title>Example 2</title>

</head>


<body>

<div class="container1">
    <div class="row">
        <div class="col-md-12">




                        <div style="font-size: 24px;font-weight: bold;text-align: center;padding: 5px">Invoice : {{$sale->order_number}}</div>
            <?php $airticPai = 0; ?>

                        @foreach ($sale->saleitemair as $sitem)
                            <table class="table table-bordered">
                                <tr>
                                    <th class="col-sm-3">
                                        Airlines
                                    </th>
                                    <th class="col-sm-2">
                                        Departure
                                    </th>
                                    <th class="col-sm-2">
                                        Arrive
                                    </th>
                                    <th class="col-sm-2">
                                        Duration
                                    </th>
                                    <th class="col-sm-2">
                                        Price
                                    </th>

                                </tr>


                                <tr>
                                    <td class="col-sm-3">
                                        <span><img src="{{public_path().'/api.travelport/images/'.$sitem->airline.'.png'}}" alt="Image" style="height: 40px;"></span>
                                        {{$sitem->airline}}
                                    </td>
                                    <td class="col-sm-2">{{$sitem->departure_time}} <br>
                                        {{$sitem->orgine}}</td>
                                    <td class="col-sm-2">{{$sitem->arrival_time}} <br>
                                        {{$sitem->destination}}</td>
                                    <td class="col-sm-2">
                                        Duration
                                    </td>
                                    <td class="col-sm-2">
                                        {{$sitem->price}}
{{--                                        {{$airticPai += $sitem->price}}--}}
                                        <?php $airticPai += $sitem->price; ?>
                                    </td>
                                </tr>

                            </table>
                            <div class="table-responsive">
                                Traveller List
                                <table class="table table-bordered">
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>DOB</th>
                                        <th>Nationality</th>
                                        {{--<th>Passport No.</th>--}}
                                        <th>Issuing Country</th>
                                        {{--<th>Passport Expiry Date</th>--}}

                                    </tr>
                                    @foreach ($sitem->passenger as $person)
                                        <tr>
                                            <td>{{$person->passenger_type.' '.$person->passenger_order}}</td>
                                            <td>{{$person->title}}</td>
                                            <td>{{$person->first_name}}</td>
                                            <td>{{$person->middle_name}}</td>
                                            <td>{{$person->last_name}}</td>
                                            <td>{{$person->dob}}</td>
                                            <td>{{$person->nationality}}</td>
{{--                                            <td>{{$person->passport_no}}</td>--}}
                                            <td>{{$person->passport_issue_country}}</td>
{{--                                            <td>{{$person->passport_expiry_date}}</td>--}}

                                        </tr>
                                    @endforeach

                                </table>

                                <div style="font-size: 18px;font-weight: bold;color: red;">Total Price : {{$airticPai}}</div>

                            </div>

                        @endforeach




        </div>
    </div>
</div>
</body>
