@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">


            <div class="container">
                <div class="panel panel-default" >
                    <div class="panel-body">

                        {{--<div style="font-size: 24px;font-weight: bold;text-align: center;">Checkout Successful</div>--}}
                        <div style="font-size: 24px;font-weight: bold;text-align: center;padding: 5px">Invoice : {{$sale->order_number}}</div>

                        <?php $airticPai = 0; ?>


                        @foreach ($sale->saleitemair as $sitem)
                        <table class="table table-bordered">
                            <tr>
                                <th class="col-sm-3">
                                    Airlines
                                </th>
                                <th class="col-sm-2">
                                    Departure
                                </th>
                                <th class="col-sm-2">
                                    Arrive
                                </th>
                                <th class="col-sm-2">
                                    Duration
                                </th>
                                <th class="col-sm-2">
                                    Price
                                </th>

                            </tr>


                            <tr>
                                <td class="col-sm-3">
                                    <span><img src="{{'/api.travelport/images/'.$sitem->airline.'.png'}}" alt="not found" style="height: 40px;"></span>
                                     {{$sitem->airline}}
                                </td>
                                <td class="col-sm-2">{{$sitem->departure_time}} <br>
                                    {{$sitem->orgine}}</td>
                                <td class="col-sm-2">{{$sitem->arrival_time}} <br>
                                    {{$sitem->destination}}</td>
                                <td class="col-sm-2">
                                    Duration
                                </td>
                                <td class="col-sm-2">
                                    {{$sitem->price}}
                                    <?php $airticPai += $sitem->price; ?>
                                </td>
                            </tr>

                        </table>
                            <div class="table-responsive">
                                Traveller List
                                <table class="table table-bordered">
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>DOB</th>
                                        <th>Nationality</th>
                                        <th>Passport No.</th>
                                        <th>Issuing Country</th>
                                        <th>Passport Expiry Date</th>

                                    </tr>
                                    @foreach ($sitem->passenger as $person)
                                    <tr>
                                        <th>{{$person->passenger_type.' '.$person->passenger_order}}</th>
                                        <th>{{$person->title}}</th>
                                        <th>{{$person->first_name}}</th>
                                        <th>{{$person->middle_name}}</th>
                                        <th>{{$person->last_name}}</th>
                                        <th>{{$person->dob}}</th>
                                        <th>{{$person->nationality}}</th>
                                        <th>{{$person->passport_no}}</th>
                                        <th>{{$person->passport_issue_country}}</th>
                                        <th>{{$person->passport_expiry_date}}</th>

                                    </tr>
                                    @endforeach

                                </table>
                                <div style="font-size: 18px;font-weight: bold;color: red;">Total Price : {{$airticPai}}</div>
                            </div>

                        @endforeach


                    </div>
                    <!--<div class="panel-footer">Panel footer</div>-->
                </div>
            </div>



            <a href="{{ route('salepdf',$sale->order_number) }}" class="btn btn-primary">Pdf</a>

        </div>
    </div>

@endsection
