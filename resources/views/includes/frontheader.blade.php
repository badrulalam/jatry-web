<nav class="navbar navbar-inverse navbar-static-top" id="main_navbar1" style="background-color: #EEEEEE !important;background-image: none !important;">
{{--<nav class="navbar navbar-default navbar-static-top" >--}}
    <div class="container-fluid">

            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{Route('home')}}">
                    <span><img src="/img/logo-with-text.svg" alt="JATRY" style="height: 25px"></span>
                </a>
            </div>
            {{--<!-- Left Side Of Navbar -->--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li><a href="{{ Route('home') }}">Home</a></li>--}}
            {{--</ul>--}}

            <!-- Right Side Of Navbar -->
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li style="display: none;"><a href="{{ Route('country-tour') }}">Tour</a></li>
                <li style="display: none;"><a href="{{ Route('all-country-tour') }}">Country</a></li>

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li style="display: none;"><a href="{{ Route('frontagentlogin') }}">Agent Login</a></li>
                    <li style="display: none;"><a href="{{ Route('agentregister') }}">Agent Register</a></li>
                    <li><a href="{{ Route('customerregister') }}">REGISTER</a></li>
                    <li><a href="{{ Route('customerlogin') }}">LOGIN</a></li>
                    <li><a href="{{ Route('customerlogin') }}">CONTACT US</a></li>
                    <li><a href="{{ Route('customerlogin') }}">HELP</a></li>
                    {{--<li><a href="{{ Route('customerregister') }}">{{dd(MyFuncs::Hostname())}}</a></li>--}}
                    {{--                    <li><a href="{{ Route('customerregister') }}">{{MyFuncs::Hostname()}}</a></li>--}}
                    {{--<li><a href="{{ Route('country-tour') }}">Tour</a></li>--}}
                    {{--                    <li><a href="{{ Route('customerregister') }}">{{dd(gettype(MyFuncs::getAgency()))}}</a></li>--}}
                    {{--<li><a href="{{ Route('customerregister') }}">{{MyFuncs::getAgency()? MyFuncs::getAgency()->user->id : 'No'}}</a></li>--}}
                @else

                    @role('customer')
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Flights<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ Route('booking-status') }}"><img src="/img/icon/Booking-13.png" alt="JATRY" >Booking Status</a></li>
                            <li><a href="{{ Route('flight-status') }}"><img src="/img/icon/Check-flight-status-14.png" alt="JATRY" >Flight Status</a></li>
                            <li><a href="{{ Route('my-tickets') }}"><img src="/img/icon/My-Tickts-15.png" alt="JATRY" >My Tickets</a></li>

                        </ul>
                    </li>
                    @endrole


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            My account<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @role('agent')
                            <li><a href="{{ Route('customermyaccount').'#/billingAddress' }}">Ag Profile</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Travelers</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Change password</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Payment info</a></li>

                            @endrole
                            @role('customer')
                            <li><a href="{{ Route('customermyaccount').'#/billingAddress' }}"><img src="/img/icon/Profile-17.png" alt="JATRY" >Profile</a></li>
                            <li><a href="{{ Route('customermyaccount') }}"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >Travelers</a></li>

                            {{--<li><a href="{{ Route('customermyaccount') }}">Payment info</a></li>--}}
                            <li><a href="{{ Route('customermyaccount') }}"><img src="/img/icon/Credit-17.png" alt="JATRY" >Credit Card</a></li>
                            @endrole

                        </ul>
                    </li>






                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }}<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ Route('userpasschange') }}"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >Change password</a></li>
                            {{--<li><a href="{{ Route('userpasschange') }}"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >Change password</a></li>--}}
                            <li><a href="{{ url('/logout') }}"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >Logout</a></li>
                            {{--<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>--}}
                        </ul>
                    </li>
                @endif
            </ul>

        </div>
        </div>

</nav>

