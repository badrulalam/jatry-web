<header id="header" class="">

    <section class="logo-heading">
        <div class="container-fluid">
            <div class="row">
                <div class="header-wrapper">
                    <div class="logo">
                        <img src="{{ URL::to('img/travelport-redefining-logo.png') }}" alt="">
                    </div>
                    <div class="right-heading-text">
                        <h1><SPAN>Travelport</SPAN> Universal API</h1>
                        <p><span>Demonstration</span> release 15.5 v35.0</p>
                        <p class="text-center bottom-parag">100% powered by Travelport universal API</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#">logo</a> -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#" class="for-border">Dashboard<span class="sr-only">(current)</span></a></li>

                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">User<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newuser')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allusers')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>

                    @endif








                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Role<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newrole')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allroles')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Permission<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newpermission')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allpermission')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Agency<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newpermission')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allagencylist')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole(['superadmin', 'superagent']))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Agent<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                {{--<li><a href="{{Route('newpermission')}}">New</a></li>--}}
                                <li><a href="{{Route('agencyaddagent')}}">New (For Agency)</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allagencylist')}}">Index</a></li>
                                <li><a href="{{Route('agencyagentlist')}}">Index(For Agency)</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Customer<span class="caret"></span></a>
                            <ul class="dropdown-menu">

                                <li><a href="{{Route('allcustomerlist')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole(['superadmin', 'superagent']))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Banner<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newbannerimage')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allbannerimage')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Package<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newpcategory')}}">Category New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allpcategory')}}">Category Index</a></li>
                                <li role="separator" class="divider"></li>

                                <li><a href="{{Route('newjcontinent')}}">Continent New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('alljcontinent')}}">Continent Index</a></li>
                                <li role="separator" class="divider"></li>

                                <li><a href="{{Route('newjcountry')}}">Country New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('alljcountry')}}">Country Index</a></li>
                                <li role="separator" class="divider"></li>

                                <li><a href="{{Route('newtpackage')}}">Package New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allpcategory')}}">Package Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif


                    @if (Auth::user()->hasRole('superadmin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Sale<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{Route('newpermission')}}">New</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allpermission')}}">Index</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                    @endif


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Logs<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>


                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

            </div><!-- /.navbar-collapse -->
        </nav>
    </div><!-- /.container-fluid -->
</header><!-- /header End-->