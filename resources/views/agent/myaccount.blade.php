@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Agent Account</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('agentmyaccountsave') }}">
                            {{ csrf_field() }}


                            <fieldset>
                                <legend>Login Information</legend>

                            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                                <label for="user_name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $user->name }}">

                                    @if ($errors->has('user_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="user_email" type="email" class="form-control" name="user_email" value="{{ $user->email }}">

                                    @if ($errors->has('user_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                    <div class="col-md-6">
                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            </fieldset>


                            @role('superagent')
                            <fieldset>
                                <legend>Agency Information</legend>

                                <div class="form-group{{ $errors->has('agency_code') ? ' has-error' : '' }}">
                                    <label for="agency_code" class="col-md-4 control-label">Code</label>
                                    <div class="col-md-6">
                                        <input id="agency_code" type="text" class="form-control" name="agency_code" value="{{ $user->agency->code }}">

                                        @if ($errors->has('agency_code'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_code') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('agency_name') ? ' has-error' : '' }}">
                                    <label for="agency_name" class="col-md-4 control-label">Name</label>
                                    <div class="col-md-6">
                                        <input id="agency_name" type="text" class="form-control" name="agency_name" value="{{ $user->agency->name }}">

                                        @if ($errors->has('agency_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agency_email') ? ' has-error' : '' }}">
                                    <label for="agency_email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="agency_email" type="email" class="form-control" name="agency_email" value="{{ $user->agency->email }}">

                                        @if ($errors->has('agency_email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('agency_address') ? ' has-error' : '' }}">
                                    <label for="agency_address" class="col-md-4 control-label">address</label>
                                    <div class="col-md-6">
                                        <textarea id="address" type="text" class="form-control" name="agency_address" >{{ $user->agency->address}}</textarea>
                                        @if ($errors->has('agency_address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_city') ? ' has-error' : '' }}">
                                    <label for="agency_city" class="col-md-4 control-label">city</label>
                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control" name="agency_city" value="{{ $user->agency->city}}">
                                        @if ($errors->has('agency_city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_city') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_state') ? ' has-error' : '' }}">
                                    <label for="agency_state" class="col-md-4 control-label">state</label>
                                    <div class="col-md-6">
                                        <input id="agency_state" type="text" class="form-control" name="agency_state" value="{{ $user->agency->state}}">
                                        @if ($errors->has('agency_state'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_state') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_postalcode') ? ' has-error' : '' }}">
                                    <label for="agency_postalcode" class="col-md-4 control-label">postalcode</label>
                                    <div class="col-md-6">
                                        <input id="agency_postalcode" type="text" class="form-control" name="agency_postalcode" value="{{ $user->agency->postalcode}}">
                                        @if ($errors->has('agency_postalcode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_postalcode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_country') ? ' has-error' : '' }}">
                                    <label for="agency_country" class="col-md-4 control-label">country</label>
                                    <div class="col-md-6">
                                        {!! Form::select('agency_country', Countries::getList('en', 'php', 'icu'), null, array('class' => 'form-control')) !!}
                                        @if ($errors->has('agency_country'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_country') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_phone') ? ' has-error' : '' }}">
                                    <label for="agency_phone" class="col-md-4 control-label">phone</label>
                                    <div class="col-md-6">
                                        <input id="agency_phone" type="text" class="form-control" name="agency_phone" value="{{ $user->agency->phone}}">
                                        @if ($errors->has('agency_phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agency_image') ? ' has-error' : '' }}">
                                    <label for="agency_image" class="col-md-4 control-label">image</label>
                                    <div class="col-md-4">
                                        <input id="agency_image" type="file" class="form-control1" name="agency_image" value="{{ $user->agency->image}}">

                                    @if ($errors->has('agency_image'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_image') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                        @if ($user->agency->image)
                                            {{ Html::image('/api/image/agency-image/'.$user->agency->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group{{ $errors->has('agency_logo') ? ' has-error' : '' }}">
                                    <label for="agency_logo" class="col-md-4 control-label">logo</label>
                                    <div class="col-md-4">
                                        <input id="agency_logo" type="file" class="form-control1" name="agency_logo" value="{{ $user->agency->logo}}">
                                        @if ($errors->has('agency_logo'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_logo') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                        @if ($user->agency->logo)
                                            {{ Html::image('/api/image/agency-logo/'.$user->agency->logo, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agency_url') ? ' has-error' : '' }}">
                                    <label for="agency_url" class="col-md-4 control-label">Agency URL</label>
                                    <div class="col-md-6">
                                        <input id="agency_url" type="text" class="form-control" name="agency_url" value="{{ $user->agency->url }}">
                                        @if ($errors->has('agency_url'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_url') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                            </fieldset>
                            @endrole


                            <fieldset>
                                <legend>Agent Information</legend>
                                <div class="form-group{{ $errors->has('agent_code') ? ' has-error' : '' }}">
                                    <label for="code" class="col-md-4 control-label">Code</label>
                                    <div class="col-md-6">
                                        <input id="code" type="text"  class="form-control" name="agent[code]" value="{{ $user->agent->code }}">
                                        @if ($errors->has('code'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name" class="col-md-4 control-label">First Name</label>
                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->agent->first_name }}">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>



                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name" class="col-md-4 control-label">Last Name</label>
                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->agent->last_name }}">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agent_email') ? ' has-error' : '' }}">
                                    <label for="agent_email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="agent_email" type="email" class="form-control" name="agent_email" value="{{ $user->agent->email }}">

                                        @if ($errors->has('agent_email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agent_address') ? ' has-error' : '' }}">
                                    <label for="agent_address" class="col-md-4 control-label">Address</label>
                                    <div class="col-md-6">
                                        <textarea id="agent_address" type="text" class="form-control" name="agent_address" >{{ $user->agent->address }}</textarea>
                                        @if ($errors->has('agent_address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agent_city') ? ' has-error' : '' }}">
                                    <label for="agent_city" class="col-md-4 control-label">City</label>
                                    <div class="col-md-6">
                                        <input id="agent_city" type="text" class="form-control" name="agent_city" value="{{ $user->agent->city }}">
                                        @if ($errors->has('agent_city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_city') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agent_state') ? ' has-error' : '' }}">
                                    <label for="agent_state" class="col-md-4 control-label">State</label>
                                    <div class="col-md-6">
                                        <input id="state" type="text" class="form-control" name="agent_state" value="{{ $user->agent->state }}">
                                        @if ($errors->has('agent_state'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_state') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agent_postalcode') ? ' has-error' : '' }}">
                                    <label for="agent_postalcode" class="col-md-4 control-label">Postal Code</label>
                                    <div class="col-md-6">
                                        <input id="agent_postalcode" type="text" class="form-control" name="agent_postalcode" value="{{ $user->agent->postalcode }}">
                                        @if ($errors->has('agent_postalcode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_postalcode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agent_country') ? ' has-error' : '' }}">
                                    <label for="agent_country" class="col-md-4 control-label">Country</label>
                                    <div class="col-md-6">
                                        {!! Form::select('agent_country', Countries::getList('en', 'php', 'icu'), null, array('class' => 'form-control')) !!}

                                        @if ($errors->has('agent_country'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_country') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agent_phone') ? ' has-error' : '' }}">
                                    <label for="agent_phone" class="col-md-4 control-label">Phone</label>
                                    <div class="col-md-6">
                                        <input id="agent_phone" type="text" class="form-control" name="agent_phone" value="{{ $user->agent->phone }}">
                                        @if ($errors->has('agent_phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('agent_image') ? ' has-error' : '' }}">
                                    <label for="agent_image" class="col-md-4 control-label">Image</label>
                                    <div class="col-md-4">
                                        <input id="agent_image" type="file" class="form-control1" name="agent_image" value="{{ $user->agent->image }}">
                                        @if ($errors->has('agent_image'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_image') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                        @if ($user->agent->image)
                                           {{ Html::image('/api/image/agent-image/'.$user->agent->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                        @endif
                                    </div>

                                </div>
                                {{--<div class="form-group{{ $errors->has('agent_logo') ? ' has-error' : '' }}">--}}
                                    {{--<label for="agent_logo" class="col-md-4 control-label">Logo</label>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<input id="agent_logo" type="file" class="form-control" name="agent_logo" value="{{ $user->agent->logo }}">--}}
                                        {{--@if ($errors->has('agent_logo'))--}}
                                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('agent_logo') }}</strong>--}}
                                    {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group{{ $errors->has('agent_url') ? ' has-error' : '' }}">--}}
                                    {{--<label for="agent_url" class="col-md-4 control-label">Agent url</label>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<input id="agent_url" type="text" class="form-control" name="agent_url" value="{{ $user->agent->url }}">--}}
                                        {{--@if ($errors->has('agent_url'))--}}
                                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('agent_url') }}</strong>--}}
                                    {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                            </fieldset>
                            <br/>
                            <fieldset>
                                {{--<legend></legend>--}}
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>

                    <ul>
                        <li>Login Info</li>
                        <li>Agency Info</li>
                        <li>Agent Info</li>
                    </ul>



                </div>
            </div>
        </div>
    </div>

@endsection
