@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newjcountry') }}"> Create New Country</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Continent</th>
                        <th width="280px" style="vertical-align: middle;">Action</th>
                    </tr>
                    @foreach ($data as $key => $jcoun)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $jcoun->name }}</td>
                            <td>
                                @if ($jcoun->image)
                                    {{ Html::image('/api/image/jcountry-image/'.$jcoun->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}
                                @endif
                            </td>
                            <td>{{ $jcoun->jcontinent->name }}</td>
                            <td>

                                <a class="btn btn-primary" href="{{ route('jcountryedit',$jcoun->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['jcountrydelete', $jcoun->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
