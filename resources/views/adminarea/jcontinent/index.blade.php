@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newjcontinent') }}"> Create New Continent</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th width="280px" style="vertical-align: middle;">Action</th>
                    </tr>
                    @foreach ($data as $key => $jconti)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $jconti->name }}</td>
                            <td>
                                @if ($jconti->image)
                                    {{ Html::image('/api/image/jcontinent-image/'.$jconti->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}
                                @endif
                            </td>
                            <td>

                                <a class="btn btn-primary" href="{{ route('jcontinentedit',$jconti->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['jcontinentdelete', $jconti->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
