@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    {{--<a class="btn btn-success" href="{{ route('allusers') }}"> Back </a>--}}
                    <a class="btn btn-primary" href="{{ route('alltpackage') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new Package</div>
                    <div class="panel-body">

                        {!! Form::open(array('route' => 'createtpackage','method'=>'POST','enctype'=>'multipart/form-data', 'class' => 'form-horizontal')) !!}
                        <fieldset>
                            <legend>Package</legend>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image1') ? ' has-error' : '' }}">
                            <label for="image1" class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                {{ Form::file('image1', ['class' => 'form-control1 imageimp', 'id' => 'image1']) }}
                                @if ($errors->has('image1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            <div class="form-group{{ $errors->has('image2') ? ' has-error' : '' }}">
                                <label for="image2" class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                    {{ Form::file('image2', ['class' => 'form-control1 imageimp', 'id' => 'image2']) }}
                                    @if ($errors->has('image2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image3') ? ' has-error' : '' }}">
                                <label for="image3" class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                    {{ Form::file('image3', ['class' => 'form-control1 imageimp', 'id' => 'image3']) }}
                                    @if ($errors->has('image3'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image4') ? ' has-error' : '' }}">
                                <label for="image4" class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                    {{ Form::file('image4', ['class' => 'form-control1 imageimp', 'id' => 'image4']) }}
                                    @if ($errors->has('image4'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image4') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('price', null, array('placeholder' => 'Price','class' => 'form-control')) !!}
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('isactive') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                       <label>{{ Form::checkbox('isactive', 1 ,  true, array('class' => 'name')) }}
                                        Active</label>
                                    @if ($errors->has('isactive'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('isactive') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </fieldset>


                        <fieldset>
                            <legend>Air / Transport</legend>

                            <div class="form-group{{ $errors->has('airfare_include') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">&nbsp;</label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('airfare_include', 1 ,  false, array('class' => 'name')) }}
                                        Airfare Include</label>
                                    @if ($errors->has('airfare_include'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('airfare_include') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                </div>

                            <div class="form-group{{ $errors->has('airfare_exclude') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('airfare_exclude', 1 ,  false, array('class' => 'name')) }}
                                        Airfare Exclude</label>
                                    @if ($errors->has('airfare_exclude'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('airfare_exclude') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('transport_include') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('transport_include', 1 ,  false, array('class' => 'name')) }}
                                        Transport Include</label>
                                    @if ($errors->has('transport_include'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('transport_include') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('transport_exclude') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('transport_exclude', 1 ,  false, array('class' => 'name')) }}
                                        Transport Exclude</label>
                                    @if ($errors->has('transport_exclude'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('transport_exclude') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </fieldset>


                        <fieldset>
                            <legend>Hotel</legend>

                            <div class="form-group{{ $errors->has('property_type') ? ' has-error' : '' }}">
                                <label for="property_type" class="col-md-4 control-label">Property Type</label>
                                <div class="col-md-6">
{{--                                    {!! Form::select('property_type[]', $property_type,[], array('class' => 'form-control')) !!}--}}
                                    {!! Form::select('property_type', [
                                          null => '---',
                                           'hotel' => 'Hotel',
                                           'resorts' => 'Resorts',
                                           'cruiseship' => 'Cruiseship'
                                      ],null, array('class' => 'form-control', 'id'=>'property_type')) !!}
                                    @if ($errors->has('property_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('property_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('hotel_rating') ? ' has-error' : '' }}">
                                <label for="hotel_rating" class="col-md-4 control-label">Hotel Rating</label>
                                <div class="col-md-6">
                                    {!! Form::select('hotel_rating', [
                                          null => '---',
                                           1 => 1,
                                           2 => 2,
                                           3 => 3,
                                           4 => 4,
                                           5 => 5
                                      ],null, array('class' => 'form-control', 'id'=>'hotel_rating')) !!}
                                    @if ($errors->has('hotel_rating'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('hotel_rating') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('check_in') ? ' has-error' : '' }}">
                                <label for="check_in" class="col-md-4 control-label">Check In</label>
                                <div class="col-md-4">
                                    {!! Form::text('check_in', null, array('placeholder' => 'Check In','class' => 'form-control')) !!}
                                    @if ($errors->has('check_in'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('check_in') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('check_out') ? ' has-error' : '' }}">
                                <label for="check_out" class="col-md-4 control-label">Check Out</label>
                                <div class="col-md-4">
                                    {!! Form::text('check_out', null, array('placeholder' => 'Check Out','class' => 'form-control')) !!}
                                    @if ($errors->has('check_out'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('check_out') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pets') ? ' has-error' : '' }}">
                                <label for="pets" class="col-md-4 control-label">Pets</label>
                                <div class="col-md-6">
                                    {!! Form::select('pets', [
                                          null => '---',
                                           'allow' => 'Allow',
                                           'notallow' => 'Not Allow'
                                      ],null, array('class' => 'form-control', 'id'=>'pets')) !!}
                                    @if ($errors->has('pets'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pets') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('laundry_service') ? ' has-error' : '' }}">
                                <label for="laundry_service" class="col-md-4 control-label">Laundry Service</label>
                                <div class="col-md-6">
                                    {!! Form::select('laundry_service', [
                                          null => '---',
                                           'yes' => 'Yes',
                                           'no' => 'No'
                                      ],null, array('class' => 'form-control', 'id'=>'laundry_service')) !!}
                                    @if ($errors->has('laundry_service'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('laundry_service') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('free_wifi') ? ' has-error' : '' }}">
                                <label for="free_wifi" class="col-md-4 control-label">Free WiFi</label>
                                <div class="col-md-6">
                                    {!! Form::select('free_wifi', [
                                          null => '---',
                                           'yes' => 'Yes',
                                           'no' => 'No'
                                      ],null, array('class' => 'form-control', 'id'=>'free_wifi')) !!}
                                    @if ($errors->has('free_wifi'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('free_wifi') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('smoking_area') ? ' has-error' : '' }}">
                                <label for="smoking_area" class="col-md-4 control-label">Smoking Area</label>
                                <div class="col-md-6">
                                    {!! Form::select('smoking_area', [
                                          null => '---',
                                           'yes' => 'Yes',
                                           'no' => 'No'
                                      ],null, array('class' => 'form-control', 'id'=>'smoking_area')) !!}
                                    @if ($errors->has('smoking_area'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('smoking_area') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('resturent') ? ' has-error' : '' }}">
                                <label for="resturent" class="col-md-4 control-label">Resturent</label>
                                <div class="col-md-6">
                                    {!! Form::select('resturent', [
                                          null => '---',
                                           'yes' => 'Yes',
                                           'no' => 'No'
                                      ],null, array('class' => 'form-control', 'id'=>'resturent')) !!}
                                    @if ($errors->has('resturent'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resturent') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('dry_cleaning') ? ' has-error' : '' }}">
                                <label for="dry_cleaning" class="col-md-4 control-label">Dry Cleaning</label>
                                <div class="col-md-6">
                                    {!! Form::select('dry_cleaning', [
                                          null => '---',
                                           'yes' => 'Yes',
                                           'no' => 'No'
                                      ],null, array('class' => 'form-control', 'id'=>'dry_cleaning')) !!}
                                    @if ($errors->has('dry_cleaning'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('dry_cleaning') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('hotel_description') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Hotel Description</label>
                                <div class="col-md-6">
                                    {!! Form::textarea('hotel_description', null, array('placeholder' => 'Hotel Description','class' => 'form-control','style'=>'height:100px')) !!}
                                    @if ($errors->has('hotel_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('hotel_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend>Category / Other </legend>

                            <div class="form-group{{ $errors->has('pcategory') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                    {!! Form::select('pcategory[]', $pcategory,[], array('class' => 'form-control','multiple')) !!}
                                    @if ($errors->has('pcategory'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pcategory') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </fieldset>



                        <fieldset>
                            <legend>Continent/Country </legend>

                            <div class="form-group{{ $errors->has('continent') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Continent</label>
                                <div class="col-md-6">
                                    {!! Form::select('continent', $continent, null, array('class' => 'form-control', 'id'=>'continent')) !!}
                                    @if ($errors->has('continent'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('continent') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Country</label>
                                <div class="col-md-6">
                                    {!! Form::select('country', $country, null, array('class' => 'form-control', 'id'=>'country')) !!}
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </fieldset>










                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
