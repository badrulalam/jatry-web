@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newtpackage') }}"> Create New Package</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Fair</th>
                        <th>Property</th>
                        <th width="150px">Location</th>
                        <th>Image</th>
                        <th>Active</th>

                        <th width="150px">Action</th>
                    </tr>
                    @foreach ($data as $key => $pkg)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $pkg->title }}</td>
                            <td>{{ $pkg->price }}</td>
                            <td>
                            {!! $pkg->airfare_include ? 'Air Fare: Include <br/>':''!!}
                                {!!$pkg->airfare_exclude ? nl2br('Air Fare: Exclude<br/>'):''!!}
                                {!!$pkg->transport_include ? nl2br('Transport: Include<br/>'):''!!}
                                {{$pkg->transport_exclude ? nl2br(e('Transport: Exclude')):''}}

                             </td>

                            <td>

                                @if ($pkg->property_type != null)
                                    Type : {!! $pkg->property_type .'<br/>' !!}
                                @endif
                                @if ($pkg->hotel_rating != 0)
                                    Rating : {!! $pkg->hotel_rating !!}
                                @endif

                            </td>

                            <td>
                                    Continent : {!! $pkg->jcontinent->name .'<br/>' !!}
                                    Country : {!! $pkg->jcountry->name !!}


                            </td>


                            <td>
                                @if ($pkg->image1)
                                    {{ Html::image('/api/image/tpackage-image/'.$pkg->image1, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}
                                @endif
                            </td>
                            <td>{!!$pkg->isactive ? 'Yes':'No'!!}</td>
                            <td style=" vertical-align: middle;">

                                <a class="btn btn-primary" href="{{ route('tpackageedit',$pkg->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['tpackagedelete', $pkg->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
