@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newpcategory') }}"> Create New Category</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($data as $key => $pcat)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $pcat->name }}</td>
                            <td>
                                @if ($pcat->image)
                                    {{ Html::image('/api/image/pcategory-image/'.$pcat->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}
                                @endif
                            </td>
                            <td>

                                <a class="btn btn-primary" href="{{ route('pcategoryedit',$pcat->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['pcategorydelete', $pcat->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
