@extends('layouts.frontend')
@section('title', '- Flight')
@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Booking Status</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                    <div class="tabbable">
                        <ul class="nav nav-pills nav-stacked my-acc-nav-tab col-md-3">

                                <li class="active">
                                    <a href="#activeFlight" data-toggle="tab"><img src="/img/icon/Profile-17.png" alt="JATRY" >ACTIVE FLIGHT</a>
                                </li>
                            <li><a href="#cancleFlight" data-toggle="tab"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >CANCLE FLIGHT</a>
                            </li>

                            </ul>

                            <div class="tab-content col-md-9 clearfix">
                                <div class="tab-pane active" id="activeFlight">
                                       Active Flight
                                </div>

                                <div class="tab-pane" id="cancleFlight">
                                    Cancle Flight
                                </div>

                            </div>
                        </div>

                        <hr/><hr/>
                        {{--========Tab start======--}}




                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')

    <script type="text/javascript">


        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#/')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });

        // Javascript to enable link to tab





        //$(document).ready(function () {
        //    if (window.location.hash) {
        //        $('.nav-pills a[href="#'+window.location.hash+'"]').tab('show');
        //    }
        //});

        //$(function () {
        //    var activeTab = $('[href=' + location.hash + ']');
        //    activeTab && activeTab.tab('show');
        //});




    </script>
@stop
{{--@endsection--}}
