@extends('layouts.frontend')
@section('title', '- Flight')
@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Booking Status</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                    <div class="tabbable">
                        <ul class="nav nav-pills nav-stacked my-acc-nav-tab col-md-3">

                            <li class="active"><a href="#invoices" data-toggle="tab"><img src="/img/icon/Profile-17.png" alt="JATRY" >TICKETS</a>
                            </li>

                            <li><a href="#refundTicket" data-toggle="tab"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >REFUND TICKET</a>
                            </li>

                            <li><a href="#cancleTicket" data-toggle="tab"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >CANCLLED TICKET</a>
                            </li>

                            </ul>

                            <div class="tab-content col-md-9 clearfix">
                             <div class="tab-pane active" id="invoices">
                                <h3>Invoice</h3>
                                {{--==============Start===============--}}
                                 <div class="row">
                                     @foreach ($sale as $sle)
                                         <div class="col-sm-6 col-md-4 well">
                                             <div class="thumbnail" >
                                                 <div class="caption">
                                                     <h4>{{$sle->order_number}}</h4>

                                                     <p>
                                                         <a target="_blank" href="{{ Route('ticketsaleview', $sle->order_number) }}" class="btn btn-primary" role="button">View</a>
                                                         <a href="#" class="btn btn-primary" role="button">Refund</a>
                                                         <a href="#" class="btn btn-primary" role="button">Cancel</a>
                                                         {{--<a href="#" class="btn btn-default" role="button">Button</a>--}}
                                                     </p>
                                                 </div>
                                             </div>
                                         </div>


                                     @endforeach
                                 </div>

                                 {{--===============--}}
                                {{--==============End===============--}}


                                 {{--<ng-include src="'/template/sale/user-invoices.html'"></ng-include>--}}
                                </div>

                                <div class="tab-pane" id="refundTicket">
                                    Refund Tickets

                                </div>

                                <div class="tab-pane" id="cancleTicket">
                                    Cancle Tickets

                                </div>

                            </div>
                        </div>

                        <hr/><hr/>
                        {{--========Tab start======--}}




                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')

    <script type="text/javascript">


        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#/')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });

        // Javascript to enable link to tab





        //$(document).ready(function () {
        //    if (window.location.hash) {
        //        $('.nav-pills a[href="#'+window.location.hash+'"]').tab('show');
        //    }
        //});

        //$(function () {
        //    var activeTab = $('[href=' + location.hash + ']');
        //    activeTab && activeTab.tab('show');
        //});




    </script>
@stop
{{--@endsection--}}
