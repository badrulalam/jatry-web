@extends('layouts.frontend')
@section('title', '- Flight')
@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Booking Status</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                    <div class="tabbable">
                        <ul class="nav nav-pills nav-stacked my-acc-nav-tab col-md-3">

                                <li class="active">
                                    <a href="#activeBooking" data-toggle="tab"><img src="/img/icon/Profile-17.png" alt="JATRY" >MY BOOKINGS</a>
                                </li>
                            <li><a href="#cancleBooking" data-toggle="tab"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >CANCELED BOOKINGS</a>
                            </li>

                            </ul>

                            <div class="tab-content col-md-9 clearfix">
                                <div class="tab-pane active" id="activeBooking">
                                       Active Booking

                                    <table class="table table-bordered">
                                        <tr>
                                            <th>No</th>
                                            <th>Booking Number</th>
                                            <th>Purchase</th>
                                            <th width="250px">Action</th>
                                        </tr>
                                        @foreach ($intBookActive as $key => $intAct)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $intAct->order_number }}</td>
                                                <td>{{ $intAct->is_purchase? 'Yes': 'No' }}</td>
                                                <td>
                                                    {{--<a class="btn btn-primary" href="{{ route('jcontinentedit',$jconti->id) }}">View</a>--}}
                                                    <a class="btn btn-primary" href="{{ route('ticketbooking',$intAct->order_number) }}">View</a>


                                                    @if (!$intAct->is_purchase)
                                                        <a class="btn btn-primary" href="{{ route('ticketbookingcancle',$intAct->order_number) }}">Cancel</a>
                                                        <a class="btn btn-primary" href="{{ route('bookingticketpurchase',$intAct->order_number) }}">Purchase</a>
                                                    @endif

                                                </td>

                                            </tr>
                                        @endforeach
                                    </table>



                                </div>

                                <div class="tab-pane" id="cancleBooking">
                                    Cancle Booking
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>No</th>
                                            <th>Booking Number</th>
                                            <th width="150px">Action</th>
                                        </tr>
                                        @foreach ($intBookCancle as $key => $intCan)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $intCan->order_number }}</td>
                                                <td>View</td>

                                            </tr>
                                        @endforeach
                                    </table>
                                </div>

                            </div>
                        </div>

                        <hr/><hr/>
                        {{--========Tab start======--}}




                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')
    <script type="text/javascript">
        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#/')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });
        // Javascript to enable link to tab
    </script>
@stop
{{--@endsection--}}
