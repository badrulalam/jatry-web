@extends('layouts.frontend')

@section('content')



    <div ng-app="app">
        <ng-include src="'/template/confirm-ticket.html'"></ng-include>
    </div>
    <div class="container">

    <div class="panel panel-default" >

        <div class="panel-heading">Traveller List</div>

        <div class="panel-body">

        <table class="table table-bordered">
            <tr>
                <th></th>
                <th>Title</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>DOB</th>
                <th>Nationality</th>
                <th>Passport No.</th>
                <th>Issuing Country</th>
                <th>Passport Expiry Date</th>
                <th></th>

            </tr>
            @foreach ($cartitemair->passenger as $person)
                <tr>
                    <td>{{$person->passenger_type.' '.$person->passenger_order}}</td>
                    <td>{{$person->title}}</td>
                    <td>{{$person->first_name}}</td>
                    <td>{{$person->middle_name}}</td>
                    <td>{{$person->last_name}}</td>
                    <td>{{$person->dob}}</td>
                    <td>{{$person->nationality}}</td>
                    <td>{{$person->passport_no}}</td>
                    <td>{{$person->passport_issue_country}}</td>
                    <td>{{$person->passport_expiry_date}}</td>
                    <td><a href="">Edit</a></td>

                </tr>
            @endforeach

        </table>
        {{--<a href="/checkout-final" class="btn btn-default">Continue</a>--}}
    </div>
    </div>
    </div>



    <div class="container">

        <div class="panel panel-default" >

            <div class="panel-heading">Payment</div>

            <div class="panel-body">

                <div class="col-md-12">
                    <h3>Billing Address</h3>

                    First name : {{$cart->billing_firstname}}


                    </div>

            </div>
        </div>
    </div>


    <div class="container">

        <div class="panel panel-default" >

            <div class="panel-heading">Payment</div>

            <div class="panel-body">

                <div ng-app="app" style="padding: 15px ">
                    <ng-include src="'/template/checkout-final.html'"></ng-include>
                </div>

            </div>
        </div>
    </div>






@endsection
