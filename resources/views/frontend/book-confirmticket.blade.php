@extends('layouts.frontend')
@section('title', '- Booking')
@section('content')


    {{--<div ng-app="app">--}}
        {{--<ng-include src="'/template/confirm-ticket.html'"></ng-include>--}}
    {{--</div>--}}
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Route List</div>
            <div class="panel-body">
                @foreach ($cart->ticket->route as $route_index => $route)
                    <div>
                        <div>
                            <h4>{{$route_index+1}}. {{$route->Origin}} -> {{$route->Destination}}</h4>
                        </div>
                        @for ($i = 0; $i < count($route->segment); $i++)
                            <div>
                                <div class="row pm-border">
                                    <div class="col-sm-3">
                                <span><img src="{{'/api.travelport/images/' . $route->segment[$i]->Carrier . '.png'}}"
                                           alt="not found" style="height: 40px;"></span>

                                        {{$route->segment[$i]->Carrier}}
                                    </div>
                                    <div class="col-sm-3">

                                        {{$route->segment[$i]->Origin}}<br>
                                        {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('H:i') }}<br>
                                        {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('D, d M, Y') }}<br>
                                    </div>
                                    <div class="col-sm-3">
                                        {{$route->segment[$i]->FlightTime}} minutes | {{'Non Stop'}} | {{'free meal'}}
                                        {{--<hr>--}}
                                        {{--{{getBookingClass($route->segment[$i]->Key)}} | {{parse.Refundable == 'true'?--}}
                                        {{--'Refundable':'Non Refundable'}}--}}

                                    </div>
                                    <div class="col-sm-3">
                                        {{$route->segment[$i]->Destination}}<br>
                                        {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('H:i') }}<br>
                                        {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('D, d M, Y') }}<br>
                                    </div>

                                </div>
                                <div>
                                    @if ($i < count($route->segment[$i]) && isset($route->segment[$i]) && isset($route->segment[$i+1]))
                                    <div class="bg-warning pm-border center">
                                        <p class="text-center">Layover: {{$route->segment[$i]->Destination}}
                                            |
                                            {{ date('G:i', strtotime($route->segment[$i+1]->DepartureTime) - strtotime($route->segment[$i]->ArrivalTime)) }}
                                        </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endfor
                    </div>
                @endforeach
            </div>
        </div>

    <div class="panel panel-default" >

        <div class="panel-heading">Traveller List</div>

        <div class="panel-body">

        <table class="table table-bordered">
            <tr>
                <th></th>
                <th>Title</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>DOB</th>
                <th>Nationality</th>
                <th>Passport No.</th>
                <th>Issuing Country</th>
                <th>Passport Expiry Date</th>
                <th></th>

            </tr>
            @foreach ($cart->ticket->passenger as $person)
                <tr>
                    <td>{{$person->passenger_type.' '.($person->passenger_order + 1)}}</td>
                    <td>{{$person->title}}</td>
                    <td>{{$person->first_name}}</td>
                    <td>{{$person->middle_name}}</td>
                    <td>{{$person->last_name}}</td>
                    <td>{{$person->dob}}</td>
                    <td>{{$person->nationality}}</td>
                    <td>{{$person->passport_no}}</td>
                    <td>{{$person->passport_issue_country}}</td>
                    <td>{{$person->passport_expiry_date}}</td>
                    <td><a href="">Edit</a></td>

                </tr>
            @endforeach

        </table>
        {{--<a href="/checkout-final" class="btn btn-default">Continue</a>--}}
    </div>
    </div>
    </div>



    <div class="container">

        <div class="panel panel-default" >

            <div class="panel-heading">Payment</div>

            <div class="panel-body">

                <div class="col-md-6">
                    <h3>Billing Address</h3>

                    First Name : {{$cart->billing_firstname}}<br/>
                    Last Name : {{$cart->billing_lastname}}<br/>
                    Email : {{$cart->billing_email}}<br/>
                    Address : {{$cart->billing_address}}<br/>
                    Phone : {{$cart->billing_phone}}<br/>


                </div>


                <div class="col-md-6">
                    <h3>Card Info</h3>

                    Card Holder : {{$cart->cardholder}}<br/>
{{--                    Card Number : {{$cart->card_number}}<br/>--}}
                    Card Number : {!! substr_replace($cart->card_number,"#####",2, -1) !!}<br/>
                    Exp Date : {{$cart->exp_date}}<br/>
                    CVV : *****<br/>


                </div>

            </div>
        </div>
    </div>


    <div class="container">
        <div class="panel panel-default" >
            <div class="panel-body">
                <div ng-app="app" style="padding: 15px ">
                    <ng-include src="'/template/book-checkout-final.html'"></ng-include>
                </div>

            </div>
        </div>
    </div>






@endsection
<script>


    window.onload = function() {
        var __selector = $(".__iata");
        for(var i = 0; i< __selector.length; i++){
            var input = __selector.eq(i).attr('value');
            __selector.eq(i).html(function(input) {
                if(!input){
                    return;
                }
                var airport = airportJson.find(function (row) {
                    return row.ac === input;
                });
                return airport ? airport.cn : "";
            }(input))
        }
    }
</script>