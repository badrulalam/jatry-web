@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="owl-carousel owl-theme">
                @foreach ($fepackage as $key => $pkg)
                <div class="item">
                    {{ Html::image('/api/image/tpackage-image/'.$pkg->image1, 'a picture', array('class' => 'img-responsive')) }}
                    <a href=""><h3>{{$pkg->title}}</h3></a>
                    <h4>{{$pkg->price}}</h4>
                </div>
                @endforeach
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-10 col-md-offset-1">


            @foreach ($data as $key => $cnt)
                <div class="col-md-4">
                    <a href="{{ route('single-country-tour',$cnt->slug) }}">{{ Html::image('/api/image/jcountry-image/'.$cnt->image, 'a picture', array('class' => 'img-responsive')) }}</a>
                    <a href="{{ route('single-country-tour',$cnt->slug) }}"><h3>{{$cnt->name}}</h3></a>

                </div>
            @endforeach




        </div>
    </div>



@endsection


@section('page-script')

    <style>

    </style>


    <script type="text/javascript">


        $(document).ready(function() {

            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:3,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:true,
                        loop:false
                    }
                }
            })
        });


    </script>
@stop
{{--@endsection--}}

