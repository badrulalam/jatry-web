@extends('layouts.frontend')
@section('title', '- Home')
@section('content')
    <br/>
    <h1 class="home-head-h1">BOOK DOMESTIC & INTERNATIONAL FLIGHTS</h1>
    <br/>
    <h4 class="home-head-h4">LOWEST PRICE GUARANTED</h4>
    <br/>

    <div ng-app="app">
        <ng-include src="'/template/home.material.html'"></ng-include>
    </div>


    <div class="container">
    <div class="row">

        <div class="col-sm-4 home-faci-container" style="text-align: center">
           <div id="home-faci-1" class="home-facility-icon">
               <img src="/img/icon/save-big.png" alt="JATRY" >
               <br/><br/>
               <span>SAVE BIG WITH US</span>
           </div>
        </div>
        <div class="col-sm-4 home-faci-container">
            <div id="home-faci-2" class="home-facility-icon">
                <img src="/img/icon/cash-deli-icon.png" alt="JATRY" >
                <br/><br/>
                <span>CASH ON DELIVERY</span>
            </div>
        </div>
        <div class="col-sm-4 home-faci-container">
            <div id="home-faci-3" class="home-facility-icon">
                <img src="/img/icon/customer-service-icon.png" alt="JATRY" >
                <br/><br/>
                <span>AWESOME CUSTOMER SERVICE</span>
            </div>
        </div>

    </div>
    </div>

    <div class="container">
    <div class="row">

        <div class="col-sm-12">
            <br/><br/><br/>
            <h1 class="home-head-h1">DOWNLOAD OUR MOBILE APPS</h1>
            <br/>
            <h4 class="home-head-h4">BUY ANYTIME ANYWHERE FROM YOUR PHONE</h4>
            <br/>

        </div>
        <div class="col-sm-4 col-sm-offset-4">
            <img src="/img/icon/andr-icon.png" alt="JATRY" >
            <img class="pull-right" src="/img/icon/apple-icon.png" alt="JATRY" >

        </div>
    </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                <br/><br/><br/>
                <h1 class="home-head-h1">SUBSCRIBE TO OUR NEWSLETTER</h1>
                <br/>
                <h4 class="home-head-h4">STAY ALERT OF THE LETEST DEALS AND LOWEST PRICE</h4>
                <br/>

            </div>
            <div class="col-sm-4 col-sm-offset-4" style=" text-align: center">
                <form>

                    <input class="col-sm-12" type="text" name="newsletter" />
                     <br/>
                     <br/>
                    <input class="btn btn-primary col-sm-6 col-sm-offset-3" type="submit" value="REGISTER" />

                </form>


            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
@endsection
