@extends('layouts.frontend')

@section('content')


    <div class="container">
        <h1>in checkout final</h1>

        <div class="row">
            <div class="col-md-6">
                <table class="table table-bordered">
                    <tr>
                        <td>Billing firstname:</td>
                        <td>{{$cart->billing_firstname}}</td>
                    </tr>
                    <tr>
                        <td>Billing lastname:</td>
                        <td>{{$cart->billing_lastname}}</td>
                    </tr>
                    <tr>
                        <td>Billing email:</td>
                        <td>{{$cart->billing_email}}</td>
                    </tr>
                    <tr>
                        <td>Billing address:</td>
                        <td>{{$cart->billing_address}}</td>
                    </tr>
                    <tr>
                        <td>Billing city:</td>
                        <td>{{$cart->billing_city}}</td>
                    </tr>
                    <tr>
                        <td>Billing postalcode:</td>
                        <td>{{$cart->billing_postalcode}}</td>
                    </tr>
                    <tr>
                        <td>Billing country:</td>
                        <td>{{$cart->billing_country}}</td>
                    </tr>
                    <tr>
                        <td>Billing state:</td>
                        <td>{{$cart->billing_state}}</td>
                    </tr>
                    <tr>
                        <td>Billing uhone:</td>
                        <td>{{$cart->billing_phone}}</td>
                    </tr>


                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-bordered">
                    <tr>
                        <td>Shipping firstname:</td>
                        <td>{{$cart->shipping_firstname}}</td>
                    </tr>
                    <tr>
                        <td>Shipping lastname:</td>
                        <td>{{$cart->shipping_lastname}}</td>
                    </tr>
                    <tr>
                        <td>Shipping email:</td>
                        <td>{{$cart->shipping_email}}</td>
                    </tr>
                    <tr>
                        <td>Shipping address:</td>
                        <td>{{$cart->shipping_address}}</td>
                    </tr>
                    <tr>
                        <td>Shipping city:</td>
                        <td>{{$cart->shipping_city}}</td>
                    </tr>
                    <tr>
                        <td>Shipping postalcode:</td>
                        <td>{{$cart->shipping_postalcode}}</td>
                    </tr>
                    <tr>
                        <td>Shipping country:</td>
                        <td>{{$cart->shipping_country}}</td>
                    </tr>
                    <tr>
                        <td>Shipping state:</td>
                        <td>{{$cart->shipping_state}}</td>
                    </tr>
                    <tr>
                        <td>Shipping phone:</td>
                        <td>{{$cart->shipping_phone}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div ng-app="app">
            <ng-include src="'/template/checkout-final.html'"></ng-include>
        </div>
    </div>

@endsection
