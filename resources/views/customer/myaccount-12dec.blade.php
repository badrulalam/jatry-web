@extends('layouts.frontend')
@section('title', '- Myaccount')
@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Account</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                    <div class="tabbable">
                        <ul class="nav nav-pills nav-stacked my-acc-nav-tab col-md-3">

                                <li class="active">
                                    <a href="#billingAddress" data-toggle="tab"><img src="/img/icon/Profile-17.png" alt="JATRY" >PROFILE</a>
                                </li>
                            <li><a href="#traveler" data-toggle="tab"><img src="/img/icon/Travellers-17-17.png" alt="JATRY" >TRAVELERS</a>
                            </li>

                                {{--<li><a href="#invoices" data-toggle="tab"><img src="/img/icon/Profile-17.png" alt="JATRY" >PAYMENT INFORMATION</a>--}}
                                {{--</li>--}}
                            <li><a href="#billingInformation" data-toggle="tab"><img src="/img/icon/Credit-17.png" alt="JATRY" >CREDIT CARD</a>
                            </li>
                            <li><a href="#shippingAddress" data-toggle="tab"><img src="/img/icon/Shippng Address-17-17.png" alt="JATRY" >SHIPPING INFORMATION</a>
                            </li>
                            <li><a href="#searchRoute" data-toggle="tab"><img src="/img/icon/Shippng Address-17-17.png" alt="JATRY" >SEARCH ROUTE</a>
                            </li>
                            </ul>

                            <div class="tab-content col-md-9 clearfix">

                                <div class="tab-pane active" id="billingAddress">

                                    @if ($billingsuccess = Session::get('billingsuccess'))
                                        <div class="alert alert-success">
                                            <p>{{ $billingsuccess }}</p>
                                        </div>
                                    @endif

                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountbillinginfosave') }}">
                                        {{ csrf_field() }}


                                            <div class="form-group{{ $errors->has('customer_code') ? ' has-error' : '' }}">
                                                <label for="code" class="col-md-4 control-label">Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_code', $user->customer->code, array('placeholder' => 'code','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_code'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_code') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_firstname') ? ' has-error' : '' }}">
                                                <label for="customer_firstname" class="col-md-4 control-label">First Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_firstname', $user->customer->first_name, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_firstname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_firstname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_lastname') ? ' has-error' : '' }}">
                                                <label for="customer_lastname" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_lastname', $user->customer->last_name, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_lastname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_lastname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_email') ? ' has-error' : '' }}">
                                                <label for="customer_email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    {!! Form::text('customer_email', $user->customer->email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">
                                                <label for="customer_address" class="col-md-4 control-label">Address</label>
                                                <div class="col-md-6">
                                                    {!! Form::textarea('customer_address', $user->customer->address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                                    @if ($errors->has('customer_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_city') ? ' has-error' : '' }}">
                                                <label for="customer_city" class="col-md-4 control-label">City</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_city', $user->customer->city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_city'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_city') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_state') ? ' has-error' : '' }}">
                                                <label for="customer_state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_state', $user->customer->state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_state'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_state') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_postalcode') ? ' has-error' : '' }}">
                                                <label for="customer_postalcode" class="col-md-4 control-label">Postal Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_postalcode', $user->customer->postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_postalcode'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_postalcode') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_country') ? ' has-error' : '' }}">
                                                <label for="customer_country" class="col-md-4 control-label">Country</label>
                                                <div class="col-md-6">
                                                    {!! Form::select('customer_country', Countries::getList('en', 'php', 'icu'), $user->customer->country, array('class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_country'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_country') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
                                                <label for="customer_phone" class="col-md-4 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_phone', $user->customer->phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_phone'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_phone') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            {{--<div class="form-group{{ $errors->has('customer_image') ? ' has-error' : '' }}">--}}
                                                {{--<label for="customer_image" class="col-md-4 control-label">Image</label>--}}
                                                {{--<div class="col-md-4">--}}
                                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                                    {{--{{ Form::file('customer_image', ['class' => 'form-control1', 'id' => 'customer_image']) }}--}}
                                                    {{--@if ($errors->has('customer_image'))--}}
                                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_image') }}</strong>--}}
                                    {{--</span>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}

                                                {{--<div class="col-md-2">--}}
                                                    {{--@if ($user->customer->image)--}}
                                                        {{--{{ Html::image('/api/image/customer-image/'.$user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="tab-pane" id="traveler">


                                    @if ($travelersuccess = Session::get('travelersuccess'))
                                        <div class="alert alert-success">
                                            <p>{{ $travelersuccess }}</p>
                                        </div>
                                    @endif



                                    <h3>Traveller List</h3>
                                    <a class="btn btn-primary" href="{{ Route('customeraddtraveler') }}">Add New</a>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>First Name</th>
                                            <th>Middle Name</th>
                                            <th>Last Name</th>
                                            <th>DOB</th>
                                            <th>Nationality</th>
                                            <th>Passport No.</th>
                                            <th>Issuing Country</th>
                                            <th>Passport Expiry Date</th>
                                            <th>Action</th>

                                        </tr>
                                        @foreach ($selpassenegr as $person)
                                            <tr>
                                                <th>{{$person->passenger_type.' '.$person->passenger_order}}</th>
                                                <th>{{$person->title}}</th>
                                                <th>{{$person->first_name}}</th>
                                                <th>{{$person->middle_name}}</th>
                                                <th>{{$person->last_name}}</th>
                                                <th>{{$person->dob}}</th>
                                                <th>{{$person->nationality}}</th>
                                                <th>{{$person->passport_no}}</th>
                                                <th>{{$person->passport_issue_country}}</th>
                                                <th>{{$person->passport_expiry_date}}</th>
                                                <th><a href="{{ Route('customeraddtraveleredit', $person->id) }}">Edit</a></th>

                                            </tr>
                                        @endforeach

                                    </table>






                                </div>
                                {{--<div class="tab-pane" id="invoices">--}}
                                    {{--<h3>Invoice</h3>--}}
                                    {{--<ng-include src="'/template/sale/user-invoices.html'"></ng-include>--}}
                                {{--</div>--}}

                                <div class="tab-pane" id="billingInformation">


                                    @if ($billinginfosuccess = Session::get('billinginfosuccess'))
                                        <div class="alert alert-success">
                                            <p>{{ $billinginfosuccess }}</p>
                                        </div>
                                    @endif



                                    <h3>Card List</h3>
                                    <a class="btn btn-primary" href="{{ Route('customeraddcard') }}">Add New</a>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Sl</th>
                                            <th>Cardholder</th>
                                            <th>Card Number</th>
                                            <th>Expiry Date</th>
                                            <th>Action</th>

                                        </tr>
                                        @foreach ($cardinfo as $card)
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>{{$card->cardholder}}</th>
                                                <th>{{$card->card_number}}</th>
                                                <th>{{$card->exp_date}}</th>
                                                <th><a href="{{ Route('customeraddcardedit', $card->cardtrak) }}">Edit</a></th>

                                            </tr>
                                        @endforeach

                                    </table>
                                </div>
                                <div class="tab-pane" id="shippingAddress">

                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountshippinginfosave') }}">
                                        {{ csrf_field() }}





                                        {{--<fieldset>--}}
                                            {{--<legend>Shipping Information</legend>--}}



                                            <div class="form-group{{ $errors->has('shipping_firstname') ? ' has-error' : '' }}">
                                                <label for="shipping_firstname" class="col-md-4 control-label">First Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_firstname', $user->customer->shipping_firstname, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_firstname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_firstname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_lastname') ? ' has-error' : '' }}">
                                                <label for="shipping_lastname" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_lastname', $user->customer->shipping_lastname, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_lastname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_lastname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_email') ? ' has-error' : '' }}">
                                                <label for="shipping_email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_email', $user->customer->shipping_email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                                    @if ($errors->has('shipping_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
                                                <label for="shipping_address" class="col-md-4 control-label">Address</label>
                                                <div class="col-md-6">
                                                    {!! Form::textarea('shipping_address', $user->customer->shipping_address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                                    @if ($errors->has('shipping_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_city') ? ' has-error' : '' }}">
                                                <label for="shipping_city" class="col-md-4 control-label">City</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_city', $user->customer->shipping_city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_city'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_city') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_state') ? ' has-error' : '' }}">
                                                <label for="shipping_state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_state', $user->customer->shipping_state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_state'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_state') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_postalcode') ? ' has-error' : '' }}">
                                                <label for="shipping_postalcode" class="col-md-4 control-label">Postal Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_postalcode', $user->customer->shipping_postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_postalcode'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_postalcode') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_country') ? ' has-error' : '' }}">
                                                <label for="shipping_country" class="col-md-4 control-label">Country</label>
                                                <div class="col-md-6">
                                                    {!! Form::select('shipping_country', Countries::getList('en', 'php', 'icu'), $user->customer->shipping_country, array('class' => 'form-control')) !!}

                                                    @if ($errors->has('shipping_country'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_country') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_phone') ? ' has-error' : '' }}">
                                                <label for="shipping_phone" class="col-md-4 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_phone', $user->customer->shipping_phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_phone'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_phone') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>


                                        {{--</fieldset>--}}


                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="tab-pane" id="searchRoute">





                                    <h3>Route Search</h3>

                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Sl</th>
                                            <th>Rogin</th>
                                            <th>Destination</th>

                                        </tr>
                                        @foreach ($routeSearch as $key=>$routeSe)
                                            <tr>
                                                <th>{{$key+1}}</th>
                                                <th>{{$routeSe->Origin}}</th>
                                                <th>{{$routeSe->Destination}}</th>
                                            </tr>
                                        @endforeach

                                    </table>
                                </div>
                            </div>
                        </div>

                        <hr/><hr/>
                        {{--========Tab start======--}}




                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')

    <script type="text/javascript">


        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#/')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });

        // Javascript to enable link to tab





        //$(document).ready(function () {
        //    if (window.location.hash) {
        //        $('.nav-pills a[href="#'+window.location.hash+'"]').tab('show');
        //    }
        //});

        //$(function () {
        //    var activeTab = $('[href=' + location.hash + ']');
        //    activeTab && activeTab.tab('show');
        //});




    </script>
@stop
{{--@endsection--}}
