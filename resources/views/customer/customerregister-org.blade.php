@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Register</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    {!! Form::open(array('route' => 'customerregistersave','method'=>'POST', 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal')) !!}
                    <fieldset>
                        {{--<legend>Login Information</legend>--}}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    </fieldset>



                    {{--<fieldset>--}}
                        {{--<legend>Customer Information</legend>--}}

                        {{--<div class="form-group{{ $errors->has('customer_firstname') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_firstname" class="col-md-4 control-label">First Name</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_firstname', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_firstname'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_firstname') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('customer_lastname') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_lastname" class="col-md-4 control-label">Last Name</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_lastname', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_lastname'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_lastname') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('customer_email') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_email" class="col-md-4 control-label">E-Mail</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_email', null, array('placeholder' => 'Agent Email','class' => 'form-control')) !!}--}}

                                {{--@if ($errors->has('customer_email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_address" class="col-md-4 control-label">Address</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::textarea('customer_address', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}--}}
                                {{--@if ($errors->has('customer_address'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_address') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('customer_city') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_city" class="col-md-4 control-label">City</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_city', null, array('placeholder' => 'customer_city','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_city'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_city') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('customer_state') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_state" class="col-md-4 control-label">State</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_state', null, array('placeholder' => 'customer_state','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_state'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_state') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('customer_postalcode') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_postalcode" class="col-md-4 control-label">Postal Code</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_postalcode', null, array('placeholder' => 'customer_postalcode','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_postalcode'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_postalcode') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('customer_country') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_country" class="col-md-4 control-label">Country</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::select('customer_country', Countries::getList('en', 'php', 'icu'), null, array('class' => 'form-control')) !!}--}}

                                {{--@if ($errors->has('customer_country'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_country') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_phone" class="col-md-4 control-label">Phone</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--{!! Form::text('customer_phone', null, array('placeholder' => 'customer_phone','class' => 'form-control')) !!}--}}
                                {{--@if ($errors->has('customer_phone'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_phone') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('customer_image') ? ' has-error' : '' }}">--}}
                            {{--<label for="customer_image" class="col-md-4 control-label">Image</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                {{--{{ Form::file('customer_image', ['class' => 'form-control1', 'id' => 'customer_image']) }}--}}
                                {{--@if ($errors->has('customer_image'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('customer_image') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}




                      {{--</fieldset>--}}
                    <br/>
                    <fieldset>
                        {{--<legend></legend>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-6 col-md-offset-4">--}}
                            {{--<button type="submit" class="btn btn-primary">--}}
                                {{--<i class="fa fa-btn fa-user"></i> Save--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Save
                                </button>
                                <a class="btn btn-primary" href="{{ Route('tofacebook') }}">Facebook</a>
                            </div>
                        </div>


                    </fieldset>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
