@extends('layouts.frontend')
@section('title', '- Registration')
@section('content')

    <div class="container" style="min-height: 820px;">
        <div class="row">

            <div class="col-sm-12">
                <br/><br/><br/>
                <h1 class="home-head-h1">CREATE YOUR ACCOUNT</h1>
                <br/>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 register-facebook-cont">
                <p>LOGIN WITH YOUR FACEBOOK ACCOUNT</p><br/>
                <a  href="{{ Route('tofacebook') }}"><img src="/img/register-facebook.png" alt="JATRY" height="40px;" ></a>
                <br/> <p>WE DO NOT POST ON YOUR WALL</p><br/>
                <p>WE DO NOT COLLECT YOUR INFORMATION</p><br/>
                <p>WE DO NOT SHARE YOYR PRIVATE DATA</p><br/>
            </div>
            <div class="col-sm-8">


                    <div class="col-sm-12 register-container">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        {!! Form::open(array('route' => 'customerregistersave','method'=>'POST', 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal')) !!}


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" >NAME</label>

                                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif

                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email"  >EMAIL ADDRESS</label>


                                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">PASSWORD</label>


                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" >CONFIRM PASSWORD</label>


                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif

                            </div>

                            <div class="form-group{{ $errors->has('termscondition') ? ' has-error' : '' }}">
                                <label for="name"></label>

                                    <label>{{ Form::checkbox('termscondition', 1 ,  false, array('class' => 'name')) }}
                                        I agree to the Terms of Use and Privacy Policy</label>
                                    @if ($errors->has('termscondition'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('termscondition') }}</strong>
                                    </span>
                                    @endif

                            </div>


                        <br/>


                            <input class="btn btn-primary col-sm-4 col-sm-offset-4" type="submit" value="REGISTER" />

                        {!! Form::close() !!}
                    </div>
                </div>

        </div>

    </div>


@endsection
