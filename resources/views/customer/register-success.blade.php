@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Thank you</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

<a class="btn btn-primary" href="{{ Route('customermyaccount') }}">Complete your profile</a>
<a class="btn btn-primary" href="{{ Route('home') }}">Homepage</a>






                </div>
            </div>
        </div>
    </div>

@endsection
