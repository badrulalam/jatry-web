@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Account</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                        <div id="exTab1" class="container1">
                            <ul  class="col-md-3 nav1 nav-pills">
                                <li class="active">
                                    <a  href="#myAccount" data-toggle="tab">My-account</a>
                                </li>
                                <li><a href="#billingAddress" data-toggle="tab">Billing address</a>
                                </li>
                                <li><a href="#shippingAddress" data-toggle="tab">Shipping address</a>
                                </li>
                                <li><a href="#invoices" data-toggle="tab">Invoice</a>
                                </li>
                            </ul>

                            <div class="tab-content col-md-9 clearfix">
                                <div class="tab-pane active" id="myAccount">
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif

                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountlogininfosave') }}">
                                        {{ csrf_field() }}


                                        <fieldset>
                                            <legend>Login Information</legend>

                                            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                                                <label for="user_name" class="col-md-4 control-label">Name</label>

                                                <div class="col-md-6">
                                                    <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $user->name }}">

                                                    @if ($errors->has('user_name'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input id="user_email" type="email" class="form-control" name="user_email" value="{{ $user->email }}">

                                                    @if ($errors->has('user_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                <div class="col-md-6">
                                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                        </fieldset>





                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>


                                </div>
                                <div class="tab-pane" id="billingAddress">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountbillinginfosave') }}">
                                        {{ csrf_field() }}





                                        <fieldset>
                                            <legend>Billing Information</legend>

                                            <div class="form-group{{ $errors->has('customer_code') ? ' has-error' : '' }}">
                                                <label for="code" class="col-md-4 control-label">Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_code', $user->customer->code, array('placeholder' => 'code','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_code'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_code') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_firstname') ? ' has-error' : '' }}">
                                                <label for="customer_firstname" class="col-md-4 control-label">First Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_firstname', $user->customer->first_name, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_firstname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_firstname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_lastname') ? ' has-error' : '' }}">
                                                <label for="customer_lastname" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_lastname', $user->customer->last_name, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_lastname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_lastname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_email') ? ' has-error' : '' }}">
                                                <label for="customer_email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    {!! Form::text('customer_email', $user->customer->email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">
                                                <label for="customer_address" class="col-md-4 control-label">Address</label>
                                                <div class="col-md-6">
                                                    {!! Form::textarea('customer_address', $user->customer->address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                                    @if ($errors->has('customer_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_city') ? ' has-error' : '' }}">
                                                <label for="customer_city" class="col-md-4 control-label">City</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_city', $user->customer->city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_city'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_city') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_state') ? ' has-error' : '' }}">
                                                <label for="customer_state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_state', $user->customer->state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_state'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_state') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_postalcode') ? ' has-error' : '' }}">
                                                <label for="customer_postalcode" class="col-md-4 control-label">Postal Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_postalcode', $user->customer->postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_postalcode'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_postalcode') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_country') ? ' has-error' : '' }}">
                                                <label for="customer_country" class="col-md-4 control-label">Country</label>
                                                <div class="col-md-6">
                                                    {!! Form::select('customer_country', Countries::getList('en', 'php', 'icu'), $user->customer->country, array('class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_country'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_country') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
                                                <label for="customer_phone" class="col-md-4 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_phone', $user->customer->phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_phone'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_phone') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_image') ? ' has-error' : '' }}">
                                                <label for="customer_image" class="col-md-4 control-label">Image</label>
                                                <div class="col-md-4">
                                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                                    {{ Form::file('customer_image', ['class' => 'form-control1', 'id' => 'customer_image']) }}
                                                    @if ($errors->has('customer_image'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_image') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-2">
                                                    @if ($user->customer->image)
                                                        {{ Html::image('/api/image/customer-image/'.$user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                                    @endif

                                                    {{--@if ($user->customer->image)--}}
                                                    {{--@if ($user->facebook_id != '' || $user->facebook_id != null)--}}
                                                    {{--{{ Html::image($user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}--}}
                                                    {{--@else--}}
                                                    {{--{{ Html::image('/api/image/customer-image/'.$user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}--}}
                                                    {{--@endif--}}
                                                    {{--@endif--}}

                                                </div>
                                            </div>




                                        </fieldset>


                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="tab-pane" id="shippingAddress">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountshippinginfosave') }}">
                                        {{ csrf_field() }}





                                        <fieldset>
                                            <legend>Shipping Information</legend>



                                            <div class="form-group{{ $errors->has('shipping_firstname') ? ' has-error' : '' }}">
                                                <label for="shipping_firstname" class="col-md-4 control-label">First Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_firstname', $user->customer->shipping_firstname, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_firstname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_firstname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_lastname') ? ' has-error' : '' }}">
                                                <label for="shipping_lastname" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_lastname', $user->customer->shipping_lastname, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_lastname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_lastname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_email') ? ' has-error' : '' }}">
                                                <label for="shipping_email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_email', $user->customer->shipping_email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                                    @if ($errors->has('shipping_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
                                                <label for="shipping_address" class="col-md-4 control-label">Address</label>
                                                <div class="col-md-6">
                                                    {!! Form::textarea('shipping_address', $user->customer->shipping_address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                                    @if ($errors->has('shipping_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('shipping_city') ? ' has-error' : '' }}">
                                                <label for="shipping_city" class="col-md-4 control-label">City</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_city', $user->customer->shipping_city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_city'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_city') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_state') ? ' has-error' : '' }}">
                                                <label for="shipping_state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_state', $user->customer->shipping_state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_state'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_state') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_postalcode') ? ' has-error' : '' }}">
                                                <label for="shipping_postalcode" class="col-md-4 control-label">Postal Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_postalcode', $user->customer->shipping_postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_postalcode'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_postalcode') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_country') ? ' has-error' : '' }}">
                                                <label for="shipping_country" class="col-md-4 control-label">Country</label>
                                                <div class="col-md-6">
                                                    {!! Form::select('shipping_country', Countries::getList('en', 'php', 'icu'), $user->customer->shipping_country, array('class' => 'form-control')) !!}

                                                    @if ($errors->has('shipping_country'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_country') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('shipping_phone') ? ' has-error' : '' }}">
                                                <label for="shipping_phone" class="col-md-4 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('shipping_phone', $user->customer->shipping_phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                                    @if ($errors->has('shipping_phone'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('shipping_phone') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>


                                        </fieldset>


                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="tab-pane" id="invoices">
                                    <h3>Invoice</h3>
                                    {{--<ng-include src="'/template/package/all-country-tour.html'"></ng-include>--}}
                                </div>
                            </div>
                        </div>

                        <hr></hr>
                        {{--========Tab start======--}}

                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')

    <style>

    </style>


    <script type="text/javascript">


        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });

        // Javascript to enable link to tab





        //$(document).ready(function () {
        //    if (window.location.hash) {
        //        $('.nav-pills a[href="#'+window.location.hash+'"]').tab('show');
        //    }
        //});

        //$(function () {
        //    var activeTab = $('[href=' + location.hash + ']');
        //    activeTab && activeTab.tab('show');
        //});




    </script>
@stop
{{--@endsection--}}
