@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Account</div>

                <div class="panel-body">


                        {{--========Tab start======--}}
                        {{--<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>--}}
                        <div id="exTab1" class="container1">
                            <ul  class="nav nav-pills">
                                <li class="active">
                                    <a  href="#myAccount" data-toggle="tab">My-account</a>
                                </li>
                                <li><a href="#invoices" data-toggle="tab">Invoice</a>
                                </li>
                                <li><a href="#billingAddress" data-toggle="tab">Billing</a>
                                </li>
                                {{--<li><a href="#4a" data-toggle="tab">Background color</a>--}}
                                {{--</li>--}}
                            </ul>

                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="myAccount">
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif

                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('customermyaccountsave') }}">
                                        {{ csrf_field() }}


                                        <fieldset>
                                            <legend>Login Information</legend>

                                            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                                                <label for="user_name" class="col-md-4 control-label">Name</label>

                                                <div class="col-md-6">
                                                    <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $user->name }}">

                                                    @if ($errors->has('user_name'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input id="user_email" type="email" class="form-control" name="user_email" value="{{ $user->email }}">

                                                    @if ($errors->has('user_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                <div class="col-md-6">
                                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                        </fieldset>


                                        <fieldset>
                                            <legend>Customer Information</legend>

                                            <div class="form-group{{ $errors->has('customer_code') ? ' has-error' : '' }}">
                                                <label for="code" class="col-md-4 control-label">Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_code', $user->customer->code, array('placeholder' => 'code','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_code'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_code') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_firstname') ? ' has-error' : '' }}">
                                                <label for="customer_firstname" class="col-md-4 control-label">First Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_firstname', $user->customer->first_name, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_firstname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_firstname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_lastname') ? ' has-error' : '' }}">
                                                <label for="customer_lastname" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_lastname', $user->customer->last_name, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_lastname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_lastname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_email') ? ' has-error' : '' }}">
                                                <label for="customer_email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    {!! Form::text('customer_email', $user->customer->email, array('placeholder' => 'Agent Email','class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">
                                                <label for="customer_address" class="col-md-4 control-label">Address</label>
                                                <div class="col-md-6">
                                                    {!! Form::textarea('customer_address', $user->customer->address, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                                                    @if ($errors->has('customer_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('customer_city') ? ' has-error' : '' }}">
                                                <label for="customer_city" class="col-md-4 control-label">City</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_city', $user->customer->city, array('placeholder' => 'customer_city','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_city'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_city') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_state') ? ' has-error' : '' }}">
                                                <label for="customer_state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_state', $user->customer->state, array('placeholder' => 'customer_state','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_state'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_state') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_postalcode') ? ' has-error' : '' }}">
                                                <label for="customer_postalcode" class="col-md-4 control-label">Postal Code</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_postalcode', $user->customer->postalcode, array('placeholder' => 'customer_postalcode','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_postalcode'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_postalcode') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_country') ? ' has-error' : '' }}">
                                                <label for="customer_country" class="col-md-4 control-label">Country</label>
                                                <div class="col-md-6">
                                                    {!! Form::select('customer_country', Countries::getList('en', 'php', 'icu'), $user->customer->country, array('class' => 'form-control')) !!}

                                                    @if ($errors->has('customer_country'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_country') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
                                                <label for="customer_phone" class="col-md-4 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    {!! Form::text('customer_phone', $user->customer->phone, array('placeholder' => 'customer_phone','class' => 'form-control')) !!}
                                                    @if ($errors->has('customer_phone'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_phone') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('customer_image') ? ' has-error' : '' }}">
                                                <label for="customer_image" class="col-md-4 control-label">Image</label>
                                                <div class="col-md-4">
                                                    {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                                    {{ Form::file('customer_image', ['class' => 'form-control1', 'id' => 'customer_image']) }}
                                                    @if ($errors->has('customer_image'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('customer_image') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-2">
                                                    @if ($user->customer->image)
                                                        {{ Html::image('/api/image/customer-image/'.$user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                                    @endif

                                                    {{--@if ($user->customer->image)--}}
                                                    {{--@if ($user->facebook_id != '' || $user->facebook_id != null)--}}
                                                    {{--{{ Html::image($user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}--}}
                                                    {{--@else--}}
                                                    {{--{{ Html::image('/api/image/customer-image/'.$user->customer->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}--}}
                                                    {{--@endif--}}
                                                    {{--@endif--}}

                                                </div>
                                            </div>




                                        </fieldset>


                                        <br/>
                                        <fieldset>
                                            {{--<legend></legend>--}}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>

                                    {{--<ul>--}}
                                        {{--<li>Login Info</li>--}}
                                        {{--<li>Agency Info</li>--}}
                                        {{--<li>Agent Info</li>--}}
                                    {{--</ul>--}}
                                </div>
                                <div class="tab-pane" id="invoices">
                                    <h3>Invoice</h3>
                                    {{--<ng-include src="'/template/package/all-country-tour.html'"></ng-include>--}}
                                </div>
                                {{--<div class="tab-pane" id="3a">--}}
                                    {{--<h3>We applied clearfix to the tab-content to rid of the gap between the tab and the content</h3>--}}
                                {{--</div>--}}
                                {{--<div class="tab-pane" id="4a">--}}
                                    {{--<h3>We use css to change the background color of the content to be equal to the tab</h3>--}}
                                {{--</div>--}}
                            </div>
                        </div>

                        <hr></hr>
                        {{--========Tab start======--}}

                </div>






            </div>
        </div>
    </div>

@endsection


@section('page-script')

    <style>

    </style>


    <script type="text/javascript">


        $(document).ready(function() {
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });

        // Javascript to enable link to tab





        //$(document).ready(function () {
        //    if (window.location.hash) {
        //        $('.nav-pills a[href="#'+window.location.hash+'"]').tab('show');
        //    }
        //});

        //$(function () {
        //    var activeTab = $('[href=' + location.hash + ']');
        //    activeTab && activeTab.tab('show');
        //});




    </script>
@stop
{{--@endsection--}}
