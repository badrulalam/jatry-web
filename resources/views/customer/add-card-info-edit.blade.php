@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Card Info</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                        {!! Form::model($cardinfo, ['method' => 'PATCH','route' => ['customeraddcardupdate', $cardinfo->cardtrak], 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal']) !!}

                        <fieldset>
                      {{-------------------------------------------------}}

                        <div class="form-group{{ $errors->has('card_firstname') ? ' has-error' : '' }}">
                            <label for="card_firstname" class="col-md-4 control-label">First Name</label>
                            <div class="col-md-6">
                                {!! Form::text('card_firstname', $cardinfo->billing_firstname, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                @if ($errors->has('card_firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_lastname') ? ' has-error' : '' }}">
                            <label for="card_lastname" class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                {!! Form::text('card_lastname', $cardinfo->billing_lastname, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                @if ($errors->has('card_lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_email') ? ' has-error' : '' }}">
                            <label for="card_email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                {!! Form::text('card_email', $cardinfo->billing_email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                @if ($errors->has('card_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_address') ? ' has-error' : '' }}">
                            <label for="card_address" class="col-md-4 control-label">Address</label>
                            <div class="col-md-6">
                                {!! Form::textarea('card_address', $cardinfo->billing_address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                @if ($errors->has('card_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_city') ? ' has-error' : '' }}">
                            <label for="card_city" class="col-md-4 control-label">City</label>
                            <div class="col-md-6">
                                {!! Form::text('card_city', $cardinfo->billing_city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                @if ($errors->has('card_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('card_state') ? ' has-error' : '' }}">
                            <label for="card_state" class="col-md-4 control-label">State</label>
                            <div class="col-md-6">
                                {!! Form::text('card_state', $cardinfo->billing_state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                @if ($errors->has('card_state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('card_postalcode') ? ' has-error' : '' }}">
                            <label for="card_postalcode" class="col-md-4 control-label">Postal Code</label>
                            <div class="col-md-6">
                                {!! Form::text('card_postalcode', $cardinfo->billing_postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                @if ($errors->has('card_postalcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_postalcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('card_country') ? ' has-error' : '' }}">
                            <label for="card_country" class="col-md-4 control-label">Country</label>
                            <div class="col-md-6">
                                {!! Form::select('card_country', Countries::getList('en', 'php', 'icu'), $cardinfo->billing_country, array('class' => 'form-control')) !!}

                                @if ($errors->has('card_country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('card_phone') ? ' has-error' : '' }}">
                            <label for="card_phone" class="col-md-4 control-label">Phone</label>
                            <div class="col-md-6">
                                {!! Form::text('card_phone', $cardinfo->billing_phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                @if ($errors->has('card_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                      {{-------------------------------------------------  --}}
                    <div class="form-group{{ $errors->has('card_holder') ? ' has-error' : '' }}">
                        <label for="card_holder" class="col-md-4 control-label">Card Holder</label>
                        <div class="col-md-6">
                            {!! Form::text('card_holder', $cardinfo->cardholder, array('placeholder' => 'Card Holder','class' => 'form-control')) !!}
                            @if ($errors->has('card_holder'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('card_holder') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                        <div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
                            <label for="card_number" class="col-md-4 control-label">Card Number</label>
                            <div class="col-md-6">
                                {!! Form::text('card_number', $cardinfo->card_number, array('placeholder' => 'Card Number','class' => 'form-control')) !!}
                                @if ($errors->has('card_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_cvv') ? ' has-error' : '' }}">
                            <label for="card_cvv" class="col-md-4 control-label">CVV</label>
                            <div class="col-md-6">
                                {!! Form::text('card_cvv', $cardinfo->cvv, array('placeholder' => 'CVV','class' => 'form-control')) !!}
                                @if ($errors->has('card_cvv'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('card_cvv') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>





                    <div class="form-group{{ $errors->has('card_expdate') ? ' has-error' : '' }}">
                        <label for="card_expdate" class="col-md-4 control-label">Card Exp Date</label>

                        <div class="col-md-6">
                            {!! Form::text('card_expdate', $cardinfo->exp_date, array('placeholder' => 'Exp Date','class' => 'form-control')) !!}

                            @if ($errors->has('card_expdate'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('card_expdate') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>




                    </fieldset>




                    <br/>
                    <fieldset>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Save
                                </button>

                            </div>
                        </div>


                    </fieldset>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
