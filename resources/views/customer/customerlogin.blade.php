@extends('layouts.frontend')
@section('title', '- Login')
@section('content')

    <div class="container" style="min-height: 820px">
        <div class="row">

            <div class="col-sm-12">
                <br/><br/><br/>
                <h1 class="home-head-h1">LOGIN TO YOUR ACCOUNT</h1>
                <br/>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 login-facebook-cont">
                <p>LOGIN WITH YOUR FACEBOOK ACCOUNT</p><br/>
                <a  href="{{ Route('tofacebook') }}"><img src="/img/facebook-signin.png" alt="JATRY" height="40px;" ></a>
                <br/> <p>WE DO NOT POST ON YOUR WALL</p><br/>
                <p>WE DO NOT COLLECT YOUR INFORMATION</p><br/>
                <p>WE DO NOT SHARE YOYR PRIVATE DATA</p><br/>
            </div>
            <div class="col-sm-8">



                <div class="col-sm-12 login-container">



                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif


                    @if ($errors->has('crederr'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('crederr') }}</strong>
                        </span>
                    @endif




                    <form class="form-horizontal" role="form" method="POST" action="{{ route('customerlogindo') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">EMAIL ADDRESS</label>


                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">PASSWORD</label>


                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                        </div>


                       <div class="col-sm-4 col-sm-offset-4" >
                           <input class="btn btn-primary col-sm-12" type="submit" value="LOGIN" /><br/><br/>
                           <a class="btn btn-link" href="{{ url('user-pass/reset') }}">FORGOT YOUR PASSWORD?</a>
                           <a class="btn btn-link" href="{{ Route('customerregister') }}">CREATE A NEW ACCOUNT</a>
                       </div>

                        {{--<input class="btn btn-primary col-sm-4 col-sm-offset-4" type="submit" value="LOGIN" />--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-btn fa-sign-in"></i> Login--}}
                                {{--</button>--}}
                                {{--<a class="btn btn-primary" href="{{ Route('tofacebook') }}">FB-LOGIN</a>--}}
                                {{--<a class="btn btn-link" href="{{ url('user-pass/reset') }}">Forgot Your Password?</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </form>
                </div>
            </div>

        </div>

    </div>

@endsection
