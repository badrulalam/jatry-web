@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Password Reset</div>

                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('userpassresetsend') }}">

                        @if ($resetmessage = Session::get('resetmessage'))
                            <div class="alert alert-success">
                                <p>{{ $resetmessage }}</p>
                            </div>
                        @endif


                        {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>







                        <br/>
                        <fieldset>
                            {{--<legend></legend>--}}
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Submit
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </form>



                </div>
            </div>
        </div>
    </div>

@endsection
