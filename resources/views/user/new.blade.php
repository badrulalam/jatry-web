@extends('layouts.admin')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1 margin-tb">
            <div class="pull-left">
                <h2>Users Management</h2>
            </div>
            <div class="pull-right">
                {{--<a class="btn btn-success" href="{{ route('allusers') }}"> Back </a>--}}
                <a class="btn btn-primary" href="{{ route('allusers') }}"> Back</a>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create new user</div>
                <div class="panel-body">

                    {!! Form::open(array('route' => 'createuser','method'=>'POST', 'class' => 'form-horizontal')) !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Roles</label>
                        <div class="col-md-6">
                            {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                            @if ($errors->has('roles'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Register
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>


    {{-------------------------------------}}




@endsection
