/**
 * Created by mahfuz on 11/30/15.
 */
(function(){
    'use strict';

    var app = angular.module('app', ['angular-loading-bar', 'ngAnimate','ui.bootstrap', 'angular.filter', 'ngSanitize',  'rzModule']);

    app.directive('sidebarDirective', function () {
        return {
            link: function (scope, element, attr) {
                scope.$watch(attr.sidebarDirective, function (newVal) {
                    if (newVal) {
                        element.addClass('show');
                        return;
                    }
                    element.removeClass('show');
                });
            }
        };
    });
    app.filter('mydate', function () {
        return function (input, option1, option2) {
            var date = new Date(input);
            if(option1 === 'date')
                return date.toLocaleDateString();
            if(option1 === 'time')
                return date.toLocaleTimeString();
            return date.toLocaleString();
        }
    });
    app.filter('reverse', function() {
        return function(input, uppercase) {
            input = input || '';
            var out = '';
            for (var i = 0; i < input.length; i++) {
                out = input.charAt(i) + out;
            }
            // conditional based on optional argument
            if (uppercase) {
                out = out.toUpperCase();
            }
            return out;
        };
    });

    app.filter('airport', function() {
        return function(input) {
            if(!input){
                return;
            }
            var airport = airportJson.find(function (row) {
                return row.ac === input;
            });

            return airport ? airport : {};
        };
    });
    app.filter('airline', function() {
        return function(input) {
            if(!input){
                return;
            }
            var airport = airlinesJson.find(function (row) {
                return row.iata === input;
            });

            return airport ? airport : {};
        };
    });


    app.directive('ngSparkline', function() {
        return {
            restrict: 'A',
            template: '<div class="sparkline"></div>'
        }
    });
    app.filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function(item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });

    app.directive('masonryWrapper', function () {
        return{
            restrict: 'A',
            link: function (scope,element) {
                console.log("element",element)
                $(element).imagesLoaded(function(){
                    $(element).masonry({
                        // options...
                        itemSelector: '.grid-item',
                        columnWidth: '.grid-sizer',
                        percentPosition: true
                    });
                });
            }
        }
    });
    app.directive('ddSpinner', function () {
        return {
            restrict: "AE",
            scope: {
                bindedModel: "=ngModel"
            },
            template: '<div class="btn-group " role="group" aria-label="..." ng-init="bindedModel = 1*bindedModel">' +
            '<button class="btn btn-default" ng-click="bindedModel > 0 ? bindedModel = 1*bindedModel - 1 : null">-</button>' +
            '<button class="btn btn-default" >{{bindedModel}}</button>' +
            '<button class="btn btn-default" ng-click="bindedModel = 1*bindedModel + 1">+</button>' +
            '</div>',
            link: function (scope, element, attrs) {
                // console.log(attrs);
            }
        }
    });
    app.directive('slideable', function () {
        return {
            restrict:'C',
            compile: function (element, attr) {
                // wrap tag
                var contents = element.html();
                element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

                return function postLink(scope, element, attrs) {
                    // default properties
                    attrs.duration = (!attrs.duration) ? '0.25s' : attrs.duration;
                    attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                    element.css({
                        'overflow': 'hidden',
                        'height': '0px',
                        'transitionProperty': 'height',
                        'transitionDuration': attrs.duration,
                        'transitionTimingFunction': attrs.easing
                    });
                };
            }
        };
    })
        app.directive('slideToggle', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    var target = document.querySelector(attrs.slideToggle);
                    attrs.expanded = false;
                    element.bind('click', function() {
                        var content = target.querySelector('.slideable_content');
                        if(!attrs.expanded) {
                            content.style.border = '1px solid rgba(0,0,0,0)';
                            var y = content.clientHeight;
                            content.style.border = 0;
                            target.style.height = y + 'px';
                        } else {
                            target.style.height = '0px';
                        }
                        attrs.expanded = !attrs.expanded;
                    });
                }
            }
        });
})();
