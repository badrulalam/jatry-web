(function () {
    'use strict';
    angular.module('app')
        .controller('PackageDetailsCtrl', ["$scope","$http", "$location", "$window", "$uibModal", function($scope, $http, $location, $window, $uibModal){



            var tpackage = $window.location.pathname.split("/")[2]|| false;

           if(tpackage){

               $http.get('/api/select-package-details/'+tpackage)
                   .success(function (data) {
                       $scope.detailsData = data;
                       //$scope.countryData = 'sd';
                       console.log(data);
                   });
           }else{
              console.log("country not found")
           }



            $scope.selQt = 0;
            $scope.qtys = [1,2,3,4,5];



            //------------Add to cart Start-------------

            $scope.pkgcarterror = false;
            $scope.packaageAddToCart = function(slug, qty){

                if(qty > 0){
                    //$scope.pkgcarterror = false;

                    $http.get('/api/package-add-to-cart/'+slug+'/'+qty)
                        .success(function (data) {
                            $scope.cartData = data;

                            if($scope.cartData === 'not-login'){
                                $window.location.href = '/customer-login-for-package/'+slug;
                            }


                            else{

                                //---Cart Item show start----

                                var modalInstance = $uibModal.open({
                                    animation: $scope.animationsEnabled,
                                    templateUrl: '/template/package/package-cart.modal.html',
                                    controller: 'customerpackageModalCtrl',
                                    //cartddata: $scope.detailsData,
                                    resolve: {
                                        items: function () {
                                            //return $http.get('/api/cart')
                                            //    .then(function (data) {
                                            //        console.log(data);
                                            //        return data.data;
                                            //    })
                                            return $scope.cartData;
                                        },
                                        //airlines: function () {
                                        //    return $scope.airlines;
                                        //}
                                    }
                                });

                                //modalInstance.result.then(function (selectedItem) {
                                //    $scope.selected = selectedItem;
                                //}, function () {
                                //    $log.info('Modal dismissed at: ' + new Date());
                                //});



                                //---Cart Item show end----


                            }



                            console.log(data);
                        });

                }
                else{
                    $scope.pkgcarterror = true;

                }




            }

            $scope.changeQty = function(qty){
                if(qty > 0){
                    $scope.pkgcarterror = false;
                }

            }


            //------------Add to cart End-------------



            //------------------Login Start-------------


            //$scope.openCustomerLogin = function (size) {
            //
            //    var modalInstance = $uibModal.open({
            //        animation: $scope.animationsEnabled,
            //        templateUrl: '/template/package-customer-login.modal.html',
            //        controller: 'customerpackageModalCtrl',
            //        size: size,
            //        resolve: {
            //            items: function () {
            //                return $http.get('/api/cart')
            //                    .then(function (data) {
            //                        console.log(data);
            //                        return data.data;
            //                    })
            //            },
            //            airlines: function () {
            //                return $scope.airlines;
            //            }
            //        }
            //    });
            //
            //    modalInstance.result.then(function (selectedItem) {
            //        $scope.selected = selectedItem;
            //    }, function () {
            //        $log.info('Modal dismissed at: ' + new Date());
            //    });
            //};


            //------------------Login End-------------





         //---------Modal Start---------

            //$scope.thumbs = [
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-10.jpg","name":"Cat on Fence"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-7.jpg","name":"Cat in Sun"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-9.jpg", "name":"Blue Eyed Cat"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-5.jpg", "name":"Patchy Cat"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-6.jpg", "name":"Feral Cats"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-3.jpg", "name":"Mad Cat"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-1.jpg", "name":"Fluffy Cat"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-4.jpg", "name":"Cat Laying Down"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-8.jpg", "name":"Content Cat"},
            //    {"image":"http://lorempixel.com/output/cats-q-c-640-480-2.jpg", "name":"Hissing Cat"},
            //    {"image":"http://lorempixel.com/output/animals-q-c-640-480-4.jpg", "name":"Not a Cat"},
            //    {"image":"http://lorempixel.com/output/animals-q-c-640-480-10.jpg", "name":"Also not a Cat"}
            //];
            //
            //$scope.open=function(indx){
            //    $scope.thumbs[indx].active=true;
            //
            //    $scope.modalInstance=$uibModal.open({
            //        animation: true,
            //        templateUrl: 'pic-modal.html',
            //        scope: $scope
            //    });
            //};
            //
            //$scope.ok = function () {
            //    $scope.modalInstance.close();
            //};

         //---------Modal End---------


        }





        ])
        .controller('customerpackageModalCtrl', ["$scope", "$uibModalInstance", "items", function ($scope, $uibModalInstance, items) {
            console.log(items);
            $scope.pDetailsData = items;
            //$scope.selected = {
            //    item: $scope.items[0]
            //};

            $scope.ok = function () {
                $uibModalInstance.close($scope.selected.item);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            
        }]);





})();
