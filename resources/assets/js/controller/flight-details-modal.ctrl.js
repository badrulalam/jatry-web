/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('FlightDetailsModalCtrl', ["$scope", "$uibModalInstance", "$http", "data", "parse", "key","airlines", function ($scope, $uibModalInstance, $http, data, parse, key, airlines) {


            $scope.parse = parse;
            console.log($scope.parse);

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.fromJson = function (data) {
                return angular.fromJson(data);
            };

            $scope.getBookingClass = function (key) {
               var booking =  $scope.parse.Price.BookingInfo.find(function (row) {
                    return row.SegmentRef == key;
                });
                if(booking){
                    return booking.CabinClass? booking.CabinClass : 'Not Found';
                }
            };
$scope.dateDifference = function (date1, date2) {
    var d1 = new Date(date1);
    var d2 = new Date(date2);
    var difference_ms = d1 - d2;
    console.log(difference_ms);
    //take out milliseconds
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var hours = Math.floor(difference_ms % 24);
    var days = Math.floor(difference_ms/24);

    var ret_data = "";
    if(days){
        ret_data += days + ' days';
    }
    if(hours){
        ret_data += hours + 'h ';
    }
    if(minutes){
        ret_data += minutes + 'm';
    }
    // return days + ' days, ' + hours + ' hours, ' + minutes + ' minutes, and ' + seconds + ' seconds';
    return ret_data;
};

            $scope.show = function () {
                console.log(data);
                console.log(key);

                var AirPricingSolution = data["AirPricingSolution"][key];
                console.log("AirPricingSolution ",AirPricingSolution);
            }
        }]);
})();
