(function () {
    'use strict';
    angular.module('app')
        .controller('ConfirmTicketCtrl', ["$scope","$http", "$location", "$window", "$httpParamSerializer", function($scope, $http, $location, $window, $httpParamSerializer) {

    $http.get('/api/air-ticket')
        .then(function (data) {
            $scope.cart = data.data;
            // console.log($scope.cartinfo );
            console.log(data.data)
        });


            $scope.saveAndContinue = function () {
                console.log($scope.ticket);
                $http.get('/api/cart')
                    .then(function (data) {
                        console.log(data);
                        var cart = data.data;
                        cart[$scope.index] = $scope.ticket;

                        $scope.setToCart(cart);
                    });
            };
            $scope.billingAndShippingSaveAndContinue = function () {
                $http.put('/api/cartinfo', $scope.cartinfo)
                    .then(function (data) {
                        console.log(data);
                        var  changeLocation = function(url, forceReload) {
                            $scope = $scope || angular.element(document).scope();
                            if(forceReload || $scope.$$phase) {
                                window.location = url;
                            }
                            else {
                                //only use this if you want to replace the history stack
                                //$location.path(url).replace();

                                //this this if you want to change the URL and add it to the history stack
                                $location.path(url);
                                $scope.$apply();
                            }
                        };
                        changeLocation("/checkout-final", true);
                    })
            };


            $scope.editTicketInfo =  function ($index) { console.log($index)
                var  changeLocation = function(url, forceReload) {
                    $scope = $scope || angular.element(document).scope();
                    if(forceReload || $scope.$$phase) {
                        window.location = url;
                    }
                    else {
                        //only use this if you want to replace the history stack
                        //$location.path(url).replace();

                        //this this if you want to change the URL and add it to the history stack
                        $location.path(url);
                        $scope.$apply();
                    }
                };
                changeLocation("/checkout-ui#?"+ $httpParamSerializer({index: $index}));
                // $window.location.reload();
            };
            $scope.fromJson = function (data) {
                return angular.fromJson(data);
            }
        }]);
})();
