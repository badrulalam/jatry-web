(function () {
    'use strict';
    AppCtrl.$inject = ["$scope", "$http", "$timeout", "$q", "$log", "$window", "$location", "$httpParamSerializer"];

    angular.module('app')
        .controller('DomHomeMaterialCtrl', AppCtrl);

    function AppCtrl($scope, $http, $timeout, $q, $log, $window, $location, $httpParamSerializer) {

        // $scope.items = [1, 2, 3, 4, 5];
        $scope.collapse = {
            filter : true,
            search : true
            };
        $scope.tLayoverAirport = [];
        $scope.tLayoverAirport_selected = [];

        $scope.tAirlines = [];
        $scope.tAirlines_selected = [];

        $scope.tStops = [{value: 1, view: "Non Stop"}, {value: 2, view: "1 Stop"}, {value: 3, view: "2 Stops+"}];
        $scope.tStopsSelected = [];

        $scope.tLayoverTime = [
            {min: 0, max: 5, view: "0h - 5h"},
            {min: 5, max: 10, view: "5h - 10h"},
            {min: 10, max: 15, view: "10h - 15h"},
            {min: 15, max: 999, view: "15h+"}
        ];
        $scope.tLayoverTimeSelected = [];

        $scope.tFareType = [
            {value: "true", view: "Refundable"},
            {value: "false", view: "Non Refundable"}
        ];
        $scope.tFareTypeSelected = [];

        $scope.tDepartureTime = [
            {min: 0, max: 6, view: "00 - 06"},
            {min: 6, max: 12, view: "6 - 12"},
            {min: 12, max: 18, view: "12 - 18"},
            {min: 18, max: 24, view: "18 - 24"}
        ];
        $scope.tDepartureTimeSelected = [];


        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };
        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };


        $scope.searchObj = {};
        $scope.searchObj.adults = 1;
        $scope.searchObj.childs = 0;
        $scope.searchObj.infant = 0;
        $scope.searchObj.class = "Economy";
        $scope.searchObj.tripType = "O";

        $scope.airportJson = airportJson;

        $scope.multicity = [{}, {}];
        $scope.addMulticity = function () {
            $scope.multicity.push({});
        };

        $scope.removeMulticity = function (index) {
            $scope.multicity.splice(index, 1);
        };


        var self = this;
        // list of `state` value/display objects
        self.states = loadAll();
        self.selectedItem = null;
        self.searchText = null;
        self.querySearch = querySearch;
        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {
            var results = query ? self.states.filter(createFilterFor(query)) : self.states;
            console.log(results);
            var deferred = $q.defer();
            //$timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            $timeout(function () {
                deferred.resolve(results);
            }, 50, false);
            return deferred.promise;
        }

        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            return airportJson.map(function (state) {
                return {
                    value: state.ct.toLowerCase(),
                    display: state.ct + ', ' + state.cn + ' (' + state.ac + ')',
                    ac: state.ac,
                    an: state.an,
                    han: state.han,
                    cn: state.cn,
                    hcn: state.hcn,
                    cc: state.cc,
                    ct: state.ct,
                    hct: state.hct
                };
            });
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };
        }


        $scope.searchTrigger = function () {
            var triggerObj = {};
            triggerObj.adults = $scope.searchObj.adults;
            triggerObj.childs = $scope.searchObj.childs;
            triggerObj.infant = $scope.searchObj.infant;
            triggerObj.class = $scope.searchObj.class;
            triggerObj.flexible = $scope.searchObj.flexible;
            if ($scope.searchObj.tripType === 'O') {
                triggerObj.tripType = $scope.searchObj.tripType;

                triggerObj.noOfSegments = 1;

                if ($scope.searchObj.origin && $scope.searchObj.origin.ac) {
                    triggerObj.origin = $scope.searchObj.origin.ac;
                }

                if ($scope.searchObj.destination && $scope.searchObj.destination.ac) {
                    triggerObj.destination = $scope.searchObj.destination.ac;
                }

                triggerObj.departureDate = $scope.searchObj.departureDate;
            }
            if ($scope.searchObj.tripType === 'R') {
                triggerObj.tripType = $scope.searchObj.tripType;

                triggerObj.noOfSegments = 2;

                if ($scope.searchObj.origin && $scope.searchObj.origin.ac) {
                    triggerObj.origin = $scope.searchObj.origin.ac;
                }

                if ($scope.searchObj.destination && $scope.searchObj.destination.ac) {
                    triggerObj.destination = $scope.searchObj.destination.ac;
                }

                triggerObj.departureDate = $scope.searchObj.departureDate;
                triggerObj.arrivalDate = $scope.searchObj.arrivalDate;
            }
            if ($scope.searchObj.tripType === 'M') {
                console.log($scope.multicity);
                triggerObj.tripType = $scope.searchObj.tripType;

                triggerObj.noOfSegments = $scope.multicity.length;

                for (var i = 0; i < $scope.multicity.length; i++) {
                    if ($scope.multicity[i].origin && $scope.multicity[i].origin.ac) {
                        triggerObj["origin_" + (i + 1)] = $scope.multicity[i].origin.ac;
                    }

                    if ($scope.multicity[i].destination && $scope.multicity[i].destination.ac) {
                        triggerObj["destination_" + (i + 1)] = $scope.multicity[i].destination.ac;
                    }

                    triggerObj["departureDate_" + (i + 1)] = $scope.multicity[i].departureDate;
                }

            }

            $scope.successData = {};
            $scope.errorData = {};
            $http.post('/api/trigger/validate', triggerObj)
                .then(function (data) {
                    var prev = $window.location.href;
                    $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);
                    if (prev != $window.location.href) {
                        location.reload();
                    }
                }, function (err) {
                    $scope.errorData = err;
                });
        };




        $scope.$watch('searchObj', function (newValue, oldValue) {
            if (newValue.adults + newValue.childs > 9) {
                newValue.adults = oldValue.adults;
                newValue.childs = oldValue.childs;
            }
            if (newValue.adults < 1) {
                newValue.adults = 1;
            }
            if (newValue.adults < newValue.infant) {
                newValue.infant = newValue.adults;
            }
        }, true);






        //===================Domestic start===========================

        $scope.domsearchObj = {};
        $scope.domsearchObj.adults = 1;
        $scope.domsearchObj.childs = 0;
        $scope.domsearchObj.infant = 0;
        $scope.domsearchObj.class = "Economy";
        $scope.domsearchObj.tripType = "O";


        $scope.domcityData = {};

        $scope.errorDomcityData = {};

        $http.get('/api/city-list')
            .then(function (data) {
                //var prev = $window.location.href;
                //$window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);
                //if (prev != $window.location.href) {
                //    location.reload();
                //}

                $scope.domcityData = data.data;
                console.log('All Cities',$scope.domcityData);


            }, function (err) {
                $scope.errorDomcityData = err;
            });

        $scope.getData = function (obj) { console.log(obj)
            $scope.isLoading = true;


            $scope.domTriggersuccessData = {};
            $scope.domTriggererrorData = {};
            $http.post('/api/domestic-trigger', obj)
                .then(function (data) {

                    $scope.domTriggersuccessData = data.data;
                    $scope.domTriggersuccessData.dpartdate = $scope.searchParams.departureDaydate;
                    $scope.domTriggersuccessData.totalPerson = parseInt($scope.searchParams.adults)+parseInt($scope.searchParams.childs)+parseInt($scope.searchParams.infant);
                    $scope.domTriggersuccessData.class = $scope.searchParams.class;
                    $scope.domTriggersuccessData.departDayname = $scope.searchParams.departureDayname;

                    $scope.isLoading = false;
                    $scope.tUrl = '/template/include/dom-one-way.html';
                    console.log('search param',$scope.searchParams);
                    console.log('found data',$scope.domTriggersuccessData);

                }, function (err) {
                    $scope.domTriggererrorData = err;
                    $scope.isLoading = false;
                }
            );

        };
        $scope.domesticSearchTrigger = function () {
            var domtriggerObj = {};
            domtriggerObj.adults = $scope.domsearchObj.adults;
            domtriggerObj.childs = $scope.domsearchObj.childs;
            domtriggerObj.infant = $scope.domsearchObj.infant;
            domtriggerObj.class = $scope.domsearchObj.class;
            //domtriggerObj.flexible = $scope.domsearchObj.flexible;
            if ($scope.domsearchObj.tripType === 'O') {
                domtriggerObj.tripType = $scope.domsearchObj.tripType;

                domtriggerObj.noOfSegments = 1;

                if ($scope.domsearchObj.origin) {
                    domtriggerObj.origin = $scope.domsearchObj.origin;
                }

                if ($scope.domsearchObj.destination) {
                    domtriggerObj.destination = $scope.domsearchObj.destination;
                }

                var weekday=new Array(7);
                weekday[0]="sun";
                weekday[1]="mon";
                weekday[2]="tue";
                weekday[3]="wed";
                weekday[4]="thu";
                weekday[5]="fri";
                weekday[6]="sat";

                var dayNum = new Date($scope.domsearchObj.departureDate).getDay();
                var dayDate = new Date($scope.domsearchObj.departureDate).getDate();
                var DateMonth = new Date($scope.domsearchObj.departureDate).getMonth()+1;
                var DateYear = new Date($scope.domsearchObj.departureDate).getFullYear();

                domtriggerObj.departureDayname = weekday[dayNum];
                domtriggerObj.departureDaydate = dayDate+'-'+DateMonth+'-'+DateYear;
            }
            if ($scope.domsearchObj.tripType === 'R') {
                domtriggerObj.tripType = $scope.domsearchObj.tripType;

                domtriggerObj.noOfSegments = 2;

                if ($scope.domsearchObj.origin) {
                    domtriggerObj.origin = $scope.domsearchObj.origin;
                }

                if ($scope.domsearchObj.destination) {
                    domtriggerObj.destination = $scope.domsearchObj.destination;
                }

                domtriggerObj.departureDate = new Date($scope.domsearchObj.departureDate).getDay();
                domtriggerObj.arrivalDate = $scope.domsearchObj.arrivalDate;
            }


            console.log(domtriggerObj);


            //if ($scope.domsearchObj.tripType === 'M') {
            //    console.log($scope.multicity);
            //    domtriggerObj.tripType = $scope.domsearchObj.tripType;
            //
            //    domtriggerObj.noOfSegments = $scope.multicity.length;
            //
            //    for (var i = 0; i < $scope.multicity.length; i++) {
            //        if ($scope.multicity[i].origin && $scope.multicity[i].origin.ac) {
            //            domtriggerObj["origin_" + (i + 1)] = $scope.multicity[i].origin.ac;
            //        }
            //
            //        if ($scope.multicity[i].destination && $scope.multicity[i].destination.ac) {
            //            domtriggerObj["destination_" + (i + 1)] = $scope.multicity[i].destination.ac;
            //        }
            //
            //        domtriggerObj["departureDate_" + (i + 1)] = $scope.multicity[i].departureDate;
            //    }
            //
            //}

            $scope.domTriggersuccessData = {};
            $scope.domTriggererrorData = {};
            $http.post('/api/domestic-trigger', domtriggerObj)
                .then(function (data) {
                    var prev = $window.location.href;
                    $window.location.href = '/dom-flights#?' + $httpParamSerializer(domtriggerObj, true);
                    if (prev != $window.location.href) {
                        location.reload();
                    }

                    console.log('Send Data',data);

                }, function (err) {
                    $scope.domTriggererrorData = err;
                }
            );
        };
        $scope.$watch('domsearchObj', function (newValue, oldValue) {
            if (newValue.adults + newValue.childs > 9) {
                newValue.adults = oldValue.adults;
                newValue.childs = oldValue.childs;
            }
            if (newValue.adults < 1) {
                newValue.adults = 1;
            }
            if (newValue.adults < newValue.infant) {
                newValue.infant = newValue.adults;
            }
        }, true);
        if ($location.search() && $location.search().tripType) {

            $scope.searchParams = $location.search();
            $scope.getData($location.search());
        }
        //===================Domestic end=============================


















    }



})();


