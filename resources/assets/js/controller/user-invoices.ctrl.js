(function () {
    'use strict';
    angular.module('app')
        .controller('UserInvoicesCtrl', ["$scope","$http", "$location", "$window", "$httpParamSerializer", function($scope, $http, $location, $window, $httpParamSerializer) {

                $http.post('/api/sale-all-invoice',$scope.payment)
                    .then(function (data) {

                        //$scope.isProcessing = false;

                        $scope.sales = data.data.allsale;
                        console.log("data", data);
                        //if(data.data.flag == "true"){
                        //    $window.location.href = '/ticket-sale/'+data.data.invoice;
                        //
                        //}else{
                        //    $scope.error = true;
                        //}
                    });


            $scope.invoiceClick = function(saleid){

                //alert(saleid);

                $http.post('/api/click-invoice',{'saleid': saleid})
                    .then(function (data) {

                        //$scope.isProcessing = false;


                        $scope.allTicket = '';
                        $scope.sale = '';
                        $scope.cardinfo = '';

                        //$scope.allTicket = data.data.allticket;
                        $scope.sale = data.data.sale;
                        $scope.cardinfo = data.data.cardinfo;

                        //console.log("datam", $scope.sale );
                        //if(data.data.flag == "true"){
                        //    $window.location.href = '/ticket-sale/'+data.data.invoice;
                        //
                        //}else{
                        //    $scope.error = true;
                        //}
                    });

            }

            $scope.dateDifference = function (date1, date2) {
                var d1 = new Date(date1);
                var d2 = new Date(date2);
                var difference_ms = d1 - d2;
                console.log(difference_ms);
                //take out milliseconds
                difference_ms = difference_ms/1000;
                var seconds = Math.floor(difference_ms % 60);
                difference_ms = difference_ms/60;
                var minutes = Math.floor(difference_ms % 60);
                difference_ms = difference_ms/60;
                var hours = Math.floor(difference_ms % 24);
                var days = Math.floor(difference_ms/24);

                var ret_data = "";
                if(days){
                    ret_data += days + ' days';
                }
                if(hours){
                    ret_data += hours + 'h ';
                }
                if(minutes){
                    ret_data += minutes + 'm';
                }
                // return days + ' days, ' + hours + ' hours, ' + minutes + ' minutes, and ' + seconds + ' seconds';
                return ret_data;
            };
        }]);
})();
