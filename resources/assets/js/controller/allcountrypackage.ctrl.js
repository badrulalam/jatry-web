(function () {
    'use strict';
    angular.module('app')
        .controller('AllCountryPackageCtrl', ["$scope","$http", "$location", "$window", function($scope, $http, $location, $window){


            $scope.allCountry = [];
            $scope.selectedCountry = [];



            $scope.propertyType = [{view: 'Hotel', type: 'hotel'},{view:'Resorts', type: 'resorts'},{view:'Cruiseship', type: 'cruiseship'}];
            $scope.selectedPropertyType = [];

            $scope.ratingType = [{ label:'1 Star', key: 'hotel_rating1', rating:'1'}, { label:'2 Star', key: 'hotel_rating2', rating:'2'}, { label:'3 Star', key: 'hotel_rating3', rating:'3'}, { label:'4 Star', key: 'hotel_rating4', rating:'4'}, { label:'5 Star', key: 'hotel_rating5', rating:'5'}];
            $scope.selectedRatingType = [];

            //$scope.airFare = [{view: 'Included', type: 'airfare_include'},{view:'Excluded', type: 'airfare_exclude'}];
            //$scope.selectedAirFare = [];

            //$scope.airFareInclude = [{ label:'Included', key: 'airfare_include', value:'1'}];
            $scope.selectedairFare = {};

            $scope.selectedtransportFare = {};
            //$scope.priceRange = {minValue: 5000, maxValue: 10000};

            //$scope.airFareExclude = [{ label:'Excluded', key: 'airfare_exclude', value:'1'}];
            //$scope.selectedairFareExclude  = [];


            //$scope.min_price = 0;
            //$scope.max_price = 0;

            //var country= $window.location.pathname.split("/")[2]|| false;

            var country = true;
           if(country){

               //$scope.min_price = 0;
               //$scope.max_price = 0;

               $http.get('/api/all-country-tour/')
                   .success(function (data) {
                       //$scope.allCountry = data.allcountry;


                       angular.forEach(data.allcountry, function (value, key) {
                           //$scope.allCountry.push(value);
                           //$scope.allCountry.push(key + ': ' + value);
                           $scope.allCountry.push({view:value.name, key :  value.id.toString()});
                       });




                       $scope.countryData = data;
                       //$scope.countryData = 'sd';
                       console.log(data);

                       var min_price = Number($scope.countryData.filterData.min_price);
                       var max_price = Number($scope.countryData.filterData.max_price);
                       //$scope.otherData.change = min_price;
                       //$scope.otherData.maxv = max_price;

                       $scope.slider = {

                           minValue: min_price,
                           maxValue: max_price,
                           options: {
                               floor: min_price,
                               ceil: max_price,
                               translate: function(value) {
                                   return  value + 'BDT';
                               },
                               //interval: 1000

                               //-------------------
                               onStart: function () {
                                   $scope.otherData.start = $scope.slider.minValue;
                               },
                               onChange: function () {
                                   $scope.otherData.change = $scope.slider.minValue;
                                   $scope.otherData.maxv = $scope.slider.maxValue;

                               },
                               onEnd: function () {
                                   $scope.otherData.end = $scope.slider.maxValue;
                               }

                               //----------------

                           }
                       };
                       $scope.otherData = {
                           start: 0,
                           //change: 0,
                           change: min_price,
                           end: 0,
                           //maxv: 0
                           maxv: max_price
                       };
                   });

               $scope.hotel = false;
               $scope.resorts = false;



               $scope.toggle = function (item, list) {
                   var idx = list.indexOf(item);
                   if (idx > -1) {
                       list.splice(idx, 1);
                   }
                   else {
                       list.push(item);
                   }
               };
               $scope.exists = function (item, list) {
                   return list.indexOf(item) > -1;
               };





           }else{
              console.log("country not found")
           }


$scope.getTotalNumberCountry = function(item){
    if($scope.countryData)
        return $scope.countryData.countFiltr[item];
}


            $scope.getTotalNumber = function(item){
                if($scope.countryData)
                    return $scope.countryData.filterData[item];
            }

$scope.counter = 0;
            $scope.value = function () {
                console.log("in");
            }
$scope.checkByFilter = function (row) { //console.log($scope.counter++);
    //"property_type": "hotel",
    //    "hotel_rating": "2",
var flag = true;
    if(flag && $scope.selectedCountry.length>0){
        flag =  hasObject($scope.selectedCountry, "key", row.jcountry_id);

    }

    if(flag && $scope.selectedRatingType.length>0){
        flag =  hasObject($scope.selectedRatingType, "rating", row.hotel_rating);
    }
    if(flag && $scope.selectedPropertyType.length>0){
        flag =  hasObject($scope.selectedPropertyType, "type", row.property_type);
    }
    if(flag) {
        if($scope.selectedairFare.airfare_include && !$scope.selectedairFare.airfare_exclude){
            flag = row.airfare_include === "1" ? true : false;
        }
        if(!$scope.selectedairFare.airfare_include && $scope.selectedairFare.airfare_exclude){
            flag = row.airfare_exclude === "1" ? true : false;
        }
    }

    if(flag) {
        if($scope.selectedtransportFare.transport_include && !$scope.selectedtransportFare.transport_exclude){
            flag = row.transport_include === "1" ? true : false;
        }
        if(!$scope.selectedtransportFare.transport_include && $scope.selectedtransportFare.transport_exclude){
            flag = row.transport_exclude === "1" ? true : false;
        }
    }

    if(flag) {
        var minValue = Number($scope.otherData.change);
        var maxValue = Number($scope.otherData.maxv);
        var price = Number(row.price);

        if (!(minValue <= price && price <= maxValue)) {
            flag = false;
        }
    }


return flag;
}


            function hasObject(myArray,  key, value) { console.log(myArray, key, value)

                for(var i = 0, len = myArray.length; i < len; i++) {
                    if (myArray[i][key] === value) return true;
                }
                return false;
            }
        }

        ]);





})();
