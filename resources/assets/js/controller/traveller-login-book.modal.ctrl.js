/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('TravellerLoginBookModalCtrl', ["$scope", "$uibModalInstance", "$http", "$httpParamSerializer", "$window", "AirPricePoint", "SearchParams", function ($scope, $uibModalInstance, $http,  $httpParamSerializer,$window, AirPricePoint, SearchParams) {

            
            
            
$scope.loginInfo = {};
$scope.registerInfo = {};
            $scope.ok = function () {
                //$uibModalInstance.close($scope.selected.item);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            $scope.login = function(){
                console.log($scope.loginInfo);
                if($scope.loginInfo.email && $scope.loginInfo.password){
                    $http.post('/customer-login-with-booking', $scope.loginInfo)
                        .success(function (data) {
                            console.log("data", data);
                            if(data === 1){
                                $scope.bookNow();
                            }else{
                                $scope.invalidMessage()
                            }
                        })
                        .error(function (err) {
                            console.log("err", err);
                        })
                }
            };           
            $scope.register = function(){
                console.log($scope.registerInfo);
                if($scope.registerInfo.email && $scope.registerInfo.password){
                    $http.post('/customer-register-with-booking', $scope.registerInfo)
                        .success(function (data) {
                            console.log("data", data);
                            if(data === 1){
                                $scope.bookNow();
                            }else{
                                $scope.invalidMessage()
                            }
                        })
                        .error(function (err) {
                            console.log("err", err);
                        })
                }
            };
            $scope.bookNow = function () {
                $http.post('/api/air-ticket', {AirPricePoint:AirPricePoint, SearchParams: SearchParams})
                    .then(function (data) {
                        if(data.data == "not-login"){

                        }else if(data.data == "already-added"){
                            alert('Already Booked');
                            location.reload();
                        }else{
                            $window.location.href = '/checkout-ui';
                        }

                    });


            };
            $scope.invalidMessage = function () {
                alert("invalid");
            }
    }]);
})();
