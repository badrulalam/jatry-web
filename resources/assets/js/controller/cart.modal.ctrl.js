/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('CartModalCtrl', ["$scope", "$uibModalInstance", "$http", "items", "airlines", "$httpParamSerializer", "$window", function ($scope, $uibModalInstance, $http, items, airlines, $httpParamSerializer, $window) {
            if(items === 'not-login'){
                $scope.notLogin = true;
            }else{
                $scope.items = items;
            }


            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
  
            $scope.clearCart = function () {
                $http.put('/api/cart', {"content": JSON.stringify([])})
                    .then(function (data) {
                        console.log(data.data);
                        $scope.items = data.data;
                    })
            };
            $scope.deleteCartItem = function (ticket, index) {
                $http.delete('/api/air-ticket/delete?' +$httpParamSerializer({"ticketKey":ticket}))

                    .then(function (data) {
                        if (data.data == 1) {
                            $scope.items.splice(index, 1);

                        }
                    })
            };
            $scope.modifyCartItem = function (ticket_key, $index) {
                console.log($index)
                var changeLocation = function (url, forceReload) {
                    $scope = $scope || angular.element(document).scope();
                    if (forceReload || $scope.$$phase) {
                        window.location = url;
                    }
                    else {
                        //only use this if you want to replace the history stack
                        //$location.path(url).replace();

                        //this this if you want to change the URL and add it to the history stack
                        $location.path(url);
                        $scope.$apply();
                    }
                };
                changeLocation("/checkout-ui#?" + $httpParamSerializer({index: ticket_key}));
                // $window.location.reload();
            }
            $scope.fromJson = function (data) {
                return angular.fromJson(data);
            }
        }]);
})();
