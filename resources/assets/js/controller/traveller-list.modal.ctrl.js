/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('TravellerListModalCtrl', ["$scope", "$uibModalInstance", "$http", "travellers", "$httpParamSerializer", "$window", function ($scope, $uibModalInstance, $http, travellers, $httpParamSerializer,$window) {

            
            $scope.travellers = travellers;
            
            $scope.selected = {
                item: $scope.travellers[0]
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selected.item);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
    }]);
})();
