@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Agent Account</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('agentmyaccountsave') }}">
                            {{ csrf_field() }}


                            <fieldset>
                                <legend>Login Information</legend>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="user[name]" value="{{ $user->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="user[email]" value="{{ $user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            </fieldset>



                            <fieldset>
                                <legend>Agency Information</legend>

                                <div class="form-group{{ $errors->has('agency_code') ? ' has-error' : '' }}">
                                    <label for="agency_code" class="col-md-4 control-label">Code</label>
                                    <div class="col-md-6">
                                        <input id="agency_code" type="text" class="form-control" name="agency[code]" value="{{ $user->agency->code }}">

                                        @if ($errors->has('agency_code'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_code') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('agency[name]') ? ' has-error' : '' }}">
                                    <label for="agency_name" class="col-md-4 control-label">Name</label>
                                    <div class="col-md-6">
                                        <input id="agency_name" type="text" class="form-control" name="agency[name]" value="{{ $user->agency->name }}">

                                        @if ($errors->has('agency[name]'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency[name]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agency[email]') ? ' has-error' : '' }}">
                                    <label for="agency_email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="agency_email" type="email" class="form-control" name="agency[email]" value="{{ $user->agency->email }}">

                                        @if ($errors->has('agency[email]'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency[email]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="address" class="col-md-4 control-label">address</label>
                                    <div class="col-md-6">
                                        <textarea id="address" type="text" class="form-control" name="agency[address]" value="{{ $user->agency->address}}"></textarea>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">city</label>
                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control" name="agency[city]" value="{{ $user->agency->city}}">
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state" class="col-md-4 control-label">state</label>
                                    <div class="col-md-6">
                                        <input id="state" type="text" class="form-control" name="agency[state]" value="{{ $user->agency->state}}">
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('postalcode') ? ' has-error' : '' }}">
                                    <label for="postalcode" class="col-md-4 control-label">postalcode</label>
                                    <div class="col-md-6">
                                        <input id="postalcode" type="text" class="form-control" name="agency[postalcode]" value="{{ $user->agency->postalcode}}">
                                        @if ($errors->has('postalcode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="country" class="col-md-4 control-label">country</label>
                                    <div class="col-md-6">
                                        {!! Form::select('agency[country]', Countries::getList('en', 'php', 'icu'), null, array('class' => 'form-control')) !!}
                                        @if ($errors->has('country'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-4 control-label">phone</label>
                                    <div class="col-md-6">
                                        <input id="phone" type="text" class="form-control" name="agency[phone]" value="{{ $user->agency->phone}}">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label for="image" class="col-md-4 control-label">image</label>
                                    <div class="col-md-6">
                                        <input id="image" type="file" class="form-control" name="agency[image]" value="{{ $user->agency->image}}">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                    <label for="logo" class="col-md-4 control-label">logo</label>
                                    <div class="col-md-6">
                                        <input id="logo" type="file" class="form-control" name="agency[logo]" value="{{ $user->agency->logo}}">
                                        @if ($errors->has('logo'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agency_url') ? ' has-error' : '' }}">
                                    <label for="agency_url" class="col-md-4 control-label">Agency URL</label>
                                    <div class="col-md-6">
                                        <input id="agency_url" type="text" class="form-control" name="agency[agency_url]" value="{{ $user->agency->url }}">
                                        @if ($errors->has('agency_url'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agency_url') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                            </fieldset>

                            <fieldset>
                                <legend>Agent Information</legend>
                                <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                    <label for="code" class="col-md-4 control-label">Code</label>
                                    <div class="col-md-6">
                                        <input id="code" type="text"  class="form-control" name="agent[code]" value="{{ $user->agent->code }}">
                                        @if ($errors->has('code'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name" class="col-md-4 control-label">First Name</label>
                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control" name="agent[first_name]" value="{{ $user->agent->first_name }}">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>



                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name" class="col-md-4 control-label">Last Name</label>
                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control" name="agent[last_name]" value="{{ $user->agent->last_name }}">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agent_email') ? ' has-error' : '' }}">
                                    <label for="agent_email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="agent_email" type="email" class="form-control" name="agent[agent_email]" value="{{ $user->agent->email }}">

                                        @if ($errors->has('agent_email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="address" class="col-md-4 control-label">Address</label>
                                    <div class="col-md-6">
                                        <textarea id="address" type="text" class="form-control" name="agent[address]" value="{{ $user->agent->address }}"></textarea>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">City</label>
                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control" name="agent[city]" value="{{ $user->agent->city }}">
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state" class="col-md-4 control-label">State</label>
                                    <div class="col-md-6">
                                        <input id="state" type="text" class="form-control" name="agent[state]" value="{{ $user->agent->state }}">
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('postalcode') ? ' has-error' : '' }}">
                                    <label for="postalcode" class="col-md-4 control-label">Postal Code</label>
                                    <div class="col-md-6">
                                        <input id="postalcode" type="text" class="form-control" name="agent[postalcode]" value="{{ $user->agent->postalcode }}">
                                        @if ($errors->has('postalcode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="country" class="col-md-4 control-label">Country</label>
                                    <div class="col-md-6">
                                        {!! Form::select('agent[country]', Countries::getList('en', 'php', 'icu'), null, array('class' => 'form-control')) !!}

                                        @if ($errors->has('country'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-4 control-label">Phone</label>
                                    <div class="col-md-6">
                                        <input id="phone" type="text" class="form-control" name="agent[phone]" value="{{ $user->agent->phone }}">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label for="image" class="col-md-4 control-label">Image</label>
                                    <div class="col-md-6">
                                        <input id="image" type="file" class="form-control" name="agent[image]" value="{{ $user->agent->image }}">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                    <label for="logo" class="col-md-4 control-label">Logo</label>
                                    <div class="col-md-6">
                                        <input id="logo" type="file" class="form-control" name="agent[logo]" value="{{ $user->agent->logo }}">
                                        @if ($errors->has('logo'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('agent_url') ? ' has-error' : '' }}">
                                    <label for="agent_url" class="col-md-4 control-label">Agent url</label>
                                    <div class="col-md-6">
                                        <input id="agent_url" type="text" class="form-control" name="agent[agent_url]" value="{{ $user->agent->url }}">
                                        @if ($errors->has('agent_url'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('agent_url') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                            </fieldset>
                            <br/>
                            <fieldset>
                                {{--<legend></legend>--}}
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>

                    <ul>
                        <li>Login Info</li>
                        <li>Agency Info</li>
                        <li>Agent Info</li>
                    </ul>



                </div>
            </div>
        </div>
    </div>

@endsection
