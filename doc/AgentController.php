<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;



use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Session;














class AgentController extends Controller
{


    public function __construct()
    {

    }








    public function agentregisterAction()
    {
        return view('agent.agentregister');
    }

    protected function agentregisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'agency_name' => 'required|unique:agency,name',
            'agency_email' => 'required|email|unique:agency,email',
            'agency_url' => 'required|unique:agency,url',

            'first_name' => 'required',
            'agent_email' => 'required|email|unique:agent,email',
            'agent_url' => 'required|unique:agent,url',

        ]);

//        $input = $request->all();
//        $input['password'] = bcrypt($input['password']);



        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();


//        $role = DB::table('users')->where('name', 'John')->first();
        $role = Role::where('name', 'superagent')->first();
        $user->attachRole($role->id);

        $agency = new Agency();
        $agency->name = $request->input('agency_name');
        $agency->email = $request->input('agency_email');
        $agency->url = $request->input('agency_url');
        $agency->isactive = true;
        $agency->user_id = $user->id;
        $agency->save();

//        $user->agency()->save($agency);
        $agency->code = 'ACY-'.$agency->id;;
        $agency->save();


        $agent = new Agent();
        $agent->first_name = $request->input('first_name');
        $agent->last_name = $request->input('last_name');
        $agent->email = $request->input('agent_email');
        $agent->url = $request->input('agent_url');
        $agent->isactive = true;
        $agent->user_id = $user->id;

        $agent->agency_id = $agency->id;
        $agent->save();

        $agent->code = 'AGT-'.$agent->id;
        $agent->save();


//        $user->agent()->save($agent);

//        $agency->agent()->save($agent);
//
//
//
//        $agent->code = 'AGT-'.$agent->id;;
//        $agent->save();




        return redirect()->route('home')
            ->with('success','User, Agency, Agent created successfully');
    }

    public function frontAgentLoginAction(Request $request)
    {
        return view('agent.agentlogin');
    }





    public function agentMyAccountAction(Request $request)
    {
        $user = \Auth::user();
        return view('agent.myaccount', ['user' => $user]);
    }




 public function agentMyAccountSaveAction(Request $request)
    {
//        return $request->input('user')['name'];
     $this->validate($request->all(), [
//         'agency.name' => 'required',
         'agency' => [
             'name' => 'required',
         ]
//         'email' => 'required|email|unique:users,email',
//         'password' => 'required|same:confirm-password',
//         'agency_name' => 'required|unique:agency,name',
//         'agency_email' => 'required|email|unique:agency,email',
//         'agency_url' => 'required|unique:agency,url',
//
//         'first_name' => 'required',
//         'agent_email' => 'required|email|unique:agent,email',
//         'agent_url' => 'required|unique:agent,url',

     ]);


        return $request->input();
    }











}




